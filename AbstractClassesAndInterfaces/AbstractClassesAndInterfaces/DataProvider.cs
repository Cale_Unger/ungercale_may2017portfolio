﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractClassesAndInterfaces
{
    interface DataProvider
    {
        /*
         * Contains method signatures - no implementation/no body
         * Interfaces can contatin the following items:
         *      Events
         *      Delegates
         *      Methods
         *      Properties
         * Interface is contract agreement by the class to implement methods defined in the interface file
         * Interfaces are good for when unrelated classes need similar bits of functionality
         */

        string[] GetData();
    }
}

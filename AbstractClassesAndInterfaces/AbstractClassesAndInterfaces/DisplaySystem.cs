﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractClassesAndInterfaces
{
    abstract class DisplaySystem
    {
        /*Abstract Classes
         *      Use like interfaces
         *      Can contain data
         *      Can contain implementations for some of their methods
         *      Can have privat methods / items
         *      Typically will contain 1 or more abstract methods
         *      Cannot be instantiated
         *      Children must implement the abstract items or they will also be abstract
         *      Typically used for large functional units of code that may contain some behaviors that are the same
         */

        public void Display(DataProvider dataSource)
        {
            if (IsTheCorrectType(dataSource))
            {
                DisplayData(dataSource);
            }
        }

        abstract protected bool IsTheCorrectType(DataProvider dataSource);

        abstract protected void DisplayData(DataProvider dataSource);
    }
}

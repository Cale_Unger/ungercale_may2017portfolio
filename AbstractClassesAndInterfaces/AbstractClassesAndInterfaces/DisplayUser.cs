﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractClassesAndInterfaces
{
    class DisplayUser : DisplaySystem
    {
        protected override bool IsTheCorrectType(DataProvider dataSource)
        {
            return dataSource is User;
        }

        protected override void DisplayData(DataProvider dataSource)
        {
            Console.WriteLine("User");
            Console.WriteLine("----");
            foreach(string line in dataSource.GetData())
            {
                Console.WriteLine(line);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace AbstractClassesAndInterfaces
{
    public class InputValidation
    {
        public InputValidation()
        {
        }
        public static bool GetBool(string message = "Please enter Y or N")
        {
            bool validateBool = true;
            bool keepAsking = true;
            string input = null;

            do
            {
                Console.WriteLine(message);
                input = Console.ReadLine();

                switch (input.ToLower())
                {
                    case "y":
                    case "yes":
                    case "true":
                    case "t":
                        {
                            validateBool = true;
                            keepAsking = false;
                        }
                        break;
                    case "n":
                    case "no":
                    case "false":
                    case "f":
                        {
                            validateBool = false;
                            keepAsking = false;
                        }
                        break;
                }
            }
            while (keepAsking);
            return validateBool;
        }
    }

    public static int GetInt(string message = "Enter an integer --> ")
    {
        int validateInt;
        string input = null;

        do
        {
            Console.WriteLine(message);
            input = Console.ReadLine();
        }
        while (!Int32.TryParse(input, out validateInt));
        {
            Console.WriteLine("Enter a valid integer -->");
        }
        return validateInt;
    }

    public static int GetInt(int min, int max, string message = "Enter an integer -->")
    {
        int validateInt;
        do
        {
            validateInt = GetInt(message);
        }
        while (validateInt <= min && validateInt >= max);
        {
        }
        return validateInt;
    }

    public static GetDouble(double min, double max, string message = "Enter a number -->")
    {
        double validatedDouble;
        string input = null;

        do
        {
            Console.Write(message);
            input = Console.ReadLine();
        }
        while (!Double.TryParse(input, out validatedDouble));
        {
            Console.WriteLine("Enter a valid number -->");
        }
        return validatedDouble;

    }
    }
}
while(!(Double.TryParse(input,out validatedDouble) && (validatedDouble >= min)
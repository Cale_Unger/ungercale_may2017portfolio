﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace AbstractClassesAndInterfaces
{
    class Preferences : DataProvider
    {
        bool _shouldAutoPlay;
        int _volumeLevel;

        public bool GetShouldAutoPlay()
        {
            return _shouldAutoPlay;
        }

        public int GetVolumeLevel()
        {
            return _volumeLevel;
        }
        
        public void SetShouldAutoPlay(bool shouldAutoPlay)
        {
            _shouldAutoPlay = shouldAutoPlay;
        }

        public void SetVolumeLevel(int volumeLevel)
        {
            _volumeLevel = volumeLevel;
        }

        public string[] GetData()
        {
            string[] data = new string[]
            {
                $"Auto Play: {GetShouldAutoPlay()}",
                $"Volume Level: {GetVolumeLevel()}"
            };
            return data;
        }
    }
}

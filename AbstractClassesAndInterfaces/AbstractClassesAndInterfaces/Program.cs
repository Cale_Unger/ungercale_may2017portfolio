﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractClassesAndInterfaces
{
    class Program
    {
        private DisplayUser dispUser;

        private Program()
        {
            dispUser = new DisplayUser();
        }

        static void Main(string[] args)
        {
            User currentUser = null;

            bool running = true;

            Program instance = new Program();

            while (running)
            {
                Console.Clear();
                Console.WriteLine("1. Create a user");
                Console.WriteLine("2. Set Preferences");
                Console.WriteLine("3. Display User");
                Console.WriteLine("4. Exit");
                Console.WriteLine("Select an option -->");
                string input = Console.ReadLine().ToLower();

                switch (input)
                {
                    case "1":
                    case "create a user":
                        {
                            currentUser = instance.CreateUser();
                        }
                        break;
                    case "2":
                    case "set preferences":
                        {

                        }
                        break;
                    case "3":
                    case "display user":
                        {
                            instance.DisplayUser(currentUser);
                        }
                        break;
                    case "4":
                    case "exit":
                        {
                            running = false;
                        }
                        break;
                    default:
                        {
                            Console.WriteLine("Enter a valid slection.");
                            Console.ReadKey();
                        }
                        break;
                }
            }
        }

        private User CreateUser()
        {
            User newUser = null;

            Console.WriteLine("1. User");
            Console.WriteLine("2. Super");
            Console.Write("What type of user is being created?");
            string input = Console.ReadLine();

            Console.Write("Enter the user's name -->");
            string uName = Console.ReadLine();

            Console.WriteLine("Enter the user's address -->");
            string uAddress = Console.ReadLine();

            int uAge = InputValidation.GetInt(18, 120, "Enter the user's age -->");

            switch (input)
            {
                case "1":
                case "user":
                    {
                        newUser = new User(uName, uAddress, uAge);
                    }
                    break;
                case "2":
                case "super":
                    {
                        newUser = new User(uName, uAddress, uAge);
                    }
                    break;
            }

            return null;
        }

        private void DisplayUser(User currentUser)
        {
            Console.WriteLine();
            if (currentUser == null)
            {
                Console.WriteLine("Please create a user first.");
            }
            else
            {
                dispUser.Display(currentUser);
            }
        }
    }
}

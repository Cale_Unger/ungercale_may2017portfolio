﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractClassesAndInterfaces
{
    class Super : User
    {
        public override int SecurityLevel
        {
            get
            {
                return 10;
            }
        }

        public Super(string name, string address, int age) : base(name, address, age)
        {

        }

        public void DisplayStatus()
        {
            Console.WriteLine("User Status: ");
        }
    }
}

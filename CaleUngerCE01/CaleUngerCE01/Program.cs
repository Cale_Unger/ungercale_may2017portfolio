﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUngerCE01
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Clear();

            //Declare variable
            string[] colors = new string[] { "red", "blue", "green", "yellow", "blue", "green", "blue", "blue", "red", "green", "yellow", "red" };
            float[] numbers = new float[] { 2.2f, 5.4f, 1.9f, 70.7f, 49.9f, 102.15f, 1.5f, 22.1f, 9.5f, 4.8f, 207.1f, 32.25f };
            string colorChoice;

            //Intro
            Console.Write("\nFish\n-----------------------------\n[");

            //This will output the array above.
            //No matter how it changes.

            int j = 0;
            for (j = 0; j < colors.Length;j++)
            {
                Console.Write(colors[j] + " ");
            }

            Console.Write("]");

            Console.Write("\n\nFish Sizes\n-----------------------------\n[");
            int n = 0;
            for (n =0; n < numbers.Length; n++)
            {
                Console.Write(numbers[n] + " ");
            }

            Console.Write("]");

            //User Input
            Console.Write("\n\nEnter one of the following colors: red, blue, green, or yellow --> ");
            colorChoice = Console.ReadLine();

            //Check for validation
            while (!(colorChoice == "red" || colorChoice == "blue" || colorChoice == "green" || colorChoice == "yellow"))
            {
                //Alert the user that something is wrong
                Console.WriteLine("\nPlease choose a correct color.");

                //Re-ask the question
                Console.Write("Enter one of the following colors: red, blue, green, or yellow --> ");

                //Re-catch the response using the same variable
                colorChoice = Console.ReadLine();
            }
            //Confirm the user's response
            Console.WriteLine($"\nGot it! You entered the color {colorChoice}.\n");

            //Checks to make sure that the current amount of fish are output based on the color chosen by the user.
            int i = 0;
            double size = 0;
            

            for(i = 0; i<colors.Length; i++)
            {
                if(colorChoice == colors[i] && size < numbers[i])
                {
                    size = numbers[i];
                }
            }

            Console.WriteLine($"The biggest {colorChoice} fish has a length of {size}.");

            Console.WriteLine("\nPress any key to exit.");
            Console.ReadKey();
        }
    }
    }


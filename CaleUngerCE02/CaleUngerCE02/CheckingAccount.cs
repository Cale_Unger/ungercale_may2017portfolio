﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUngerCE02
{
    class CheckingAccount
    {
        decimal _accountBalance;
        int _accountNumber;

        public CheckingAccount(int accountNumber, decimal accountBalance)
        {
            _accountNumber = accountNumber;
            _accountBalance = accountBalance;

        }

        public int AccountNumber
        {
            get
            {
                return _accountNumber;
            }
            set
            {
                _accountNumber = value;
            }
        }

        public decimal AccountBalance
        {
            get
            {
                return _accountBalance;
            }
            set
            {
                _accountBalance = value;
            }
        }

    }
}

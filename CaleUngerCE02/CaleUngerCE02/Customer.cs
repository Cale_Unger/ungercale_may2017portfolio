﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUngerCE02
{
    class Customer
    {
        CheckingAccount _checkingAccount;
        string _name;

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        internal CheckingAccount CheckingAccount
        {
            get
            {
                return _checkingAccount;
            }

            set
            {
                _checkingAccount = value;
            }
        }

        public Customer(CheckingAccount checkingAccount,string name)
        {
            CheckingAccount = checkingAccount;
            _name = name;

        }

        public void Display()
        {
            
            Console.WriteLine("Account Summary \n"
                            +"-------------------\n"
                            +$"Name:           {Name}\n"
                            +$"AccountNumber:  {CheckingAccount.AccountNumber}\n"
                            +$"AccountBalance: {CheckingAccount.AccountBalance.ToString("c")}");
                            
                         

        }
    }
}

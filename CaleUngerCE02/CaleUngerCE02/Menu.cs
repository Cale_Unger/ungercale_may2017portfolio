﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUngerCE02
{
    class Menu
    {
        public static void menuStart()
        {
            Customer customer = new Customer(null,null);

            bool running = true;

            while (running)
            {
                Console.WriteLine("SparxTrust ATM");
                Console.WriteLine("--------------");
                Console.WriteLine("[1] Create Customer");
                Console.WriteLine("[2] Create Account");
                Console.WriteLine("[3] Set Account Balance");
                Console.WriteLine("[4] Display Account Balance");
                Console.WriteLine("[5] Exit");

                Console.Write("\nSelection -->");
                string selection = Console.ReadLine();
                switch (selection.ToLower())
                {
                    case "1":
                    case "create customer":
                        {
                            
                            customer = new Customer(null, null);

                            Console.Clear();

                            Console.WriteLine("Create Customer");
                            Console.WriteLine("---------------");

                            Console.Write("Enter your name -->");
                            customer.Name = Console.ReadLine();
                            Console.WriteLine("\nPress any key to continue.");
                            Console.ReadKey();

                            Console.Clear();

                            Console.WriteLine("Account Summary");
                            Console.WriteLine("---------------");

                            Console.WriteLine($"Name:           {customer.Name}");
                            

                            Console.WriteLine("Press any key to return to the ATM menu.");
                            Console.ReadKey();

                            Console.Clear();
                        }
                        break;
                    case "2":
                    case "create account":
                        {
                            
                            customer.CheckingAccount = new CheckingAccount(0, 0);

                            Console.Clear();

                            string validateName;
                            string stringAccountNumber;
                            int accountNumber = 0;

                            if (customer.Name != null)
                            {
                                Console.WriteLine("Create Account");
                                Console.WriteLine("--------------");
                                Console.Write("Enter a customer for your account -->");
                                validateName = Console.ReadLine();

                                while (validateName != customer.Name)
                                {
                                    Console.WriteLine("Please enter a valid customer account name -->");
                                    validateName = Console.ReadLine();
                                }

                                Console.Clear();

                                Console.Write("Create an account number -->");
                                stringAccountNumber = Console.ReadLine();

                                while (!Int32.TryParse(stringAccountNumber, out accountNumber))
                                {
                                    Console.Write("Enter a positive number for your account number -->");
                                    stringAccountNumber = Console.ReadLine();
                                    
                                }

                                Console.Clear();

                                while (customer.CheckingAccount.AccountNumber < 0)
                                {
                                    Console.WriteLine("Enter a positive number for your account number -->");
                                    stringAccountNumber = Console.ReadLine();
                                    customer.CheckingAccount.AccountNumber = int.Parse(stringAccountNumber);
                                }
                                customer.CheckingAccount.AccountNumber = int.Parse(stringAccountNumber);

                                Console.Clear();

                                customer.Display();

                            }
                            if(customer.Name == null)
                            {
                                Console.WriteLine("You must make yourself a customer before creating an account.\n");
                            }

                            Console.WriteLine("Press any key to return the the ATM menu.");
                            Console.ReadKey();
                            Console.Clear();
                        }
                        break;
                    case "3":
                    case "set account balance":
                        {
                            Console.Clear();

                            string stringAccountBalance;
                            decimal accountBalance;

                            if(customer.CheckingAccount != null)
                            {
                                Console.WriteLine("Set Account Balance");
                                Console.WriteLine("-------------------");
                                Console.Write("\nEnter the amount to deposit -->");
                                stringAccountBalance = Console.ReadLine();

                                while (!decimal.TryParse(stringAccountBalance, out accountBalance))
                                {
                                    Console.Write("Enter a positive number to deposit -->");
                                    stringAccountBalance = Console.ReadLine();

                                }
                                customer.CheckingAccount.AccountBalance = decimal.Parse(stringAccountBalance);

                                Console.Clear();

                                while (customer.CheckingAccount.AccountBalance < 0)
                                {
                                    Console.WriteLine("Enter a positive number to deposit -->");
                                    stringAccountBalance = Console.ReadLine();
                                    customer.CheckingAccount.AccountBalance = decimal.Parse(stringAccountBalance);
                                }

                                customer.CheckingAccount.AccountBalance = decimal.Parse(stringAccountBalance);

                                customer.Display();
                            }
                            if (customer.CheckingAccount == null)
                            {
                                Console.WriteLine("You need an account to set a balance.\n");
                            }
                            Console.WriteLine("Press any key to return to the ATM menu.");
                            Console.ReadKey();
                            Console.Clear();
                        }
                        break;
                    case "4":
                    case "display account balance":
                        {
                            Console.Clear();
                            
                            if(customer.CheckingAccount != null)
                            {
                                customer.Display();
                            }
                            else
                            {
                                Console.WriteLine("You need to make an account first.");
                            }
                            Console.WriteLine("Press any key to return to the ATM menu.");
                            Console.ReadKey();

                            Console.Clear();
                        }
                        break;
                    case "5":
                    case "exit":
                        {
                            Console.Clear();
                            Console.WriteLine("Thanks for using SparxTrust ATM!");
                            Console.WriteLine("\nPress any key to exit.");
                            Console.ReadKey();
                            running = false;
                        }
                        break;
                    default:
                        {
                            Console.Clear();

                            Console.WriteLine("You didn't enter a valid selection.");
                            Console.WriteLine("\nPress any key to return to the ATM menu.");
                            Console.ReadKey();
                            Console.Clear();
                        }
                        break;
                }
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUngerCE03
{
    class Course
    {
        private string _title;
        private string _description;
        private Teacher _teacher;
        private string[] _students;

        public Course(string title, string description, Teacher teacher, string[] students)
        {
            _title = title;
            _description = description;
            _teacher = teacher;
            _students = students;
        }

        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                _title = value;
            }
        }

        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
            }
        }

        public Teacher Teacher
        {
            get
            {
                return _teacher;
            }
            set
            {
                _teacher = value;
            }
        }

        public string[] Students
        {
            get
            {
                return _students;
            }
            set
            {
                _students = value;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUngerCE03
{
    class Menu
    {
        public static void menuStart()
        {
            Student student = new Student("","",0,0);
            Teacher teacher = new Teacher("", "", 0, student[]);
            Course course = new Course("", "", teacher, student[]);
             
            while (true)
            {

                Console.WriteLine("Course Manager");
                Console.WriteLine("--------------");
                Console.WriteLine("[1] Create Course");
                Console.WriteLine("[2] Create Teacher");
                Console.WriteLine("[3] Add Students");
                Console.WriteLine("[4] Display");
                Console.WriteLine("[5] Exit");

                Console.Write("\nSelection -->");
                string selection = Console.ReadLine();
                switch (selection.ToLower())
                {
                    case "1":
                    case "create course":
                        {
                            Console.Clear();

                            Console.WriteLine("Create Course");
                            Console.WriteLine("---------------");

                            Console.Write("Enter a name for your course -->");
                            course.Title = Console.ReadLine();
                            Console.WriteLine("\nPress any key to continue.");
                            Console.ReadKey();

                            Console.Clear();

                            Console.WriteLine("Create Course");
                            Console.WriteLine("---------------");

                            Console.Write("Enter a description for your course -->");
                            course.Description = Console.ReadLine();
                            Console.WriteLine("\nPress any key to continue.");
                            Console.ReadKey();

                            Console.Clear();

                            Console.WriteLine("\nPress any key to continue.");
                            Console.ReadKey();

                            Console.Clear();
                        }
                        break;

                    case "2":
                    case "create teacher":
                        {
                            string stringAge = "";
                            
                            Console.Clear();

                            Console.WriteLine("Create Teacher");
                            Console.WriteLine("---------------");

                            Console.Write("Enter course teacher's name -->");
                            teacher.Name = Console.ReadLine();
                            Console.WriteLine("\nPress any key to continue.");
                            Console.ReadKey();

                            Console.Clear();

                            Console.WriteLine("Create Teacher");
                            Console.WriteLine("---------------");

                            Console.Write("Enter course teacher's description -->");
                            teacher.Description = Console.ReadLine();
                            Console.WriteLine("\nPress any key to continue.");
                            Console.ReadKey();

                            Console.Clear();

                            Console.WriteLine("Create Teacher");
                            Console.WriteLine("---------------");

                            Console.Write("Enter course teacher's age -->");
                            stringAge = Console.ReadLine();
                            teacher.Age = int.Parse(stringAge);
                            Console.WriteLine("\nPress any key to continue.");
                            Console.ReadKey();

                            Console.Clear();

                            for (int i = 0; i < teacher.knowledge.Length; i++)
                            {

                                Console.WriteLine("Create Teacher");
                                Console.WriteLine("---------------");

                                Console.WriteLine("Type \"done\" when finished.");
                                Console.Write("Enter course teacher's knowledge -->");
                                teacher.knowledge[i] = Console.ReadLine();

                                Console.Clear();
                                if (teacher.knowledge[i] == "done")
                                {
                                    Console.Clear();

                                    Console.WriteLine("\nPress any key to continue.");
                                    Console.ReadKey();
                                }
                                
                            }
                            
                        }
                        break;
                    default:
                        {
                            Console.Clear();

                            Console.WriteLine("You didn't enter a valid selection.");
                            Console.WriteLine("\nPress any key to return to the course manager menu.");
                            Console.ReadKey();
                            Console.Clear();
                        }
                        break;
                }
            }
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUngerCE04
{
    class Car
    {
        private string _make;
        private string _model;
        private string _color;
        private float _mileage;
        private int _year;

        public Car(string make, string model, string color, float mileage, int year)
        {
            _make = make;
            _model = model;
            _color = color;
            _mileage = mileage;
            _year = year;
        }

        public string Make
        {
            get
            {
                return _make;
            }
            set
            {
                _make = value;
            }
        }

        public string Model
        {
            get
            {
                return _model;
            }
            set
            {
                _model = value;
            }
        }

        public string Color
        {
            get
            {
                return _color;
            }
            set
            {
                _color = value;
            }
        }

        public float Mileage
        {
            get
            {
                return _mileage;
            }
            set
            {
                _mileage = value;
            }
        }

        public int Year
        {
            get
            {
                return _year;
            }
            set
            {
                _year = value;
            }
        }

        public void Drive(float mileage)
        {
            _mileage += mileage;
            Menu.GetLogger.LogW(_mileage.ToString());
            Console.WriteLine($"You have driven a total of {_mileage} miles.");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUngerCE04
{
    class LogToConsole : Logger
    {
        public override void Log(string log)
        {
            Console.WriteLine($"\nLine: {lineNumber+1}: {log}");
            lineNumber++;
        }

        public override void LogD(string logD)
        {
            Console.WriteLine($"\nLine: {lineNumber+1}: DEBUG: {logD}");
            lineNumber++;
        }

        public override void LogW(string logW)
        {
            Console.WriteLine($"\nLine: {lineNumber+1}: WARNING: {logW}");
            lineNumber++;
        }
    }
}

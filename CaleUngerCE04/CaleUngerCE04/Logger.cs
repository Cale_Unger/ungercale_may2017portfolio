﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUngerCE04
{
    abstract class Logger : ILog
    {
        protected static int lineNumber;

        public abstract void Log(string log);
        public abstract void LogD(string logD);
        public abstract void LogW(string logW);


    }
}

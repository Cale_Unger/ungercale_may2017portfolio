﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUngerCE04
{
    class Menu
    {

        static Logger logger = null;

        public static Logger GetLogger
        {
            get
            {
                return logger;
            }
            set
            {
                logger = value;
            }
        }

        public static void menuStart()
        {
            Car currentCar = null;

            LogToConsole logYes = new LogToConsole();
            DoNotLog logNo = new DoNotLog();

            logger = logNo;

            string logStatus = "";

            string go = "\nPress any key to continue.";

            bool running = true;

            while (running)
            {

                Console.WriteLine("Car Creator Simulator");
                Console.WriteLine("---------------------");
                Console.WriteLine("[1] Create a Car");
                Console.WriteLine("[2] Drive a Car");
                Console.WriteLine("[3] Destroy a Car");
                Console.WriteLine("[4] Enable Logging");
                Console.WriteLine("[5] Disable Logging");
                Console.WriteLine("[6] Exit");

                if (logger == logYes)
                {
                    logStatus = "On";
                }
                else if (logger == logNo)
                {
                    logStatus = "Off";
                }
                Console.WriteLine($"\nLogging: {logStatus}");

                Console.WriteLine();

                Console.Write("\nSelection --> ");
                string selection = Console.ReadLine();
                switch (selection.ToLower())
                {
                    case "1":
                    case "create a car":
                        {
                            currentCar = null;

                            string carMake;
                            string carModel;
                            string carColor;
                            string stringMileage;
                            float carMileage = 0.0f;
                            string stringYear;
                            int carYear;

                            string carHeader = "Create a Car\n---------------";

                            Console.Clear();

                            Console.WriteLine(carHeader);

                            Console.Write("Enter a make for your car --> ");
                            carMake = Console.ReadLine();

                            while (string.IsNullOrWhiteSpace(carMake))
                            {
                                Console.Write("Enter a make for your car --> ");
                                carMake = Console.ReadLine();
                            }

                            Console.WriteLine(go);
                            Console.ReadKey();

                            Console.Clear();

                            Console.WriteLine(carHeader);

                            Console.Write("Enter a model for your car --> ");
                            carModel = Console.ReadLine();

                            while (string.IsNullOrWhiteSpace(carModel))
                            {
                                Console.Write("Enter a model for your car --> ");
                                carMake = Console.ReadLine();
                            }

                            Console.WriteLine(go);
                            Console.ReadKey();

                            Console.Clear();

                            Console.WriteLine(carHeader);

                            Console.Write("Enter a color for your car --> ");
                            carColor = Console.ReadLine();

                            while (string.IsNullOrWhiteSpace(carColor))
                            {
                                Console.Write("Enter a color for your car --> ");
                                carMake = Console.ReadLine();
                            }

                            Console.WriteLine(go);
                            Console.ReadKey();

                            Console.Clear();

                            Console.WriteLine(carHeader);

                            Console.Write("Enter the mileage for your car --> ");
                            stringMileage = Console.ReadLine();

                            while (!float.TryParse(stringMileage, out carMileage))
                            {
                                Console.Write("Enter the mileage for your car --> ");
                                stringMileage = Console.ReadLine();
                            }

                            Console.WriteLine(go);
                            Console.ReadKey();

                            Console.Clear();

                            Console.WriteLine(carHeader);

                            Console.Write("Enter the year for your car --> ");
                            stringYear = Console.ReadLine();

                            while (!Int32.TryParse(stringYear, out carYear))
                            {
                                Console.Write("Enter the year for your car --> ");
                                stringYear = Console.ReadLine();
                            }

                            if (logger == logYes)
                            {
                                Console.Clear();
                                Console.WriteLine("Log");
                                Console.WriteLine("---------------------------------------------------");

                                logger.LogD(carMake);
                                Console.WriteLine($"Car make has been logged as: {carMake}.");

                                logger.LogD(carModel);
                                Console.WriteLine($"Car model has been logged as: {carModel}.");

                                logger.LogD(stringMileage);
                                Console.WriteLine($"Car mileage has been logged as: {carMileage}.");

                                logger.LogD(stringYear);
                                Console.WriteLine($"Car year has been logged as: {carYear}.");

                            }
                            else if (logger == logNo)
                            {
                                Console.WriteLine("Nothing has been logged.");
                            }

                            currentCar = new Car(carMake, carModel, carColor, carMileage, carYear);

                            Console.WriteLine(go);
                            Console.ReadKey();

                            Console.Clear();
                        }
                        break;

                    case "2":
                    case "drive a Car":
                        {
                            string stringDrive;
                            float carDrive;

                            if (currentCar != null)
                            {
                                Console.Clear();

                                Console.WriteLine("Drive the Car");
                                Console.WriteLine("---------------");

                                Console.Write("How far are you driving the car --> ");
                                stringDrive = Console.ReadLine();

                                while (!float.TryParse(stringDrive, out carDrive))
                                {
                                    Console.Write("How far are you driving the car --> ");
                                    stringDrive = Console.ReadLine();
                                }

                                currentCar.Drive(carDrive);

                                Console.WriteLine(go);
                                Console.ReadKey();

                                Console.Clear();
                            }
                            else
                            {
                                Console.Clear();

                                Console.WriteLine("You need to make a car to drive a car.");

                                Console.WriteLine(go);
                                Console.ReadKey();

                                Console.Clear();
                            }
                        }
                        break;
                    case "3":
                    case "destroy a car":
                        {
                            if (currentCar != null)
                            {
                                Console.Clear();

                                currentCar = null;

                                Console.WriteLine("The current car has been destroyed.");
                                Console.WriteLine(go);
                                Console.ReadKey();

                                Console.Clear();
                            }
                            else
                            {
                                Console.Clear();

                                Console.WriteLine("There isn't a car to be destroyed.");
                                Console.WriteLine(go);
                                Console.ReadKey();

                                Console.Clear();
                            }
                        }
                        break;
                    case "4":
                    case "enable logging":
                        {
                            Console.Clear();

                            logger = logYes;

                            Console.WriteLine("Logging has been enabled.");
                            Console.WriteLine(go);
                            Console.ReadKey();

                            Console.Clear();
                        }
                        break;
                    case "5":
                    case "disable logging":
                        {
                            Console.Clear();

                            logger = logNo;

                            Console.WriteLine("Logging has been disabled.");
                            Console.WriteLine(go);
                            Console.ReadKey();

                            Console.Clear();

                        }
                        break;
                    case "6":
                    case "exit":
                        {
                            Console.Clear();

                            Console.WriteLine("Thanks for using car creator simulator.");

                            Console.WriteLine("\nPress any key to continue.");
                            Console.ReadKey();

                            running = false;
                        }
                        break;
                    default:
                        {
                            Console.Clear();

                            Console.WriteLine("You've entered an invalid selection. Try again.");

                            Console.WriteLine(go);
                            Console.ReadKey();

                            Console.Clear();
                        }
                        break;
                }
            }
        }
    }
}

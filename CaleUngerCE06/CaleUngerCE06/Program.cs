﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUngerCE06
{
    class Program
    {
        static void Main(string[] args)
        {
            
            CollectionManager<Card> currentManager = new CollectionManager<Card>(0);

            bool running = true;

            while (running)
            {
                Console.Clear();

                Console.WriteLine("Cards");
                Console.WriteLine("---------------------");
                Console.WriteLine("[1] Create collection manager");
                Console.WriteLine("[2] Create a card");
                Console.WriteLine("[3] Add a card to a collection");
                Console.WriteLine("[4] Remove a card from a collection.");
                Console.WriteLine("[5] Display a collection.");
                Console.WriteLine("[6] Display all collections.");
                Console.WriteLine("[7] Exit");

                Console.Write("\nSelection -->");
                string selection = Console.ReadLine();
                switch (selection.ToLower())
                {
                    case "1":
                    case "create collection manager":
                        {
                            Console.Clear();

                            Console.Write("What should the collection be called -->");
                            string name = Console.ReadLine();

                            currentManager = new CollectionManager<Card>(currentManager.cards.Count);

                            Console.WriteLine("Collection has been created.");
                            Console.ReadKey();
                        }
                        break;
                    case "2":
                    case "create a card":
                        {
                            Console.Clear();

                            string stringValue;

                            if(currentManager != null)
                            {
                                Console.Write("Enter a name for your card -->");
                                string name = Console.ReadLine();

                                Console.Write("Enter a description for your card -->");
                                string description = Console.ReadLine();

                                Console.Write("Enter a value for your card -->");
                                stringValue = Console.ReadLine();

                                decimal value;
                                while (!decimal.TryParse(stringValue, out value))
                                {
                                    Console.Write("Enter a value for your card -->");
                                    stringValue = Console.ReadLine();
                                }

                                currentManager.cards.Add(new Card(name,description,value));

                                Console.WriteLine("\nPress any key to return to the menu.");
                                Console.ReadKey();
                            }
                            else
                            {
                                Console.WriteLine("You need to create a collection first.");
                                Console.ReadKey();
                            }
                        }
                        break;
                    case "3":
                    case "add a card to a collection":
                        {
                            Console.Clear();

                            if (currentManager != null)
                            {

                                int i = 1;
                                foreach (Card item in currentManager.cards)
                                {
                                    Console.WriteLine($"[{i++}] {item.Name}");
                                }

                                string cartItem;
                                if (currentManager.cards.Count > 0)
                                {
                                    Console.Write("\nSelect a card to add --> ");
                                    cartItem = Console.ReadLine();

                                    int itemNum;
                                    while (!int.TryParse(cartItem, out itemNum))
                                    {
                                        Console.WriteLine("\nPlease enter a positive number for the card you want to add --> ");
                                        cartItem = Console.ReadLine();
                                    }
                                    currentManager.cardsDictionary.Add(itemNum.ToString(), currentManager.cards);
                                    currentManager.cards.Remove(currentManager.cards[itemNum-1]);
                                    Console.WriteLine($"\nItem {itemNum} has been added to the collection.");
                                }
                                else
                                {
                                    Console.WriteLine("There are no items to add.");
                                }


                            }
                            else
                            {
                                Console.WriteLine("You need to create a collection first.");
                                Console.ReadKey();
                            }
                            Console.WriteLine("\nPress any key to return to the menu.");
                            Console.ReadKey();
                        }
                        break;
                    case "4":
                    case "remove a card from a collection":
                        {
                            int i = 0;
                            for (i=1; i<currentManager.cardsDictionary.Count;i++)
                            {
                                Console.WriteLine($"[{i++}] {currentManager.cardsDictionary}");
                            }

                            string cartItem;
                            if (currentManager.cardsDictionary.Count > 0)
                            {
                                Console.Write("\nSelect a card to remove --> ");
                                cartItem = Console.ReadLine();

                                int itemNum;
                                while (!int.TryParse(cartItem, out itemNum))
                                {
                                    Console.WriteLine("Please enter a positive number for the card you want to remove --> ");
                                    cartItem = Console.ReadLine();
                                }
                                currentManager.cards.Add(currentManager.cards[itemNum]);
                                currentManager.cardsDictionary.Remove(itemNum.ToString());
                                Console.WriteLine($"\nCard {itemNum} has been removed from the cart.");
                            }
                            else
                            {
                                Console.WriteLine("There are no cards to remove.");
                            }

                            Console.WriteLine("\nPress any key to return to the menu.");
                            Console.ReadKey();
                        }
                        break;
                    case "5":
                    case "Display a collection":
                        {
                            int i = 0;
                            for (i = 1; i < currentManager.cardsDictionary.Count; i++)
                            {
                                Console.WriteLine($"[{i++}] {currentManager.cardsDictionary}");
                            }
                        }
                        break;
                    case "6":
                    case "Display all collections":
                        {
                            Console.WriteLine(currentManager.cardsDictionary.Count);
                        }
                        break;
                    case "7":
                    case "exit":
                        {
                            Console.WriteLine("Thanks for using card collection creator!");
                            Console.ReadKey();
                            running = false;
                        }
                        break;
                }
            }
        }
    }
}

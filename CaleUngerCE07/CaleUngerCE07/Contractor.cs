﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUngerCE07
{
    class Contractor : Hourly
    {
        decimal _noBenefitsBonus = 0;

        public Contractor(string name, string address, decimal hoursPerWeek, decimal payPerHour, decimal noBenefitsBonus): base(name, address, hoursPerWeek, payPerHour)
        {
            _noBenefitsBonus = noBenefitsBonus;
        }

        public decimal NoBenefitsBonus
        {
            get
            {
                return _noBenefitsBonus;
            }
            set
            {
                _noBenefitsBonus = value;
            }
        }

        public override string CalculatePay()
        {
            int weeksPerYear = 52;
            decimal salary = (HoursPerWeek * PayPerHour) * (weeksPerYear);

            Console.WriteLine("\nEmployee Type: Contractor");
            Console.WriteLine($"Bonus: {NoBenefitsBonus.ToString("c")}");
            return base.CalculatePay();
        }
    }
}

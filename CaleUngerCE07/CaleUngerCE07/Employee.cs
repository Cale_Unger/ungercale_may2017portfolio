﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUngerCE07
{
    class Employee : IComparable<Employee>
    {
        string _name;
        string _address;

        public Employee(string name, string address)
        {
            _name = name;
            _address = address;
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public string Address
        {
            get
            {
                return _address;
            }
            set
            {
                _address = value;
            }
        }

        public int CompareTo(Employee name)
        {
            return Name.CompareTo(name.Name);
        }

        public virtual string CalculatePay()
        {
            Console.WriteLine($"Name: {Name}");
            Console.WriteLine($"Address: {Address}");

            return "";
        }
    }
}

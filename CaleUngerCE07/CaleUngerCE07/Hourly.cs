﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUngerCE07
{
    class Hourly : Employee
    {
        decimal _payPerHour;
        decimal _hoursPerWeek;

        public Hourly(string name, string address, decimal hoursPerWeek, decimal payPerHour): base(name, address)
        {
            _payPerHour = payPerHour;
            _hoursPerWeek = hoursPerWeek;
        }

        public decimal PayPerHour
        {
            get
            {
                return _payPerHour;
            }
            set
            {
                _payPerHour = value;
            }
        }

        public decimal HoursPerWeek
        {
            get
            {
                return _hoursPerWeek;
            }
            set
            {
                _hoursPerWeek = value;
            }
        }

        public override string CalculatePay()
        {
            int weeksPerYear = 52;
            decimal salary = (HoursPerWeek * PayPerHour) * weeksPerYear;
            Console.WriteLine($"Salary:  {salary.ToString("c")}");

            return base.CalculatePay();
        }
    }
}

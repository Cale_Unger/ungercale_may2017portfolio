﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUngerCE07
{
    class Manager : Salaried
    {
        decimal _bonus;

        public Manager(string name, string address, decimal salary, decimal bonus): base(name, address, salary)
        {
            _bonus = bonus;
        }

        public decimal Bonus
        {
            get
            {
                return _bonus;
            }
            set
            {
                _bonus = value;
            }
        }


        public override string CalculatePay()
        {

            Console.WriteLine("\nEmployee Type: Manager");
            Console.WriteLine($"Bonus: {Bonus.ToString("c")}");
            Console.WriteLine($"Name: {Name}");
            Console.WriteLine($"Address: {Address}");

            return base.CalculatePay();
        }
    }
    }

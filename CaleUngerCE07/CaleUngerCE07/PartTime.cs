﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUngerCE07
{
    class PartTime : Hourly
    {
        public PartTime(string name, string address, decimal hoursPerWeek, decimal payPerHour): base(name, address, hoursPerWeek, payPerHour)
        {
        }

        public override string CalculatePay()
        {
            Console.WriteLine("\nEmployee Type: Part Time");

            return base.CalculatePay();
        }
    }
}

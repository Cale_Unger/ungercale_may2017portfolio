﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUngerCE07
{
    class Program
    {
        static void Main(string[] args)
        {

            List<Employee> employees = new List<Employee>();

            bool runningMain = true;

            while (runningMain)
            {
                Console.Clear();

                Console.WriteLine("Employee Tracker");
                Console.WriteLine("----------------");
                Console.WriteLine("[1] Add Employee");
                Console.WriteLine("[2] Remove Employee");
                Console.WriteLine("[3] Display Payroll");
                Console.WriteLine("[4] Exit");

                Console.Write("\nSelect an option --> ");
                string selection = Console.ReadLine();
                switch (selection.ToLower())
                {
                    case "1":
                    case "add employee":
                        {
                            bool runningEmployee = true;
                            while (runningEmployee)
                            {
                                Console.Clear();

                                Console.WriteLine("Employee Types");
                                Console.WriteLine("------------------");
                                Console.WriteLine("[1] Full Time");
                                Console.WriteLine("[2] Part Time");
                                Console.WriteLine("[3] Contractor");
                                Console.WriteLine("[4] Salaried");
                                Console.WriteLine("[5] Manager");
                                Console.WriteLine("[6] Return to the main menu");

                                Console.Write("\nSelect an employee type --> ");
                                selection = Console.ReadLine();
                                switch (selection.ToLower())
                                {
                                    case "1":
                                    case "full time":
                                        {
                                            Console.Clear();

                                            Console.WriteLine("Employee Type - Full Time");
                                            Console.WriteLine("-------------------------");

                                            Console.Write("Please enter the full time employee's name --> ");
                                            string name = Console.ReadLine();

                                            Console.Write("\nEnter the full time employee's address --> ");
                                            string address = Console.ReadLine();

                                            Console.Write("\nEnter the full time employee's pay per hour --> ");
                                            string stringPayPerHour = Console.ReadLine();

                                            decimal payPerHour;
                                            while (!decimal.TryParse(stringPayPerHour, out payPerHour) || payPerHour < 0 || string.IsNullOrWhiteSpace(stringPayPerHour))
                                            {
                                                Console.Write("Enter a positive number for the full time employee's pay per hour --> ");
                                                stringPayPerHour = Console.ReadLine();
                                            }

                                            Console.WriteLine("\nA full time employee works 40 hours.");
                                            
                                            int hours = 40;

                                            employees.Add(new FullTime(name, address, hours, payPerHour));

                                            Console.WriteLine("\nPress any key to continue.");
                                            Console.ReadKey();
                                        }
                                        break;
                                    case "2":
                                    case "part time":
                                        {
                                            Console.Clear();

                                            Console.WriteLine("Employee Type - Part Time");
                                            Console.WriteLine("-------------------------");

                                            Console.Write("Please enter the part time employee's name --> ");
                                            string name = Console.ReadLine();

                                            Console.Write("\nEnter the part time employee's address --> ");
                                            string address = Console.ReadLine();

                                            Console.Write("\nEnter the part time employee's work hours --> ");
                                            string stringHoursPerWeek = Console.ReadLine();

                                            decimal hoursPerWeek;
                                            while (!decimal.TryParse(stringHoursPerWeek, out hoursPerWeek) || hoursPerWeek < 0 || string.IsNullOrWhiteSpace(stringHoursPerWeek))
                                            {
                                                Console.Write("Enter a positive number for the part time employee's work hours --> ");
                                                stringHoursPerWeek = Console.ReadLine();
                                            }

                                            Console.Write("\nEnter the part time employee's pay per hour --> ");
                                            string stringPayPerHour = Console.ReadLine();

                                            decimal payPerHour;
                                            while (!decimal.TryParse(stringPayPerHour, out payPerHour) || hoursPerWeek < 0 || string.IsNullOrWhiteSpace(stringPayPerHour))
                                            {
                                                Console.Write("Enter a positive number for the part time employee's pay per hour --> ");
                                                stringPayPerHour = Console.ReadLine();
                                            }

                                            employees.Add(new PartTime(name, address, hoursPerWeek, payPerHour));

                                            Console.WriteLine("\nPress any key to continue.");
                                            Console.ReadKey();
                                        }
                                        break;
                                    case "3":
                                    case "contractor":
                                        {
                                            Console.Clear();

                                            Console.WriteLine("Employee Type - Contractor");
                                            Console.WriteLine("--------------------------");

                                            Console.Write("Please enter the contractor's name --> ");
                                            string name = Console.ReadLine();

                                            Console.Write("\nEnter the contractor's address --> ");
                                            string address = Console.ReadLine();

                                            Console.Write("\nEnter the contractor's work hours --> ");
                                            string stringHoursPerWeek = Console.ReadLine();

                                            decimal hoursPerWeek;
                                            while (!decimal.TryParse(stringHoursPerWeek, out hoursPerWeek) || hoursPerWeek < 0 || string.IsNullOrWhiteSpace(stringHoursPerWeek))
                                            {
                                                Console.Write("Enter a positive number for the contractor's work hours --> ");
                                                stringHoursPerWeek = Console.ReadLine();
                                            }

                                            Console.Write("\nDoes the contractor get a bonus? (yes / no)");
                                            string benefitsBonus = Console.ReadLine();

                                            while (benefitsBonus.ToLower() != "yes" && benefitsBonus.ToLower() != "no")
                                            {
                                                Console.Write("Please enter \"yes\" or \"no\" --> ");
                                                benefitsBonus = Console.ReadLine();
                                            }

                                            decimal noBenefitsBonus;
                                            if (benefitsBonus.ToLower() == "yes")
                                            {
                                                Console.WriteLine("\nThis contractor will get a 10% bonus.");
                                                noBenefitsBonus = 0.10m;
                                            }
                                            else
                                            {
                                                Console.WriteLine("\nThis contractor will not get a bonus.");
                                                noBenefitsBonus = 0m;
                                            }

                                            Console.Write("\nEnter the constractor's pay per hour --> ");
                                            string stringPayPerHour = Console.ReadLine();

                                            decimal payPerHour;
                                            while (!decimal.TryParse(stringPayPerHour, out payPerHour) || hoursPerWeek < 0 || string.IsNullOrWhiteSpace(stringPayPerHour))
                                            {
                                                Console.Write("Enter a positive number for the contractor's pay per hour --> ");
                                                stringPayPerHour = Console.ReadLine();
                                            }

                                            decimal salary = (payPerHour * hoursPerWeek) * 52;
                                            decimal bonusCalculated = noBenefitsBonus * salary;

                                            employees.Add(new Contractor(name, address, hoursPerWeek, payPerHour, bonusCalculated));

                                            Console.WriteLine("\nPress any key to continue.");
                                            Console.ReadKey();
                                        }
                                        break;
                                    case "4":
                                    case "salaried":
                                        {
                                            Console.Clear();

                                            Console.WriteLine("Employee Type - Salaried");
                                            Console.WriteLine("------------------------");

                                            Console.Write("Enter the salaried employee's name --> ");
                                            string name = Console.ReadLine();

                                            Console.Write("\nEnter the salaried employee's address --> ");
                                            string address = Console.ReadLine();

                                            Console.Write("\nEnter the salaried employee's salary --> ");
                                            string stringSalary = Console.ReadLine();

                                            decimal salary;
                                            while (!decimal.TryParse(stringSalary, out salary) || salary < 0 || string.IsNullOrWhiteSpace(stringSalary))
                                            {
                                                Console.Write("Enter a positive number for the salaried employee's salary --> ");
                                                stringSalary = Console.ReadLine();
                                            }

                                            employees.Add(new Salaried(name, address, salary));

                                            Console.WriteLine("\nPress any key to continue.");
                                            Console.ReadKey();
                                        }
                                        break;
                                    case "5":
                                    case "manager":
                                        {
                                            Console.Clear();

                                            Console.WriteLine("Employee Type - Manager");
                                            Console.WriteLine("-----------------------");

                                            Console.Write("Please enter the manager's name --> ");
                                            string name = Console.ReadLine();

                                            Console.Write("\nEnter the manager's address --> ");
                                            string address = Console.ReadLine();
                                            
                                            Console.Write("\nEnter the manager's salary -->");
                                            string stringSalary = Console.ReadLine();

                                            decimal salary;
                                            while (!decimal.TryParse(stringSalary, out salary) || salary < 0 || string.IsNullOrWhiteSpace(stringSalary))
                                            {
                                                Console.Write("Enter a positive number for the manager's salary --> ");
                                                stringSalary = Console.ReadLine();
                                            }

                                            Console.WriteLine("Managers get a 10% bonus to their salary.");
                                            decimal bonus = 0.10m * salary;

                                            employees.Add(new Manager(name, address, salary, bonus));

                                            Console.WriteLine("\nPress any key to continue.");
                                            Console.ReadKey();
                                        }
                                        break;
                                    case "6":
                                    case "main menu":
                                        {
                                            Console.WriteLine("You will now be returned to the main menu.");
                                            Console.ReadKey();
                                            runningEmployee = false;
                                        }
                                        break;
                                    default:
                                        {
                                            Console.Clear();
                                            Console.WriteLine("Please enter a valid input.");
                                            Console.WriteLine("Press any key to return to the employee menu.");
                                            Console.ReadKey();
                                        }
                                        break;
                                }
                            }
                            break;
                        }
                    case "2":
                    case "remove employee":
                        {
                            Console.Clear();

                            Console.WriteLine("Remove Employee(s)");
                            Console.WriteLine("------------------");

                            employees.Sort();

                            for (int i = 0; i < employees.Count; i++)
                            {
                                Console.WriteLine($"{i+1} {employees[i].Name}");
                            }

                            string employee;
                            if(employees.Count > 0)
                            {
                                Console.Write("\nSelect an employee to remove --> ");
                                employee = Console.ReadLine();

                                int employeeNum;
                                while ((!int.TryParse(employee, out employeeNum)) || employeeNum <= 0 || employeeNum > employees.Count || string.IsNullOrWhiteSpace(employee))
                                {
                                    Console.WriteLine("Please enter a positive number for the employee you want to remove --> ");
                                    employee = Console.ReadLine();
                                }

                                employees.RemoveAt(employeeNum - 1);
                            }
                            else
                            {
                                Console.WriteLine("There are no employees to remove.");
                            }

                            Console.WriteLine("\nPress any key to continue");
                            Console.ReadKey();
                        }
                        break;
                    case "3":
                    case "display payroll":
                        {
                            Console.Clear();

                            Console.WriteLine("Display Payroll");
                            Console.WriteLine("------------------");

                            employees.Sort();
                            if (employees.Count != 0)
                            {
                                for (int i = 0; i < employees.Count; i++)
                                {
                                    Console.WriteLine($"{employees[i].CalculatePay()}");
                                }
                            }
                            else
                            {
                                Console.WriteLine("There aren't any employee payrolls to display.");
                            }
                            Console.WriteLine("\nPress any key to continue");
                            Console.ReadKey();
                        }
                        break;
                    case "4":
                    case "exit":
                        {
                            Console.Clear();

                            Console.WriteLine("Thanks for using Employee Tracker!");
                            Console.ReadKey();
                            runningMain = false;
                        }
                        break;
                    default:
                        {
                            Console.Clear();
                            Console.WriteLine("Please enter a valid input.");
                            Console.WriteLine("\nPress any key to return to the main menu.");
                            Console.ReadKey();
                        }
                        break;
                }
            }
        }
    }
}

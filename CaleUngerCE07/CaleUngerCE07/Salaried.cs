﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUngerCE07
{
    class Salaried : Employee
    {
        decimal _salary;

        public Salaried(string name, string address, decimal salary): base(name, address)
        {
            _salary = salary;
        }

        public decimal Salary
        {
            get
            {
                return _salary;
            }
            set
            {
                _salary = value;
            }
        }

        public override string CalculatePay()
        {
            Console.WriteLine("\nEmployee Type: Salaried");
            Console.WriteLine($"Salary: {Salary.ToString("c")}");

            return base.CalculatePay();
        }
    }
}

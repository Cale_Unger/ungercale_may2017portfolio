﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUngerCE08
{
    class Program
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.CreateDirectory(@"Input\");
            program.CreateDirectory(@"Output\");
            
            List<string> dataFields = new List<string>();

            List<string> file = new List<string>();

            string inputPath = "../../Files/Input/";
            string outputPath = "../../Files/Output/";

            bool running = true;

            while (running)
            {
                Console.Clear();

                Console.WriteLine("File Writer");
                Console.WriteLine("-----------");
                Console.WriteLine("[1] Write File 1");
                Console.WriteLine("[2] Write File 2");
                Console.WriteLine("[3] Write File 3");
                Console.WriteLine("[4] Exit");

                Console.Write("Enter a number to make your choice --> ");
                string selection = Console.ReadLine();

                switch (selection.ToLower())
                {
                    case "1":
                    case "write file 1":
                    case "2":
                    case "write file 2":
                    case "3":
                    case "write file 3":
                        {
                            try
                            {
                                using (StreamReader dataReader = new StreamReader(inputPath + "DataFieldsLayout.txt"))
                                {
                                    while (dataReader.EndOfStream == false)
                                    {
                                        string lines = dataReader.ReadLine();
                                        dataFields.Add(lines);
                                    }
                                }
                            }
                            catch (IOException)
                            {
                            }

                            try
                            {
                                using (StreamReader fileReader = new StreamReader($"{inputPath}DataFile{selection}.txt"))
                                {
                                    while (!fileReader.EndOfStream)
                                    {
                                        string lines = fileReader.ReadLine();
                                        file.Add(lines);
                                    }
                                    file.RemoveAt(file.Count - 1);
                                    file.RemoveAt(0);
                                }
                            }
                            catch (IOException)
                            {
                            }

                            try
                            {
                                using (StreamWriter sw = new StreamWriter($"{outputPath}JsonDataFile{selection}.json"))
                                {
                                    sw.Write("[");

                                    for (int i = 0; i < file.Count; i++)
                                    {
                                        string tFile = (string)file[i];
                                        string[] tFileArray = tFile.Split('|');

                                        sw.Write("{");

                                        for (int j = 0; j < file.Count; j++)
                                        {
                                            if (j != file.Count - 1)
                                            {
                                                sw.WriteLine($"\"{dataFields[j]}\" : \"{tFileArray[j]}\",");
                                            }
                                            else
                                            {
                                                sw.WriteLine($"\"{dataFields[j]}\" : \"{tFileArray[j]}\"");
                                            }
                                        }
                                        if (i != file.Count - 1)
                                        {
                                            sw.Write("},");
                                        }
                                        else
                                        {
                                            sw.Write("}");
                                        }
                                    }
                                    sw.WriteLine("]");
                                }
                                Console.Clear();
                                Console.WriteLine("The JSON file has been written.");

                                Console.WriteLine("\nPress any key to continue.");
                                Console.ReadLine();
                            }
                            catch (IOException)
                            {
                            }
                        }
                        break;
                    case "4":
                    case "exit":
                        {
                            Console.Clear();
                            Console.WriteLine("Thanks for using file writer.");
                            Console.WriteLine("\nPress any key to exit.");
                            Console.ReadKey();
                            running = false;
                        }
                        break;
                    default:
                        {
                            Console.WriteLine("Please enter valid input.");
                            Console.WriteLine("\nPress any key to continue.");
                            Console.ReadKey();
                        }
                        break;
                    }
                }
            }

            private void CreateDirectory(string directory)
            {

            if (Directory.Exists(directory))
            {
                Console.WriteLine($"dir \"{directory}\" already exists.");
            }
            else
            {
                Directory.CreateDirectory(directory);
                Console.WriteLine($"dir {directory} created.");
            }
        }

        private void DeleteDirectory(string directory)
        {
            Directory.Delete(directory);
        }
    }
}

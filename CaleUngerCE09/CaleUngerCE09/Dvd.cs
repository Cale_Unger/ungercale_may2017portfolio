﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUngerCE09
{
    class Dvd
    {

        string _title;
        decimal _price;
        float _rating;

        public Dvd(string title, decimal price, float rating)
        {
            _title = title;
            _price = price;
            _rating = rating;
        }

        public override string ToString()
        {
            return $"Title: {_title} Price: {_price.ToString("c")} Rating: {_rating}";
        }
    }
}
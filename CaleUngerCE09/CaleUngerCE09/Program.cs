﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MySql.Data.MySqlClient;
using System.IO;
using System.Data;

namespace CaleUngerCE09
{
    class Program
    {
        //Connection for the database
        MySqlConnection _con = null;

        Program()
        {
            //Initialize the connection field with a MySqlConnection object
            _con = new MySqlConnection();
        }

        static void Main(string[] args)
        {
            List<Dvd> inventory = new List<Dvd>();
            List<Dvd> shoppingCart = new List<Dvd>();

            bool running = true;

            //Create an instance of Program
            Program app = new Program();
            app.Connect();

            DataTable data = app.QueryDB("SELECT DVD_title, Price, publicRating FROM dvd LIMIT 20");
            DataRowCollection rows = data.Rows;

            foreach (DataRow row in rows)
            {
                /*
                Console.Write(row["make"].ToString() + " ");
                Console.WriteLine(row["model"].ToString() + " ");
                Console.WriteLine(row["year"].ToString());
                */

                inventory.Add(new Dvd(row["DVD_title"].ToString(), Convert.ToDecimal(row["Price"].ToString()), float.Parse(row["publicRating"].ToString())));
            }

            while (running)
            {
                Console.Clear();

                Console.WriteLine("DVD Shop");
                Console.WriteLine("-------------------------");
                Console.WriteLine("[1] View inventory");
                Console.WriteLine("[2] View shopping cart");
                Console.WriteLine("[3] Add dvd to cart");
                Console.WriteLine("[4] Remove dvd from cart");
                Console.WriteLine("[5] Exit");

                Console.Write("Selection -->");
                string selection = Console.ReadLine();

                switch (selection.ToLower())
                {
                    case "1":
                    case "view inventory":
                        {
                            Console.Clear();

                            Console.WriteLine("Inventory");
                            Console.WriteLine("-------------------------");

                            //Stops the increment variable (i) from going over 20
                            //or increasing everytime option 1 is selected
                            int i = 1;
                            if (i > 20)
                            {
                                i = 1;
                            }
                            else
                            {
                                for(i = 0; i< inventory.Count; i++)
                                {
                                Console.WriteLine($"[{i+1}] {inventory[i]}");
                                }
                            }
                            Console.WriteLine("\nPress any key to return to the menu.");
                            Console.ReadKey();
                        }
                        break;
                    case "2":
                    case "view shopping cart":
                        {
                            Console.Clear();

                            if (shoppingCart.Count != 0)
                            {
                                Console.Clear();

                                Console.WriteLine("Shopping Cart");
                                Console.WriteLine("-------------------------");
                                int i = 0;
                                for (i = 0; i < shoppingCart.Count; i++)
                                {
                                    Console.WriteLine($"[{i + 1}] {shoppingCart[i]}");
                                }

                                Console.WriteLine("Press any key to continue.");
                                Console.ReadKey();
                            }
                            else
                            {
                                Console.WriteLine("You have no items in your cart.");
                                Console.WriteLine("\nPress any key to return to the menu.");
                                Console.ReadKey();
                            }
                        }
                        break;
                    case "3":
                    case "add dvd to cart":
                        {
                            Console.WriteLine("Inventory");
                            Console.WriteLine("-------------------------");  

                            int i = 0;
                            for (i = 0; i < inventory.Count; i++)
                            {
                                Console.WriteLine($"[{i + 1}] {inventory[i]}");
                            }
                                string cartItem;
                            if (inventory.Count > 0)
                            {
                                Console.Write("\nSelect an item to add --> ");
                                cartItem = Console.ReadLine();

                                int itemNum;
                                while ((!int.TryParse(cartItem, out itemNum)) || itemNum < 0 || itemNum > inventory.Count || string.IsNullOrWhiteSpace(cartItem))
                                {
                                    Console.WriteLine("\nPlease enter a positive number for the item you want to add --> ");
                                    cartItem = Console.ReadLine();
                                }
                                shoppingCart.Add(inventory[itemNum-1]);
                                inventory.Remove(inventory[itemNum-1]);
                                Console.WriteLine($"\nItem {itemNum} has been added to the cart.");
                            }
                            else
                            {
                                Console.WriteLine("There are no items to add.");
                            }

                            Console.WriteLine("\nPress any key to return to the menu.");
                            Console.ReadKey();
                        }
                        break;
                    case "4":
                    case "remove dvd from cart":
                        {
                            Console.Clear();

                            Console.WriteLine("Remove from cart");
                            Console.WriteLine("------------------");

                            int i = 0;
                            for(i = 0; i< shoppingCart.Count; i++)
                            {
                                Console.WriteLine($"[{i+1}] {shoppingCart[i]}");
                            }

                            string cartItem;
                            if (shoppingCart.Count > 0)
                            {
                                Console.Write("\nSelect an item to remove --> ");
                                cartItem = Console.ReadLine();

                                int itemNum;
                                while ((!int.TryParse(cartItem, out itemNum)) || itemNum < 0 || itemNum > shoppingCart.Count || string.IsNullOrWhiteSpace(cartItem))
                                {
                                    Console.WriteLine("Please enter a positive number for the item you want to remove --> ");
                                    cartItem = Console.ReadLine();
                                }

                                inventory.Add(shoppingCart[itemNum-1]);
                                shoppingCart.Remove(shoppingCart[itemNum-1]);

                                Console.WriteLine($"\nItem {itemNum} has been removed from the cart.");
                            }
                            else
                            {
                                Console.WriteLine("There are no items to remove.");
                            }

                            Console.WriteLine("\nPress any key to return to the menu.");
                            Console.ReadKey();
                        }
                        break;
                    case "5":
                    case "exit":
                        {
                            Console.Clear();

                            Console.WriteLine("Thanks for using dvd shop.");
                            Console.WriteLine("\nPress any key to exit.");
                            Console.ReadKey();
                            running = false;
                        }
                        break;
                }
            }
        }

        void Connect()
        {
            BuildConnectionString();

            try
            {
                _con.Open();

                Console.WriteLine("Connection Successful!");
            }
            catch (MySqlException e)
            {
                string msg = "";
                switch (e.Number)
                {
                    default:
                    case 0:
                        {
                            msg = e.ToString();

                            break;
                        }
                    case 1042:
                        {
                            msg = "Can't resolve host address.\n" + _con.ConnectionString;

                            break;
                        }
                    case 1045:
                        {
                            msg = "Invalid username / password";
                            break;

                        }
                }
                Console.WriteLine(msg);
            }
        }

        void BuildConnectionString()
        {
            StringBuilder conString = new StringBuilder();
            conString.Append("Server=");

            using (StreamReader sr = new StreamReader("c:/VFW/connect.txt"))
            {
                string ip = sr.ReadLine();
                conString.Append($"{ip};");
            }

            conString.Append("uid=dbsAdmin;");
            conString.Append("pwd=pass;");
            conString.Append("database=example_1705;");
            conString.Append("port=8889;");

            _con.ConnectionString = conString.ToString();
        }

        DataTable QueryDB(string query)
        {
            MySqlDataAdapter adapt = new MySqlDataAdapter(query, _con);
            DataTable data = new DataTable();

            adapt.SelectCommand.CommandType = CommandType.Text;
            adapt.Fill(data);

            return data;
        }
    }
}

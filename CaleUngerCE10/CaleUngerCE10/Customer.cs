﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUngerCE10
{
    class Customer
    {
        string _name;
        int _age;
        List<Inventory> _shoppingCart = new List<Inventory>();

        public Customer(string name, int age, List<Inventory> shoppingCart)
        {
            _name = name;
            _age = age;
            _shoppingCart = shoppingCart;
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public List<Inventory> Cart
        {
            get
            {
                return _shoppingCart;
            }
            set
            {
                _shoppingCart = value;
            }
        }
    }
}

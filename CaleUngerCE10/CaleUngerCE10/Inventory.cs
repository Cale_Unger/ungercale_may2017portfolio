﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUngerCE10
{
    class Inventory
    {
        string _title;
        decimal _price;

        public Inventory(string title, decimal price)
        {
            _title = title;
            _price = price;
        }

        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                _title = value;
            }
        }

        public decimal Price
        {
            get
            {
                return _price;
            }
            set
            {
                _price = value;
            }
        }

        public override string ToString()
        {
            return $"Title: {_title} Price: {_price}";
        }
    }
}

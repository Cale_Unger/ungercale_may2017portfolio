﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUngerCE10
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Inventory> inventory = new List<Inventory>();
            Dictionary<string, Customer> customers = new Dictionary<string, Customer>();
            string name;
            int age;

            //Inventory
            //--------------------------------------------------------------------------------------
            inventory.Add(new Inventory("Jurassic Park", 10.00m));
            inventory.Add(new Inventory("Jurassic Park || : The Lost World", 10.00m));
            inventory.Add(new Inventory("Jurassic Park |||", 10.00m));
            inventory.Add(new Inventory("Jurassic Park Collection ", 25.00m));
            inventory.Add(new Inventory("Jurassic World", 20.00m));
            inventory.Add(new Inventory("Stars Wars", 10.00m));
            inventory.Add(new Inventory("The Empire Strikes Back", 10.00m));
            inventory.Add(new Inventory("Return of the Jedi", 10.00m));
            inventory.Add(new Inventory("Star Wars Trilogy: Original ", 25.00m));
            inventory.Add(new Inventory("Star Wars: Episode | - The Phantom Menace", 10.00m));
            inventory.Add(new Inventory("Star Wars: Episode || - Attack of the Clones", 10.00m));
            inventory.Add(new Inventory("Star Wars: Episode ||| - Revenge of the Sith", 10.00m));
            inventory.Add(new Inventory("Star Wars Trilogy: Prequels", 25.00m));
            inventory.Add(new Inventory("Star Wars: V|| - The Force Awakens", 20.00m));
            inventory.Add(new Inventory("The Park", 5.00m));
            inventory.Add(new Inventory("Jurassic Movie", 7.00m));
            inventory.Add(new Inventory("Dinosaur Park", 2.00m));
            inventory.Add(new Inventory("Space Wars", 11.00m));
            inventory.Add(new Inventory("Space Wars ||", 15.00m));
            inventory.Add(new Inventory("Space Wars |||", 10.00m));
            inventory.Add(new Inventory("Space Park", 22.00m));
            inventory.Add(new Inventory("Space Battle", 16.00m));
            inventory.Add(new Inventory("Zoo", 3.00m));
            inventory.Add(new Inventory("Space Zoo", 12.00m));
            inventory.Add(new Inventory("Space Base", 32.00m));
            inventory.Add(new Inventory("Dinosaurs in Space", 22.00m));
            inventory.Add(new Inventory("Star Base", 10.00m));
            inventory.Add(new Inventory("Galactic Battle", 8.00m));
            inventory.Add(new Inventory("Galaxy Force", 9.00m));
            inventory.Add(new Inventory("Jurassic Ark", 10.00m));
            inventory.Add(new Inventory("Ark Park", 10.00m));
            inventory.Add(new Inventory("Galactic Federation", 21.00m));
            inventory.Add(new Inventory("Space Thing", 4.00m));
            inventory.Add(new Inventory("Name of movie with space", 10.00m));
            inventory.Add(new Inventory("Name of movie with dinosaurs", 10.00m));
            inventory.Add(new Inventory("Dinosaur", 6.00m));
            inventory.Add(new Inventory("Place in space", 10.00m));
            inventory.Add(new Inventory("Original Movie Name", 12.00m));
            inventory.Add(new Inventory("Sequel Movie", 10.00m));
            inventory.Add(new Inventory("Movie with that one thing", 10.00m));
            inventory.Add(new Inventory("Movie about a thing", 30.00m));
            inventory.Add(new Inventory("Space Jam", 13.00m));
            inventory.Add(new Inventory("Jamming in Space", 17.00m));
            inventory.Add(new Inventory("Park Jam", 10.00m));
            inventory.Add(new Inventory("Dinosaur Train", 19.00m));
            inventory.Add(new Inventory("Prehistoric Park Place", 18.00m));
            inventory.Add(new Inventory("Space Place", 10.00m));
            inventory.Add(new Inventory("Some place in space", 18.00m));
            inventory.Add(new Inventory("Space Station", 11.00m));
            inventory.Add(new Inventory("Jurassic Era", 10.00m));
            inventory.Add(new Inventory("Dino Time", 10.00m));
            //--------------------------------------------------------------------------------------

            bool running = true;

            while (running)
            {
                Console.Clear();

                Console.WriteLine("Action Replay Video Store");
                Console.WriteLine("--------------------------");
                Console.WriteLine("[1] Select current shopper");
                Console.WriteLine("[2] View store inventory");
                Console.WriteLine("[3] View cart");
                Console.WriteLine("[4] Add to cart");
                Console.WriteLine("[5] Remove from cart");
                Console.WriteLine("[6] Complete purchase");
                Console.WriteLine("[7] Exit");

                Console.Write("Selection --> ");
                string selection = Console.ReadLine();

                switch (selection.ToLower())
                {
                    case "1":
                    case "select current shopper":
                        {
                            bool stillRunning = true;

                            if(customers.Count() != 0)
                            {
                                Console.WriteLine();
                                Console.WriteLine($"There are currently {customers.Count()}.");
                                Console.WriteLine("Would you like to: \n[1] Create a new shopper \n[2] Continue shopping with a current shopper \n[3] Exit");

                                Console.WriteLine("Selection --> ");
                                string stillSelecting = Console.ReadLine();

                                while (stillRunning)
                                {
                                    switch (stillSelecting)
                                    {
                                        case "1":
                                        case "create shopper":
                                            {
                                                Console.WriteLine("\nCreate Shopper");
                                                Console.WriteLine("--------------");
                                                Console.Write("Enter the shoppers name --> ");
                                                name = Console.ReadLine();

                                                Console.Write("Enter the shoppers age --> ");
                                                string cAge = Console.ReadLine();

                                                while (!int.TryParse(cAge, out age))
                                                {
                                                    Console.Write("Enter the shoppers age --> ");
                                                    cAge = Console.ReadLine();
                                                }
                                                customers.Add(name, new Customer(name, age, null));
                                            }
                                            break;
                                        case "2":
                                        case "continue shopping":
                                            {
                                                Console.Clear();

                                                Console.WriteLine("Inventory");
                                                Console.WriteLine("-------------------------");

                                                //Stops the increment variable (i) from going over 50
                                                //or increasing everytime option 1 is selected
                                                int i = 1;
                                                if (i > 50)
                                                {
                                                    i = 1;
                                                }
                                                else
                                                {

                                                    foreach (Inventory item in inventory)
                                                    {
                                                        Console.WriteLine($"[{i++}] Title: {item.Title} Price: {item.Price.ToString("c")}");
                                                    }
                                                }

                                                Console.WriteLine("\nPress any key to return to the menu.");
                                                Console.ReadKey();
                                            }
                                            break;
                                    }
                                }
                            }
                            else
                            {
                                Console.WriteLine("Information");
                                Console.WriteLine("-----------");
                                Console.WriteLine("There are currently no shoppers.");
                                Console.WriteLine("You can: Create a shopper.");
                                
                                Console.WriteLine("\nCreate Shopper");
                                Console.WriteLine("--------------");
                                Console.Write("Enter the shoppers name --> ");
                                name = Console.ReadLine();

                                Console.Write("Enter the shoppers age --> ");
                                string cAge = Console.ReadLine();

                                
                                while(!int.TryParse(cAge, out age))
                                {
                                    Console.Write("Enter the shoppers age --> ");
                                    cAge = Console.ReadLine();
                                }
                                customers.Add(name, new Customer(name, age, null));
                            }
                        }
                        break;
                    case "2":
                    case "view store inventory":
                        {
                            Console.Clear();

                            Console.WriteLine("Inventory");
                            Console.WriteLine("-------------------------");

                            //Stops the increment variable (i) from going over 50
                            //or increasing everytime option 1 is selected
                            int i = 1;
                            if (i > 50)
                            {
                                i = 1;
                            }
                            else
                            {
                                
                                foreach (Inventory item in inventory)
                                {
                                    Console.WriteLine($"[{i++}] Title: {item.Title} Price: {item.Price.ToString("c")}");
                                }
                            }

                            Console.WriteLine("Enter the number for the item you would like to add to your cart -->");
                            string id = Console.ReadLine();

                            int num;
                            while(!int.TryParse(id, out num))
                            {
                                Console.WriteLine("Enter the number for the item you would like to add to your cart -->");
                                id = Console.ReadLine();
                            }

                            Customer temp = customers[id];

                            Console.WriteLine($"{num} has been added to the cart.");

                            Console.WriteLine("\nPress any key to return to the menu.");
                            Console.ReadKey();
                        }
                        break;
                    case "3":
                    case "view cart":
                        {
                            for(int i = 0;i <)

                            Console.WriteLine("The Cart is empty.");
                            Console.ReadLine();
                        }
                        break;
                    case "4":
                    case "add to cart":
                        break;
                    case "5":
                    case "7":
                    case "exit":
                        {
                            Console.WriteLine("Thanks for using the Action Replay Rental Store.");
                            Console.WriteLine("Press any key to exit.");
                            Console.ReadKey();
                            running = false;
                        }
                        break;
                    default:
                        {
                            Console.WriteLine("Please enter a valid selection.");
                            Console.WriteLine("Press any key to return to menu.");
                            Console.ReadKey();
                        }
                        break;
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUnger_CE01
{
    class Paper
    {
        int mPaperInt;
        long mPaperLong;
        short mPaperShort;

        Rock rock;
        Scissors scissors;

        public Paper()
        {
            Console.WriteLine("Paper default constructor");
        }

        public Paper(int _paperInt, long _paperLong, short _paperShort)
        {
            mPaperInt = _paperInt;
            mPaperLong = _paperLong;
            mPaperShort = _paperShort;

            Console.WriteLine("Paper 3 Parameter Constructor");
        }

        public Paper(Rock _rock)
        {
            this.rock = _rock;
            Console.WriteLine("Paper with parameter Rock");
        }

        public Paper(Scissors _scissors)
        {

            this.scissors = _scissors;
            Console.WriteLine("Paper with parameter Scissors");
        }
        public void InRockCreateScissors(Rock newRock)
        {
            this.scissors = new Scissors(newRock);
            Console.WriteLine("Paper method with parameter Rock");
        }
        public void InScissorsCreateRock(Scissors newScissors)
        {
            this.rock = new Rock(newScissors);
            Console.WriteLine("Paper method with parameter Scissors");
        }
        public void InBothCreateBoth(Rock bothRock, Scissors bothScissors)
        {
            this.InRockCreateScissors(bothRock);
            this.InScissorsCreateRock(bothScissors);
            Console.WriteLine("Paper calling both methods");

        }

    }
}

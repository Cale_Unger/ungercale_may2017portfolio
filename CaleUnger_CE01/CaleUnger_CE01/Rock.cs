﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUnger_CE01
{
    class Rock
    {
        int mRockInt;
        bool mRockBool;
        string mRockString;

        Paper paper;
        Scissors scissors;

        public Rock()
        {
            Console.WriteLine("Rock default constructor");
        }

        public Rock(int _rockInt, bool _rockBool, string _rockString)
        {
            mRockInt = _rockInt;
            mRockBool = _rockBool;
            mRockString = _rockString;

            Console.WriteLine("Rock 3 Parameter Constructor");
        }

        public Rock(Paper _paper)
        {
            this.paper = _paper;
            Console.WriteLine("Rock with parameter Paper");
        }

        public Rock(Scissors _scissors)
        {
            this.scissors = _scissors;
            Console.WriteLine("Rock with parameter Scissors");
        }

        public void InPaperCreateScissors(Paper newPaper)
        {
            this.scissors = new Scissors(newPaper);
            Console.WriteLine("Rock method with parameter Paper");
        }
        public void InScissorsCreatePaper(Scissors newScissors)
        {
            this.paper = new Paper(newScissors);
            Console.WriteLine("Rock method with parameter Scissors");
        }
        public void InBothCreateBoth(Paper bothPaper, Scissors bothScissors)
        {
            this.InPaperCreateScissors(bothPaper);
            this.InScissorsCreatePaper(bothScissors);
            Console.WriteLine("Rock calling both methods");

        }
    }
}
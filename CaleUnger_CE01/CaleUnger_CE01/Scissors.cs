﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUnger_CE01
{
    class Scissors
    {
        double mScissorDouble;
        decimal mScissorDecimal;
        byte mScissorByte;

        Rock rock;
        Paper paper;

        public Scissors()
        {
            Console.WriteLine("Scissors default constructor");
        }

        public Scissors(double _scissorDouble, decimal _scissorDecimal, byte _scissorByte)
        {
            mScissorDouble = _scissorDouble;
            mScissorDecimal = _scissorDecimal;
            mScissorByte = _scissorByte;

            Console.WriteLine("Scissors 3 Parameter Constructor");
        }

        public Scissors(Rock _rock)
        {
            this.rock = _rock;
            Console.WriteLine("Scissors with parameter Rock");
        }

        public Scissors(Paper _paper)
        {
            this.paper = _paper;
            Console.WriteLine("Scissors with parameter Paper");
        }
        public void InRockCreatePaper(Rock newRock)
        {
            this.paper = new Paper(newRock);
            Console.WriteLine("Scissors method with parameter Rock");
        }
        public void InPaperCreateRock(Paper newPaper)
        {
            this.rock = new Rock(newPaper);
            Console.WriteLine("Scissors method with parameter Paper");
        }
        public void InBothCreateBoth(Rock bothRock, Paper bothPaper)
        {
            this.InRockCreatePaper(bothRock);
            this.InPaperCreateRock(bothPaper);
            Console.WriteLine("Scissors calling both methods");

        }
    }
}

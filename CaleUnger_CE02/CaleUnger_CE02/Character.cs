﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUnger_CE02
{
    class Character
    {
        private string _name;
        private int _baseAttack;
        private int _health;
        private int _speed;
        private int _stamina;
        private int _strength;
        private int _charisma;
        private int _gold;
        private Weapon _weapon;

        public Character(string name, int baseAttack, int health, int speed, int stamina, int strength, int charisma, int gold) : this(name, baseAttack, health, speed, stamina, strength, charisma, gold, null)
        {

        }
        
        public Character(string name, int baseAttack, int health, int speed, int stamina, int strength, int charisma, int gold, Weapon weapon)
        {
            _name = name;
            _baseAttack = baseAttack;
            _health = health;
            _speed = speed;
            _stamina = stamina;
            _strength = strength;
            _charisma = charisma;
            _gold = gold;
            _weapon = weapon;

        }

        public string enteredName
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public string name
        {
            get
            {
                return $"{ _name}";
            }
        }

        public int speed
        {
            get
            {
                return _speed;
            }
            set
            {
                _speed = value;
            }
        }

        public int stamina
        {
            get
            {
                return _stamina;
            }
            set
            {
                _stamina = value;
            }
        }

        public int strength
        {
            get
            {
                return _strength;
            }
            set
            {
                _strength = value;
            }
        }

        public int charisma
        {
            get
            {
                return _charisma;
            }
            set
            {
                _charisma = value;
            }
        }

        public Weapon weapon
        {
            set
            {
                _weapon = value;
            }
        }
        
        //Output information from class
        public void Display()
        {
            // cw + tab(x2)
            Console.WriteLine("Character Info\n"
                            + "---------------\n"
                            + $"Name:        {name}\n"
                            + $"Attack:      {_baseAttack}\n"
                            + $"Health:      {_health}\n"
                            + $"Speed:       {_speed}\n"
                            + $"Stamina      {_stamina}\n"
                            + $"Strength     {_strength}\n"
                            + $"Charisma     {_charisma}\n");
                            
        }
    }
}

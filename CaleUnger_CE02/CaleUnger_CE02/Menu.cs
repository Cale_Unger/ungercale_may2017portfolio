﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUnger_CE02
{
    class Menu
    {
        public static void menuStart()
        {

            while (true)
            {
                int speed = 0;
                int stamina = 0;
                int strength = 0;
                int charisma = 0;
                
                string speedString = "";
                string staminaString = "";
                string strengthString = "";
                string charismaString = "";

                int attributeMath = 10;

                Console.WriteLine("Character Menu");
                Console.WriteLine("--------------");
                Console.WriteLine("[1] Create Character");
                Console.WriteLine("[2] Modify Character");
                Console.WriteLine("[3] Modify Weapon");
                Console.WriteLine("[4] Show Character Stats");
                Console.WriteLine("[5] Exit");

                string selection = Console.ReadLine();
                switch (selection.ToLower())
                {
                    case "1":
                    case "create character":
                        {
                            Console.Clear();

                            Character character;
                            Weapon weapon;

                            character = new Character("", MyAPI.userGetRandomInt(1,10), 100, 0, 0, 0, 0, 1000);
                            weapon = new Weapon("", MyAPI.getRandomInt(), "");

                            Console.Write("Enter a name for your character -->");
                            character.enteredName = Console.ReadLine();
                            Console.WriteLine("Press any key to continue.");
                            Console.ReadKey();

                            Console.Clear();

                            Console.Write("Enter a name for your weapon -->");
                            weapon.weaponName = Console.ReadLine();
                            Console.WriteLine("Press any key to continue.");
                            Console.ReadKey();

                            Console.Clear();

                            Console.Write("Enter an element type for your weapon -->");
                            weapon.element = Console.ReadLine();
                            Console.WriteLine("Press any key to continue.");
                            Console.ReadKey();

                            Console.Clear();

                            while (attributeMath > 0)
                            {
                                Console.WriteLine("You have 10 attribute points to spend over these four attributes.");
                                Console.WriteLine("Attributes");
                                Console.WriteLine("----------");
                                Console.WriteLine("Speed\nStamina\nStrength\nCharisma");
                                Console.WriteLine("\nPress any key to continue.");
                                Console.ReadKey();

                                Console.Clear();

                                Console.WriteLine("\nYou have {0} attribute points remaining.\n", attributeMath);
                                Console.Write("Speed --> ");
                                speedString = Console.ReadLine();

                                while (!(int.TryParse(speedString, out speed)))
                                {
                                    Console.WriteLine("\nYou have {0} attribute points remaining.\n", attributeMath);
                                    Console.Write("Speed --> ");
                                    speedString = Console.ReadLine();
                                }
                                character.speed = speed;

                                Console.Clear();

                                Console.WriteLine("\nYou have {0} attribute points remaining.\n", attributeMath = attributeMath - speed);
                                Console.Write("Stamina -->");
                                staminaString = Console.ReadLine();

                                while (!(int.TryParse(staminaString, out stamina)))
                                {
                                    Console.WriteLine("\nYou have {0} attribute points remaining.\n", attributeMath);
                                    Console.Write("Stamina --> ");
                                    staminaString = Console.ReadLine();
                                }
                                character.stamina = stamina;

                                Console.Clear();

                                Console.WriteLine("\nYou have {0} attribute points remaining.\n", attributeMath = attributeMath - stamina);
                                Console.Write("Strength --> ");
                                strengthString = Console.ReadLine();

                                while (!(int.TryParse(strengthString, out strength)))
                                {
                                    Console.WriteLine("\nYou have {0} attribute points remaining.\n", attributeMath);
                                    Console.Write("Strength --> ");
                                    strengthString = Console.ReadLine();
                                }
                                character.strength = strength;

                                Console.Clear();

                                Console.WriteLine("\nYou have {0} attribute points remaining.\n", attributeMath = attributeMath - strength);
                                Console.Write("Charisma --> ");
                                charismaString = Console.ReadLine();

                                while (!(int.TryParse(charismaString, out charisma)))
                                {
                                    Console.WriteLine("You have {0} attribute points remaining.\n", attributeMath);
                                    Console.Write("Charisma --> ");
                                    charismaString = Console.ReadLine();
                                }
                                character.charisma = charisma;
                            }
                            character.weapon = weapon;

                            Console.Clear();

                            character.Display();
                            Console.WriteLine();
                            weapon.Display();

                            Console.WriteLine("Press any key to return to the character menu.");
                            Console.ReadKey();

                            Console.Clear();
                        }
                        break;
                    case "2":
                    case "modify character":
                        {
                            Console.Clear();

                            Character character;
                            Weapon weapon;

                            character = new Character("", MyAPI.userGetRandomInt(1, 10), 100, 0, 0, 0, 0, 1000);
                            weapon = new Weapon("", MyAPI.getRandomInt(), "");

                            Console.Write("Enter a name for your character -->");
                            character.enteredName = Console.ReadLine();
                            Console.WriteLine("Press any key to continue.");
                            Console.ReadKey();

                            Console.Clear();

                            Console.Write("Enter a name for your weapon -->");
                            weapon.weaponName = Console.ReadLine();
                            Console.WriteLine("Press any key to continue.");
                            Console.ReadKey();

                            Console.Clear();

                            Console.Write("Enter an element type for your weapon -->");
                            weapon.element = Console.ReadLine();
                            Console.WriteLine("Press any key to continue.");
                            Console.ReadKey();

                            Console.Clear();

                            while (attributeMath > 0)
                            {
                                Console.WriteLine("You have 10 attribute points to spend over these four attributes.");
                                Console.WriteLine("Attributes");
                                Console.WriteLine("----------");
                                Console.WriteLine("Speed\nStamina\nStrength\nCharisma");
                                Console.WriteLine("\nPress any key to continue.");
                                Console.ReadKey();

                                Console.Clear();

                                Console.WriteLine("\nYou have {0} attribute points remaining.\n", attributeMath);
                                Console.Write("Speed --> ");
                                speedString = Console.ReadLine();

                                while (!(int.TryParse(speedString, out speed)))
                                {
                                    Console.WriteLine("\nYou have {0} attribute points remaining.\n", attributeMath);
                                    Console.Write("Speed --> ");
                                    speedString = Console.ReadLine();
                                }
                                character.speed = speed;

                                Console.Clear();

                                Console.WriteLine("\nYou have {0} attribute points remaining.\n", attributeMath = attributeMath - speed);
                                Console.Write("Stamina -->");
                                staminaString = Console.ReadLine();

                                while (!(int.TryParse(staminaString, out stamina)))
                                {
                                    Console.WriteLine("\nYou have {0} attribute points remaining.\n", attributeMath);
                                    Console.Write("Stamina --> ");
                                    staminaString = Console.ReadLine();
                                }
                                character.stamina = stamina;

                                Console.Clear();

                                Console.WriteLine("\nYou have {0} attribute points remaining.\n", attributeMath = attributeMath - stamina);
                                Console.Write("Strength --> ");
                                strengthString = Console.ReadLine();

                                while (!(int.TryParse(strengthString, out strength)))
                                {
                                    Console.WriteLine("\nYou have {0} attribute points remaining.\n", attributeMath);
                                    Console.Write("Strength --> ");
                                    strengthString = Console.ReadLine();
                                }
                                character.strength = strength;

                                Console.Clear();

                                Console.WriteLine("\nYou have {0} attribute points remaining.\n", attributeMath = attributeMath - strength);
                                Console.Write("Charisma --> ");
                                charismaString = Console.ReadLine();

                                while (!(int.TryParse(charismaString, out charisma)))
                                {
                                    Console.WriteLine("You have {0} attribute points remaining.\n", attributeMath);
                                    Console.Write("Charisma --> ");
                                    charismaString = Console.ReadLine();
                                }
                                character.charisma = charisma;
                            }
                            character.weapon = weapon;

                            Console.Clear();

                            character.Display();
                            Console.WriteLine();
                            weapon.Display();

                            Console.WriteLine("Press any key to return to the character menu.");
                            Console.ReadKey();

                            Console.Clear();
                        }
                        break;
                    case "3":
                    case "modify weapon":
                        {
                            Console.Clear();

                            Character character;
                            Weapon weapon;

                            character = new Character("", MyAPI.userGetRandomInt(1,10), 100, 0, 0, 0, 0, 1000);
                            weapon = new Weapon("", MyAPI.getRandomInt(), "");

                            Console.Write("Enter a name for your new weapon -->");
                            weapon.weaponName = Console.ReadLine();

                            Console.WriteLine("Do you want to re-roll your weapon stats?");
                            string answer = Console.ReadLine();

                            if (answer == "yes")
                            {
                                weapon = new Weapon("", MyAPI.getRandomInt(), "");
                            }
                            else
                            {
                            }

                            Console.Write("Enter an element type for your weapon -->");
                            weapon.element = Console.ReadLine();

                            character.weapon = weapon;

                            Console.Clear();

                            character.Display();
                            weapon.Display();
                            Console.WriteLine("\nPress any key to return to the character menu.");
                            Console.ReadKey();

                            Console.Clear();
                        }
                        break;
                    case "4":
                    case "show stats":
                        {
                            Console.Clear();

                            Character character;
                            Weapon weapon;

                            character = new Character("", MyAPI.userGetRandomInt(1,10), 100, 0, 0, 0, 0, 1000);
                            weapon = new Weapon("", MyAPI.getRandomInt(), "");

                            character.Display();
                            weapon.Display();
                            Console.WriteLine("\nPress any key to return to the character menu.");
                            Console.ReadKey();

                            Console.Clear();
                        }
                        break;
                    case "5":
                    case "exit":
                        {
                            Console.Clear();

                            Console.WriteLine("Thanks for playing!");
                            Console.ReadKey();
                            Environment.Exit(0);
                        }
                        break;
                    default:
                        {
                            Console.Clear();

                            Console.WriteLine("You didn't enter a valid selection.");
                            Console.ReadKey();
                        }
                        break;
                }
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUnger_CE02
{
    class MyAPI
    {
        public static double getHeron(double a, double b, double c)
        {
            double s = (a + b + c) / 2.0;
            return Math.Sqrt(s * (s - a) * (s - b) * (s - c));
        }

        public static bool isTriangle(double a, double b, double c)
        {
            if (a + b > c && b + c > a && a + c > b)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static double getDouble(string prompt)
        {
            Console.WriteLine(prompt);
            double num = double.Parse(prompt);
            return num;
        }

        public static int userGetRandomInt(int start, int stop)
        {
            int randomNum = 1;
            int range = stop - start;
            Random random = new Random();
            randomNum = (random.Next((range + 1)) + start);
            return randomNum;
        }

        public static int getRandomInt()
        {
            
            Random random = new Random();
            int randomInt = random.Next(0, 100);
            return randomInt;
        }

        public static double getDistance(double x1, double y1, double x2, double y2)
        {
            double distance = Math.Sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
            return distance;
        }

        public static string getString(string prompt)
        {

            Console.WriteLine(prompt);
            string data = Console.ReadLine();
            return data;
        }

        public static int getInt(string prompt)
        {
            Console.WriteLine(prompt);
            int num = int.Parse(prompt);
            return num;
        }
        public static string getMultipleStrings(string prompt)
        {
            Console.WriteLine(prompt);
            string data = Console.ReadLine();
            return data;
        }
        public static bool validateNumber(string prompt)
        {
            int numCheck;

            while (!(int.TryParse(prompt, out numCheck)))
            {
                return false;
            }
            return true;
        }
    }
}

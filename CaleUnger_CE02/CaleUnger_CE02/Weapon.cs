﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUnger_CE02
{
    class Weapon
    {
        private string _weaponName;
        private int _attack;
        private string _element;

        public Weapon(string weaponName, int attack, string element)
        {
            _weaponName = weaponName;
            _attack = attack;
            _element = element;
        }

        public string weaponName
        {
            get
            {
                return _weaponName;
            }
            set
            {
                _weaponName = value;
            }
        }

        public string element
        {
            get
            {
                return _element;
            }
            set
            {
                _element = value;
            }
        }

        public void Display()
        {
            Console.WriteLine($"Weapon Stats\n"
                            + $"------------\n"
                            + $"Name: {_weaponName}\n"
                            + $"Damage: {_attack}\n" 
                            + $"Element: {_element}\n");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUnger_CE03
{
    class Character
    {
        string _name;
        int _health;
        string _statusEffect;

        public Character(string name, int health, string statusEffect)
        {
            _name = name;
            _health = health;
            _statusEffect = statusEffect;
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public int Health
        {
            get
            {
                return _health;
            }
            set
            {
                _health = value;
            }
        }

        public string StatusEffect
        {
            get
            {
                return _statusEffect;
            }
            set
            {
                _statusEffect = value;
            }
        }

        //Output information from class
        public void Display()
        {
            // cw + tab(x2)
            Console.WriteLine("The Aftermath of the Potion\n"
                            + "---------------------------\n"
                            + $"Name:          {Name}\n"
                            + $"Status Effect: {StatusEffect}\n"
                            + $"Status Value:  {Health}");
                            

        }
    }
}

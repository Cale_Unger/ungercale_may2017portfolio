﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUnger_CE03
{
    interface IUsable
    {
        void Use(Character character);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUnger_CE03
{
    abstract class Item : IUsable
    {
        protected int _intEffect;
        protected string _strEffect;
        protected string _description;

        public Item(int intEffect, string strEffect, string description)
        {
            _intEffect = intEffect;
            _strEffect = strEffect;
            _description = description;
        }

        public abstract void Use(Character character);
    }
}

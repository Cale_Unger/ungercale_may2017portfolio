﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUnger_CE03
{
    class Potion : Item
    {
        public Potion(int intEffect, string strEffect, string description) : base(intEffect,strEffect,description)
        {
            _intEffect = intEffect;
            _strEffect = strEffect;
            _description = description;
        }

        public int IntEffect
        {
            get
            {
                return _intEffect;
            }
            set
            {
                _intEffect = value;
            }
        }

        public override void Use(Character character)
        {
            character.Health += _intEffect;
        }
    }
}

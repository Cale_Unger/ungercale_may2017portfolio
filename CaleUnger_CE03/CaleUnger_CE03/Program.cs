﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUnger_CE03
{
    class Program
    {
        static void Main(string[] args)
        {
            int effect;
            string effectString;

            Character character = new Character("",100,"");
            
            StatusMedicine statusMedicine = new StatusMedicine(1,"","");
            
            Console.Write("Enter a name for your character -->");
            character.Name = Console.ReadLine();

            Console.Write("\nYou decide to create a potion.\nEnter the name of an effect for your potion -->");
            character.StatusEffect = Console.ReadLine();

            Console.Write("\nEnter the value of the effect of your potion -->");
            effectString = Console.ReadLine();

            while (!(int.TryParse(effectString, out effect)))
            {
                Console.WriteLine("Only enter integers.");
                effectString = Console.ReadLine();
            }

            Potion potion = new Potion(effect, "", "");

            Console.WriteLine("\nYou create a potion of {0}.",character.StatusEffect);

            Console.WriteLine("You decide to use the potion.");

            Console.WriteLine("The status effect [{0}] is applied to you with a value of [{1}].\n", character.StatusEffect, effect);

            potion.Use(character);
            statusMedicine.Use(character);
            character.Display();

            Console.WriteLine("\nPress any key to exit.");
            Console.ReadKey();
        }
    }
}

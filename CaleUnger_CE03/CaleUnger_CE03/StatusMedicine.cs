﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUnger_CE03
{
    class StatusMedicine : Potion
    {
        public StatusMedicine(int intEffect, string strEffect, string description) : base(intEffect, strEffect, description)
        {
            _intEffect = intEffect;
            _strEffect = strEffect;
            _description = description;
        }

        public override void Use(Character character)
        {
            if(_strEffect.ToLower() == character.StatusEffect.ToLower())
            {
                character.StatusEffect = "";

                
            }
            else if(_strEffect.ToLower() == "all")
            {
                character.StatusEffect = "";
            }
        }
    }
}

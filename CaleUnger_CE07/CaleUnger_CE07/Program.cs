﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUnger_CE07
{
    class Program
    {

        static void Main(string[] args)
        {
            using (FileStream stream = File.OpenRead("../../Files/DataFieldsLayout.txt"))
            using (FileStream writeStream = File.OpenWrite("../../Files/File1.txt"))
            {
                BinaryReader reader = new BinaryReader(stream);
                BinaryWriter writer = new BinaryWriter(writeStream);

                // create a buffer to hold the bytes
                byte[] buffer = new Byte[1024];
                int bytesRead;

                // while the read method returns bytes
                // keep writing them to the output stream
                while ((bytesRead =
                        stream.Read(buffer, 0, 1024)) > 0)
                {
                    writeStream.Write(buffer, 0, bytesRead);
                }
            }
            Console.ReadKey();
        }
    }
}
            
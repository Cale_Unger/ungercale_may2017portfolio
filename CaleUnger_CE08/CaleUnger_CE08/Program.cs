﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUnger_CE08
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] list = { "zed", "potato", "a", "c", "george", "f", "t", "b" };

            DisplayArray(list);
            InsertionSort(list,false);
            DisplayArray(list);

            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();
        }
        static void DisplayArray(string[] array)
        {
            Console.Write("Array Values: ");
            foreach (string value in array)
            {
                Console.Write($"{value} ");
            }
            Console.WriteLine();
        }
        static void InsertionSort(string[] list, bool order)
        {
            string words = "";

            int i = 0, j = 0; //i will count the check loop
                              //j will count the words loop
            
            for (j = 1; j < list.Length; j++)
            {//start j at second element (j=1)
                words = list[j];
                
                if(order == true)
                {
                    for (i = j - 1; i >= 0 && list[i].CompareTo(words) < 0; i--)
                    {
                        list[i + 1] = list[i];//move elements down the list
                    }
                    list[i + 1] = words;//insert words into list

                }
                else
                {
                    for (i = j - 1; i >= 0 && list[i].CompareTo(words) > 0; i--)
                    {
                        list[i + 1] = list[i];//move elements down the list
                    }
                    list[i + 1] = words;//insert words into list

                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Json;

namespace CEExceptions
{
    class Program
    {
        //This program follows a set of procedural steps and has the potential to throw a lot of exceptions
        static void Main(string[] args)
        {
            //Initialize the variables that will be used in this application
            WebClient apiConnection = new WebClient();
            Person thePerson = new Person();
            String userInput = "";
            String apiEndPoint = @"https://www.govtrack.us/api/v2/person?limit=10";
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(APIdata));
            APIdata data = new APIdata();

            //Display introduction
            Console.WriteLine("The purpose of this application is to enter information about a person. Let's begin.");

            //Ask the user for their name
            Console.WriteLine("What is the person's full name? (first middle last)");
            userInput = Console.ReadLine();

            //separate the first, middle, and last names out of the user input and store them in the Person object
            try
            {
                String[] names = userInput.Split(' ');
                thePerson.FirstName = names[0];
                thePerson.MiddleName = names[1];
                thePerson.LastName = names[2];
            }
            catch (FormatException)
            {
                //Console.WriteLine(e.StackTrace);
                Console.WriteLine("\n[This exception runs when the userInput variable isn't a string.]\n");

            }
            catch (IndexOutOfRangeException)
            {
                Console.WriteLine("\n[This exception runs when the user doesn't enter a first, middle, and / or last name.]\n");
            }
            catch (MissingFieldException)
            {
                Console.WriteLine("\n[This exception runs when the userInput variable is empty.");
            }

            //ask the user for their age
            Console.WriteLine("What is the person's age?");
            userInput = Console.ReadLine();

            //convert the age to an int and store it in the Person object
            try
            {
                thePerson.Age = Convert.ToInt32(userInput);
            }
            
            catch (NullReferenceException)
            {
                Console.WriteLine("\n[This exception runs when the userInput variable is empty.");
            }
            catch (FormatException)
            {
                //Console.WriteLine(e.StackTrace);
                Console.WriteLine("\n[This exception runs when thePerson.Age can't be converted or is a string.]\n");
            }
            catch (OverflowException)
            {
                Console.WriteLine("\n[This exception runs when the userInput variable is larger than the max possible integer.]\n");
            }


            //Ask the user to provide some miscellaneous traits and add them to the Person object
            Console.WriteLine("Provide some traits of your choice.");
            Console.WriteLine("How many traits will you add? (Maximum of 5)");
            userInput = Console.ReadLine();

            try
            {
                int numTraits = Convert.ToInt32(userInput);
                if (numTraits > 5)
                {
                    numTraits = 5;
                }

                //ask for as many traits as the user specified and add them to the Person object
                for (int i = 0; i < numTraits; i++)
                {
                    Console.WriteLine("What is the name of this trait?");
                    string traitName = Console.ReadLine();

                    Console.WriteLine("What is the value of this trait?");
                    string traitValue = Console.ReadLine();

                    thePerson.AddTrait(traitName, traitValue);
                }
            }
            catch (FormatException)
            {
                //Console.WriteLine(e.StackTrace);
                Console.WriteLine("\n[This exception runs when the traitValue or traitName variable isn't a string.]\n");
            }
            catch (NullReferenceException)
            {
                //Console.WriteLine(e.StackTrace);
                Console.WriteLine("\n[This exception runs when the traitValue or traitName variable is null.]\n");
            }
            catch (OverflowException)
            {
                Console.WriteLine("\n[This exception runs when the userInput variable is larger than the max possible integer.]\n");
            }

            //display the Person object that the user created
            try
            {
                Console.WriteLine("This is the data you have entered:");
                Console.WriteLine(thePerson);
            }
            catch (NullReferenceException)
            {
                //Console.WriteLine(e.StackTrace);
                Console.WriteLine("\n[This exception runs when the data stored in thePerson is null.]\n");
            }
            catch (FormatException)
            {
                //Console.WriteLine(e.StackTrace);
                Console.WriteLine("\n[This exception runs when the data stored in thePerson is the incorrect format.]\n");
            }

            //inform the user that we will now be downloading some additional Person objects
            Console.WriteLine("We will now download some additional people from an online API.");

            //read in some objects from an API; you may not recognize how this code works, but researching what exceptions it may throw is still the same
            try
            {
                Stream apiStream = apiConnection.OpenRead(apiEndPoint);

                data = (APIdata)ser.ReadObject(apiStream);

                apiStream.Close();
            }
            catch (FormatException)
            {
                //Console.WriteLine(e.StackTrace);
                Console.WriteLine("\n[This exception runs when apiStream isn't in the correct format.]\n");
            }

            //let the user select which of these people that they want to see all of the information about.
            while (true)
            {
                try
                {
                    Console.WriteLine("Which of these people would you like to see full details on? (Enter e to exit)");
                    for (int i = 0; i < data.People.Count; i++)
                    {
                        Console.WriteLine(String.Format("{0}.{1} {2}\n", i, data.People[i].FirstName, data.People[i].LastName));
                    }
                    userInput = Console.ReadLine();
                    if (userInput.ToLower().Equals("e"))
                    {
                        break;
                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine(data.People[Convert.ToInt32(userInput)]);
                    }
                }
                catch (ArgumentOutOfRangeException)
                {
                    //Console.WriteLine(e.StackTrace);
                    Console.WriteLine("\n[This exception runs when the userInput variable is an integer out of the programs specified range.]\n");
                }
                catch (FormatException)
                {
                    Console.WriteLine("\n[This exception runs when the userInput variable isn't an integer.]\n");
                }
                catch (NullReferenceException)
                {
                    Console.WriteLine("\n[This exception runs when the userInput variable is null.]\n");
                }
                catch (OverflowException)
                {
                    Console.WriteLine("\n[This exception runs when the userInput is larger than the max possible integer.]\n");
                }
            }

            //end of program
            }
        }
    }

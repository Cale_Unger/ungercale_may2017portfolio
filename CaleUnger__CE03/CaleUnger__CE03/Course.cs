﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUnger__CE03
{
    class Course
    {
        private string _title;
        private string _description;
        private Teacher _teacher;
        private Student[] _students;
        private Student _student;

        public Course(string title, string description, Teacher teacher, Student[] students)
        {
            _title = title;
            _description = description;
            _teacher = teacher;
            _students = students;
        }

        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                _title = value;
            }
        }

        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
            }
        }

        public Teacher Teacher
        {
            get
            {
                return _teacher;
            }
            set
            {
                _teacher = value;
            }
        }

        public Student[] Students
        {
            get
            {
                return _students;
            }
            set
            {
                _students = value;
            }
        }

        public Student Student
        {
            get
            {
                return _student;
            }
            set
            {
                _student = value;
            }
        }

        public void Display()
        {
            Console.WriteLine($"Title:       {Title}"
                             + $"\nDescription: {Description}"
                             + $"\n\nTeacher"
                             + $"\n---------------------------"
                             + $"\nName:        {Teacher.Name}"
                             + $"\nDescription: {Teacher.Description}"
                             + $"\nAge:         {Teacher.Age}"
                             + $"\n\nKnowledge    "
                             + $"\n---------------------------");
            
            for(int i = 0; i < Teacher.Knowledge.Length; i++)
            {
                Console.WriteLine($"{i+1}: {Teacher.Knowledge[i]}");
            }
                            
            for(int i=0;i<Students.Length; i++)
            {
                Console.WriteLine($"\nStudent: {i+1}/{Students.Length}"
                                 + $"\n-------------------------------"
                                 + $"\nName:          {Students[i].Name}"
                                 + $"\nDescription:   {Students[i].Description}"
                                 + $"\nAge:           {Students[i].Age}"
                                 + $"\nGrade:         {Students[i].Grade}"
                                 + $"\n-------------------------------");
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUnger__CE03
{
    class Menu
    {
        public static void menuStart() {

            Student[] students = new Student[]{};
            Student currentStudent = new Student(null, null, 0, 0);
            Teacher currentTeacher = new Teacher(null, null, 0, null);
            Course currentCourse = new Course(null, null, null, null);

            bool running = true;

            while (running)
            {
                Console.Clear();

                Console.WriteLine("Course Manager");
                Console.WriteLine("--------------");
                Console.WriteLine("[1] Create Course");
                Console.WriteLine("[2] Create Teacher");
                Console.WriteLine("[3] Add Students");
                Console.WriteLine("[4] Display");
                Console.WriteLine("[5] Exit");

                Console.Write("\nSelection -->");
                string selection = Console.ReadLine();
                switch (selection.ToLower())
                {
                    case "1":
                    case "create course":
                        {
                            currentCourse = new Course(null, null, null, null);

                            string courseName;
                            string courseDescription;
                            string stringAmount;
                            int studentAmount;

                            Console.Clear();

                            Console.WriteLine("Create Course");
                            Console.WriteLine("---------------");

                            Console.Write("Enter a name for your course --> ");
                            courseName = Console.ReadLine();
                            Console.WriteLine("\nPress any key to continue.");
                            Console.ReadKey();

                            Console.Clear();

                            Console.WriteLine("Create Course");
                            Console.WriteLine("---------------");

                            Console.Write("Enter a description for your course --> ");
                            courseDescription = Console.ReadLine();
                            
                            Console.Clear();

                            Console.WriteLine("Create Course");
                            Console.WriteLine("---------------");

                            Console.Write("Enter how many students are in the course --> ");
                            stringAmount = Console.ReadLine();

                            while(!Int32.TryParse(stringAmount, out studentAmount) || studentAmount <= 0 || string.IsNullOrWhiteSpace(stringAmount))
                            {
                                Console.Write("Enter how many students are in the course --> ");
                                stringAmount = Console.ReadLine();
                            }

                            Console.Clear();

                            students = new Student[studentAmount];
                            currentCourse = new Course(courseName, courseDescription, null, students);

                            Console.WriteLine($"Course: {courseName} has been created. \n{courseName} is the current course.");

                            Console.WriteLine("\nPress any key to continue.");
                            Console.ReadKey();

                            Console.Clear();
                        }
                        break;

                    case "2":
                    case "create teacher":
                        {
                            if (currentCourse.Title != null)
                            {

                                currentTeacher = new Teacher(null, null, 0, null);

                                string teacherName;
                                string teacherDescription;
                                string stringAge;
                                int teacherAge;

                                Console.Clear();

                                Console.WriteLine("Create Teacher");
                                Console.WriteLine("---------------");

                                Console.Write("Enter course teacher's name --> ");
                                teacherName = Console.ReadLine();
                                Console.WriteLine("\nPress any key to continue.");
                                Console.ReadKey();

                                Console.Clear();

                                Console.WriteLine("Create Teacher");
                                Console.WriteLine("---------------");

                                Console.Write("Enter course teacher's description --> ");
                                teacherDescription = Console.ReadLine();
                                Console.WriteLine("\nPress any key to continue.");
                                Console.ReadKey();

                                Console.Clear();

                                Console.WriteLine("Create Teacher");
                                Console.WriteLine("---------------");

                                Console.Write("Enter course teacher's age --> ");
                                stringAge = Console.ReadLine();

                                while (!Int32.TryParse(stringAge, out teacherAge) || teacherAge <= 0 || string.IsNullOrWhiteSpace(stringAge))
                                {
                                    Console.Write("Enter a positive number for the teacher's age --> ");
                                    stringAge = Console.ReadLine();
                                }

                                Console.WriteLine("\nPress any key to continue.");
                                Console.ReadKey();

                                Console.Clear();

                                Console.WriteLine("Create Teacher");
                                Console.WriteLine("---------------");

                                Console.Write("Enter how many types of knowledge the teacher has --> ");
                                string stringknowledge = Console.ReadLine();

                                int knowledgeAmount = 0;
                                while (!Int32.TryParse(stringknowledge, out knowledgeAmount) || knowledgeAmount <= 0 || string.IsNullOrWhiteSpace(stringknowledge))
                                {
                                    Console.Write("\nEnter a positive number for how many types of knowledge the teacher has --> ");
                                    stringknowledge = Console.ReadLine();
                                }

                                string[] teacherKnowledge = new string[knowledgeAmount];

                                Console.Clear();

                                Console.WriteLine("Create Teacher");
                                Console.WriteLine("---------------");

                                for (int i = 0; i < knowledgeAmount; i++)
                                {
                                    Console.Write($"[{i + 1}] Enter course teacher's knowledge --> ");
                                    teacherKnowledge[i] = Console.ReadLine();
                                    Console.WriteLine("Press any key to continue.");
                                    Console.ReadLine();
                                }

                                currentTeacher = new Teacher(teacherName, teacherDescription, teacherAge, teacherKnowledge);
                                currentCourse.Teacher = currentTeacher;

                                Console.Clear();

                                Console.WriteLine($"Teacher: {teacherName} has been added to the current course. \n{teacherName} is the current teacher.");

                                Console.WriteLine("\nPress any key to continue.");
                                Console.ReadKey();

                                Console.Clear();
                            }
                            else
                            {
                                Console.Clear();

                                Console.WriteLine("You must create a course to add a teacher.");
                                Console.WriteLine("\nPress any key to continue.");
                                Console.ReadKey();
                            }
                        }
                        break;
                    case "3":
                    case "add students":
                        {
                            Console.Clear();

                            if(currentCourse.Title != null)
                            {

                            currentStudent = new Student(null, null, 0, 0);
                            
                            string studentName;
                            string studentDescription;
                            string stringAge;
                            int studentAge;
                            string stringGrade;
                            int studentGrade;

                                for (int i = 0; i < students.Length; i++)
                                {

                                    Console.Clear();

                                    Console.WriteLine("Create Student");
                                    Console.WriteLine("---------------");

                                    Console.Write("Enter the student's name --> ");
                                    studentName = Console.ReadLine();
                                    Console.WriteLine("\nPress any key to continue.");
                                    Console.ReadKey();

                                    Console.Clear();

                                    Console.WriteLine("Create Student");
                                    Console.WriteLine("---------------");

                                    Console.Write("Enter the student's description --> ");
                                    studentDescription = Console.ReadLine();
                                    Console.WriteLine("\nPress any key to continue.");
                                    Console.ReadKey();

                                    Console.Clear();

                                    Console.WriteLine("Create Student");
                                    Console.WriteLine("---------------");

                                    Console.Write("Enter the student's age --> ");
                                    stringAge = Console.ReadLine();

                                    while (!Int32.TryParse(stringAge, out studentAge) || studentAge <= 0 || string.IsNullOrWhiteSpace(stringAge))
                                    {
                                        Console.Write("\nEnter a positive number for the students's age --> ");
                                        stringAge = Console.ReadLine();
                                    }

                                    Console.WriteLine("\nPress any key to continue.");
                                    Console.ReadKey();

                                    Console.Clear();

                                    Console.WriteLine("Create Student");
                                    Console.WriteLine("---------------");

                                    Console.Write("Enter the student's grade --> ");
                                    stringGrade = Console.ReadLine();

                                    while (!Int32.TryParse(stringGrade, out studentGrade) || studentGrade < 0 || string.IsNullOrWhiteSpace(stringGrade))
                                    {
                                        Console.Write("\nEnter a positive number for the students's grade --> ");
                                        stringAge = Console.ReadLine();
                                    }

                                    currentStudent = new Student(studentName, studentDescription, studentAge, studentGrade);
                                    students[i] = currentStudent;
                                    //currentCourse.Students[i] = students[i];
                                    //currentCourse.Students[i] = currentStudent;

                                    Console.Clear();

                                    Console.WriteLine($"Student: {studentName} has been added to the current course.");

                                    Console.WriteLine("\nPress any key to continue.");
                                    Console.ReadKey();

                                    Console.Clear();
                                }
                            }
                            else
                            {
                                Console.Clear();

                                Console.WriteLine("You must create a course to add students.");
                                Console.WriteLine("\nPress any key to continue.");
                                Console.ReadKey();
                            }
                        }
                        break;
                    case "4":
                    case "display":
                        {
                            if (currentCourse.Title != null && currentTeacher.Name != null && students != null)
                            {
                                if (students[0] != null)
                                {
                                    Console.Clear();

                                    Console.WriteLine("Course Display");
                                    Console.WriteLine("---------------");

                                    currentCourse.Display();

                                    Console.WriteLine("\nPress any key to continue.");
                                    Console.ReadKey();
                                }
                                else
                                {
                                    Console.Clear();

                                    Console.WriteLine("You must create a course and fill all of its criteria to display.");
                                    Console.WriteLine("\nPress any key to continue.");
                                    Console.ReadKey();
                                }
                            }
                            else
                            {
                                Console.Clear();

                                Console.WriteLine("You must create a course and fill all of its criteria to display.");
                                Console.WriteLine("\nPress any key to continue.");
                                Console.ReadKey();

                            }
                        }
                        break;
                    case "5":
                    case "exit":
                        {
                            Console.Clear();
                            Console.WriteLine("Thanks for using course manager.");
                            Console.ReadKey();
                            running = false;
                        }
                        break;
                    default:
                        {
                            Console.Clear();

                            Console.WriteLine("You didn't enter a valid selection.");
                            Console.WriteLine("\nPress any key to return to the course manager menu.");
                            Console.ReadKey();
                            Console.Clear();
                        }
                        break;
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUnger__CE03
{
    class Student : Person
    {
        private int _grade;

        public Student(string name, string description, int age, int grade) : base(name, description, age)
        {
            _grade = grade;
        }

        public int Grade
        {
            get
            {
                return _grade;
            }
            set
            {
                _grade = value;
            }
        }
    }
}

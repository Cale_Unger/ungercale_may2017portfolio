﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUnger__CE03
{
    class Teacher : Person
    {
        private string[] _knowledge;

        public Teacher(string name, string description, int age, string[] knowledge) : base(name, description, age)
        {
            _knowledge = knowledge;
        }

        public string[] Knowledge
        {
            get
            {
                return _knowledge;
            }
            set
            {
                _knowledge = value;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUnger__CE06
{
    class Card
    {
        string _name;
        string _description;
        decimal _value;

        public Card(string name, string description, decimal value)
        {
            _name = name;
            _description = description;
            _value = value;
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
            }
        }

        public decimal Value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
            }
        }

        public void Display()
        {
            Console.WriteLine("Card Details"
                              +"\n------------"
                              +$"\nName: {Name}"
                              +$"\nDescription: {Description}"
                              +$"\nValue: {Value}");
        }
    }
}
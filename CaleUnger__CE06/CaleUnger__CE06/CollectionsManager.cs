﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUnger__CE06
{
    class CollectionsManager
    {
        private string _name;
        Card _card = new Card("","",0);
        List<Card> _cards;
        Dictionary<string, List<Card>> _cardCollection;

        
        public Dictionary<string, List<Card>> CardCollection
        {
            get
            {
                return _cardCollection;
            }
        }

        public List<Card> CardList
        {
            get
            {
                return _cards;
            }
        }

        public int SelectCard(string message)
        {
            int count = _cards.Count;
            int index = 0;

            if (count > 0)
            {
                for (index = 0; index < count; ++index)
                {
                    Console.WriteLine($"{index + 1}. {_cards[index].Name}");
                }
                Console.Write(message);
                string selection = Console.ReadLine();

                while (!int.TryParse(selection, out index) || (index < 1 || index > count))
                {
                    Console.Write("Please make a valid selection --> ");
                    selection = Console.ReadLine();
                }
                --index;

            }
            ++index;
            return index;
        }

        public void AddCard()
        {
            decimal value;
            string stringValue;

            Console.Write("Enter a name for the card --> ");
            _card.Name = Console.ReadLine();

            Console.Write("Enter a description for the card --> ");
            _card.Description = Console.ReadLine();

            Console.Write("Enter a value for the card --> ");
            stringValue = Console.ReadLine();

            while (!decimal.TryParse(stringValue, out value))
            {
                Console.Write("Please enter a number for the card value --> ");
                stringValue = Console.ReadLine();
            }

            _cards.Add(_card);
            _cardCollection.Add(_card.Name,_cards);
        }

        public void RemoveCard()
        {
            int index = SelectCard("Enter the number for the card you would like to remove --> ");

            if (index == 0)
            {
                Console.WriteLine("No cards to remove. Add some first.");
                Console.ReadKey();
            }
            else
            {
                _cardCollection.Remove(_card.Name);
            }
        }

        public void AddToCollection()
        {
            int index = SelectCard("Enter the number for the card you would like to add -->");

            if (index == 0)
            {
                Console.WriteLine("No cards to add. Add some first.");
                Console.ReadKey();
            }
            else
            {
                _cardCollection.Add(_card.Name, CardList);
                Console.WriteLine($"Index {index} has been added to the collection.");
                _cards.RemoveAt(index);
                
            }
        }

        public void DisplayCollection()
        {
            int index = SelectCard("Enter the number of a collection to display cards for --> ");

            if (index == -1)
            {
                Console.WriteLine("No collection to display cards for. Try adding one first.");
                Console.ReadKey();
            }
            else
            {
                _cards[index].Display();
                Console.ReadKey();
            }
        }

        public void DisplayAllCollections()
        {
            foreach (Card c in _cards)
            {
                c.Display();
            }
            Console.ReadKey();
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }
    }
}

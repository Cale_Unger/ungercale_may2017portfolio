﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUnger__CE06
{
    class Program
    {
        static void Main(string[] args)
        {
            Card card = new Card("","",0);
            CollectionsManager currentManager = null;

            bool running = true;

            while (running)
            {
                Console.Clear();

                Console.WriteLine("Cards");
                Console.WriteLine("---------------------");
                Console.WriteLine("[1] Create collection manager");
                Console.WriteLine("[2] Create a card");
                Console.WriteLine("[3] Add a card to a collection");
                Console.WriteLine("[4] Remove a card from a collection.");
                Console.WriteLine("[5] Display a collection.");
                Console.WriteLine("[6] Display all collections.");
                Console.WriteLine("[7] Exit");

                Console.Write("\nSelection -->");
                string selection = Console.ReadLine();
                switch (selection.ToLower())
                {
                    case "1":
                    case "create collection manager":
                        {
                            Console.Clear();

                            currentManager = new CollectionsManager();

                            Console.Write("Enter a name for your collection -->");
                            currentManager.Name = Console.ReadLine();

                            Console.WriteLine($"\nCollection has been created with the name {currentManager.Name}.");

                            Console.WriteLine("\nPress any key to return to the menu.");
                            Console.ReadKey();
                        }
                        break;
                    case "2":
                    case "create a card":
                        {
                            Console.Clear();
                            if(currentManager != null)
                            {
                                currentManager.AddCard();
                            
                                Console.WriteLine("\nPress any key to return to the menu.");
                                Console.ReadKey();
                                
                            }
                            else
                            {
                                Console.WriteLine("You need to create a collection first.");
                                Console.ReadKey();
                            }
                        }
                        break;
                    case "3":
                    case "add a card to a collection":
                        {
                            Console.Clear();

                            if (currentManager != null)
                            {
                                currentManager.AddToCollection();
                                Console.ReadKey();
                                
                            }
                            else
                            {
                                Console.WriteLine("You need to create a collection first.");
                                Console.ReadKey();
                            }
                        }
                        break;
                    case "4":
                    case "remove a card from a collection":
                        {
                            if (currentManager != null)
                            {
                                currentManager.RemoveCard();
                                Console.WriteLine("\nPress any key to return to the menu.");
                                Console.ReadKey();
                            }
                            else
                            {
                                Console.WriteLine("You need to create a collection first.");
                                Console.ReadKey();
                            }
                        }
                        break;
                    case "5":
                    case "Display a collection":
                        {
                            if (currentManager != null)
                            {
                                currentManager.DisplayCollection();
                                Console.WriteLine("\nPress any key to return to the menu.");
                                Console.ReadKey();
                            }
                            else
                            {
                                Console.WriteLine("You need to create a collection first.");
                                Console.ReadKey();
                            }
                        }
                        break;
                    case "6":
                    case "Display all collections":
                        {
                            if (currentManager != null)
                            {
                                currentManager.DisplayAllCollections();

                                Console.WriteLine("\nPress any key to return to the menu.");
                                Console.ReadKey();
                            }
                            else
                            {
                                Console.WriteLine("You need to create a collection first.");
                                Console.ReadKey();
                            }
                        }
                        break;
                    case "7":
                    case "exit":
                        {
                            Console.WriteLine("Thanks for using card collection creator!");
                            Console.ReadKey();
                            running = false;
                        }
                        break;
                }
            }
        }
    }
}
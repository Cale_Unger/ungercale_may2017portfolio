﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUnger__CE10
{
    class Customer
    {
        string _name;
        int _age;
        List<InventoryItem> _shoppingCart = new List<InventoryItem>();

        public Customer(string name, int age, List<InventoryItem> shoppingCart)
        {
            _name = name;
            _age = age;
            _shoppingCart = shoppingCart;
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public List<InventoryItem> Cart
        {
            get
            {
                return _shoppingCart;
            }
            set
            {
                _shoppingCart = value;
            }
        }
    }
}

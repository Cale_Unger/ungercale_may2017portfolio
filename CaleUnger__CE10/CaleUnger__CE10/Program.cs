﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUnger__CE10
{
    class Program
    {
        static void Main(string[] args)
        {
            List<InventoryItem> cart = new List<InventoryItem>();
            List<InventoryItem> inventory = new List<InventoryItem>();
            Dictionary<string, Customer> customers = new Dictionary<string, Customer>();

            string returnMenu = "\nPress any key to return to the main menu.";
            string returnContinue = "\nPress any key to continue.";

            //inventoryItemItem
            //--------------------------------------------------------------------------------------
            inventory.Add(new InventoryItem("Jurassic Park", 10.00m));
            inventory.Add(new InventoryItem("Jurassic Park || : The Lost World", 10.00m));
            inventory.Add(new InventoryItem("Jurassic Park |||", 10.00m));
            inventory.Add(new InventoryItem("Jurassic Park Collection ", 25.00m));
            inventory.Add(new InventoryItem("Jurassic World", 20.00m));
            inventory.Add(new InventoryItem("Stars Wars", 10.00m));
            inventory.Add(new InventoryItem("The Empire Strikes Back", 10.00m));
            inventory.Add(new InventoryItem("Return of the Jedi", 10.00m));
            inventory.Add(new InventoryItem("Star Wars Trilogy: Original ", 25.00m));
            inventory.Add(new InventoryItem("Star Wars: Episode | - The Phantom Menace", 10.00m));
            inventory.Add(new InventoryItem("Star Wars: Episode || - Attack of the Clones", 10.00m));
            inventory.Add(new InventoryItem("Star Wars: Episode ||| - Revenge of the Sith", 10.00m));
            inventory.Add(new InventoryItem("Star Wars Trilogy: Prequels", 25.00m));
            inventory.Add(new InventoryItem("Star Wars: V|| - The Force Awakens", 20.00m));
            inventory.Add(new InventoryItem("The Park", 5.00m));
            inventory.Add(new InventoryItem("Jurassic Movie", 7.00m));
            inventory.Add(new InventoryItem("Dinosaur Park", 2.00m));
            inventory.Add(new InventoryItem("Space Wars", 11.00m));
            inventory.Add(new InventoryItem("Space Wars ||", 15.00m));
            inventory.Add(new InventoryItem("Space Wars |||", 10.00m));
            inventory.Add(new InventoryItem("Space Park", 22.00m));
            inventory.Add(new InventoryItem("Space Battle", 16.00m));
            inventory.Add(new InventoryItem("Zoo", 3.00m));
            inventory.Add(new InventoryItem("Space Zoo", 12.00m));
            inventory.Add(new InventoryItem("Space Base", 32.00m));
            inventory.Add(new InventoryItem("Dinosaurs in Space", 22.00m));
            inventory.Add(new InventoryItem("Star Base", 10.00m));
            inventory.Add(new InventoryItem("Galactic Battle", 8.00m));
            inventory.Add(new InventoryItem("Galaxy Force", 9.00m));
            inventory.Add(new InventoryItem("Jurassic Ark", 10.00m));
            inventory.Add(new InventoryItem("Ark Park", 10.00m));
            inventory.Add(new InventoryItem("Galactic Federation", 21.00m));
            inventory.Add(new InventoryItem("Space Thing", 4.00m));
            inventory.Add(new InventoryItem("Name of movie with space", 10.00m));
            inventory.Add(new InventoryItem("Name of movie with dinosaurs", 10.00m));
            inventory.Add(new InventoryItem("Dinosaur", 6.00m));
            inventory.Add(new InventoryItem("Place in space", 10.00m));
            inventory.Add(new InventoryItem("Original Movie Name", 12.00m));
            inventory.Add(new InventoryItem("Sequel Movie", 10.00m));
            inventory.Add(new InventoryItem("Movie with that one thing", 10.00m));
            inventory.Add(new InventoryItem("Movie about a thing", 30.00m));
            inventory.Add(new InventoryItem("Space Jam", 13.00m));
            inventory.Add(new InventoryItem("Jamming in Space", 17.00m));
            inventory.Add(new InventoryItem("Park Jam", 10.00m));
            inventory.Add(new InventoryItem("Dinosaur Train", 19.00m));
            inventory.Add(new InventoryItem("Prehistoric Park Place", 18.00m));
            inventory.Add(new InventoryItem("Space Place", 10.00m));
            inventory.Add(new InventoryItem("Some place in space", 18.00m));
            inventory.Add(new InventoryItem("Space Station", 11.00m));
            inventory.Add(new InventoryItem("Jurassic Era", 10.00m));
            inventory.Add(new InventoryItem("Dino Time", 10.00m));
            //--------------------------------------------------------------------------------------

            bool running = true;

            while (running)
            {
                Console.Clear();

                Console.WriteLine("[1] Select a shopper");
                Console.WriteLine("[2] View inventory");
                Console.WriteLine("[3] Add item to cart");
                Console.WriteLine("[4] Remove item form cart");
                Console.WriteLine("[5] Complete purchase");
                Console.WriteLine("[6] Exit");

                Console.Write("\nSelection --> ");
                string selection = Console.ReadLine();

                switch (selection.ToLower())
                {
                    case "1":
                    case "select a shopper":
                        {

                            bool stillRunning = true;

                            string name;
                            string customerAge;
                            int age;

                            if(customers.Count != 0)
                            {
                                while (stillRunning)
                                {
                                    Console.Clear();

                                    Console.WriteLine("[1] Choose a shopper");
                                    Console.WriteLine("[2] Create a shopper");
                                    Console.WriteLine("[3] Exit");

                                    Console.Write("Selection --> ");
                                    selection = Console.ReadLine();

                                    switch (selection.ToLower())
                                    {
                                        case "1":
                                        case "choose a shopper":
                                            {
                                                Console.Clear();

                                                Console.WriteLine("Choose a shopper");
                                                Console.WriteLine("----------------");

                                                for (int i = 0; i < customers.Count; i++)
                                                {
                                                    Console.WriteLine($"{customers.Keys}");
                                                }

                                                Console.Write("\nEnter the name of your chosen shopper --> ");
                                                name = Console.ReadLine();

                                                while(name != customers.Keys.Count.ToString())
                                                {
                                                    Console.Write("\nEnter the name of your chosen shopper --> ");
                                                    name = Console.ReadLine();
                                                }

                                                Console.WriteLine($"The current shopper is now {name}.");
                                                
                                            }
                                            break;
                                        case "2":
                                        case "create a shopper":
                                            {
                                                Console.Clear();

                                                Console.WriteLine("Create a shopper");
                                                Console.WriteLine("----------------");

                                                Console.Write("\nEnter the shopper's name --> ");
                                                name = Console.ReadLine();

                                                Console.Write("\nEnter the shopper's age --> ");
                                                customerAge = Console.ReadLine();

                                                while (!Int32.TryParse(customerAge, out age))
                                                {
                                                    Console.Write("\nEnter the shopper's age --> ");
                                                    customerAge = Console.ReadLine();
                                                }

                                                Customer currentCustomer = new Customer(name, age, cart);
                                                customers.Add(name, currentCustomer);
                                            }
                                            break;
                                        case "3":
                                        case "exit":
                                            {
                                                Console.Clear();
                                                Console.WriteLine(returnMenu);
                                                Console.ReadKey();
                                            }
                                            break;
                                    }
                                }
                            }
                            else
                            {
                                while (stillRunning)
                                {
                                    Console.Clear();

                                    Console.WriteLine($"Currently, there are {customers.Count} shoppers.");

                                    Console.WriteLine("[1] Create a shopper");
                                    Console.WriteLine("[2] Return to main menu");

                                    Console.Write("\nSelection --> ");
                                    selection = Console.ReadLine();

                                    switch (selection.ToLower())
                                    {
                                        case "1":
                                        case "create a shopper":
                                            {
                                                Console.Clear();

                                                Console.WriteLine("Create a shopper");
                                                Console.WriteLine("----------------");

                                                Console.Write("\nEnter the shopper's name --> ");
                                                name = Console.ReadLine();

                                                Console.Write("\nEnter the shopper's age --> ");
                                                customerAge = Console.ReadLine();

                                                while(!Int32.TryParse(customerAge, out age))
                                                {
                                                    Console.Write("\nEnter the shopper's age --> ");
                                                    customerAge = Console.ReadLine();
                                                }

                                                Customer currentCustomer = new Customer(name, age, cart);
                                                customers.Add(name, currentCustomer);
                                            }
                                            break;
                                        case "2":
                                        case "exit":
                                            {
                                                Console.Clear();

                                                Console.WriteLine(returnMenu);
                                                stillRunning = false;
                                            }
                                            break;
                                    }
                                }
                            }
                        }
                        break;
                    case "2":
                    case "view inventory":
                        {
                            Console.Clear();

                            foreach(InventoryItem item in inventory)
                            {
                                Console.WriteLine($"Title:{item.Title} Price:{item.Price}");
                            }

                            Console.WriteLine(returnMenu);
                            Console.ReadKey();
                        }
                        break;
                    case "3":
                    case "add item to cart":
                        {
                            if (customers.Count != 0)
                            {

                                Console.Clear();

                                Console.WriteLine("Inventory");
                                Console.WriteLine("-------------------------");

                                int i = 0;
                                for (i = 0; i < inventory.Count; i++)
                                {
                                    Console.WriteLine($"[{i + 1}] {inventory[i].Title}");
                                }
                                string cartItem;
                                if (inventory.Count > 0)
                                {
                                    Console.Write("\nSelect an item to add --> ");
                                    cartItem = Console.ReadLine();

                                    int itemNum;
                                    while ((!int.TryParse(cartItem, out itemNum)) || itemNum < 0 || itemNum > inventory.Count || string.IsNullOrWhiteSpace(cartItem))
                                    {
                                        Console.WriteLine("\nPlease enter a positive number for the item you want to add --> ");
                                        cartItem = Console.ReadLine();
                                    }
                                    cart.Add(inventory[itemNum - 1]);
                                    inventory.Remove(inventory[itemNum - 1]);
                                    Console.WriteLine($"\nItem {itemNum} has been added to the cart.");
                                }
                                else
                                {
                                    Console.WriteLine("There are no items to add.");
                                }

                                Console.WriteLine(returnMenu);
                                Console.ReadKey();
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine("Please select a customer before trying to add items to the cart.");
                                Console.WriteLine(returnMenu);
                                Console.ReadKey();
                            }
                        }
                        break;
                    case "4":
                    case "remove item from cart":
                        {
                            if (customers.Count != 0)
                            {

                                Console.Clear();

                                Console.WriteLine("Remove item from cart");
                                Console.WriteLine("------------------");

                                int i = 0;
                                for (i = 0; i < cart.Count; i++)
                                {
                                    Console.WriteLine($"[{i + 1}] {cart[i].Title}");
                                }

                                string cartItem;
                                if (cart.Count > 0)
                                {
                                    Console.Write("\nSelect an item to remove --> ");
                                    cartItem = Console.ReadLine();

                                    int itemNum;
                                    while ((!int.TryParse(cartItem, out itemNum)) || itemNum < 0 || itemNum > cart.Count || string.IsNullOrWhiteSpace(cartItem))
                                    {
                                        Console.WriteLine("Please enter a positive number for the item you want to remove --> ");
                                        cartItem = Console.ReadLine();
                                    }

                                    inventory.Add(cart[itemNum - 1]);
                                    cart.Remove(cart[itemNum - 1]);

                                    Console.WriteLine($"\nItem {itemNum} has been removed from the cart.");
                                }
                                else
                                {
                                    Console.WriteLine("There are no items to remove.");
                                }

                                Console.WriteLine(returnMenu);
                                Console.ReadKey();
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine("Please select a customer before trying to add items to the cart.");
                                Console.WriteLine(returnMenu);
                                Console.ReadKey();
                            }
                        }
                        break;
                    case "5":
                    case "complete purchase":
                        {
                            if (cart.Count != 0 && customers.Count != 0)
                            {

                                Console.Clear();

                                decimal total = 0;

                                for (int i = 0; i < cart.Count; i++)
                                {
                                    Console.WriteLine($"Title:{cart[i].Title} Price:{cart[i].Price}");
                                    total += cart[i].Price;
                                }
                                Console.WriteLine("------------------------------------------");
                                Console.WriteLine($"Total: {total}");
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine("Please select a customer before trying to add items to the cart.");
                                Console.WriteLine(returnMenu);
                                Console.ReadKey();
                            }
                        }
                        break;
                    case "6":
                    case "exit":
                        {
                            Console.Clear();

                            Console.WriteLine(returnContinue);
                            Console.ReadKey();
                            running = false;
                        }
                        break;
                }
            }
        }
    }
}         

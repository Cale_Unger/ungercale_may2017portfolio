﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUnger___CE06
{
    class CollectionManager
    {
        List<Card> _cards = new List<Card>();
        Dictionary<string,List<Card>> _collections = new Dictionary<string,List<Card>>();

        public CollectionManager()
        {

        }
        
        public List<Card> Cards
        {
            get
            {
                return _cards;
            }
            set
            {
                _cards = value;
            }
        }

        public Dictionary<string, List<Card>> Collections
        {
            get
            {
                return _collections;
            }
            set
            {
                _collections = value;
            }
        }
    }
}

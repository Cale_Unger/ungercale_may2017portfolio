﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaleUnger___CE06
{
    class Program
    {
        static void Main(string[] args)
        {
            
            CollectionManager currentManager = null;

            bool running = true;

            while (running)
            {
                Console.Clear();

                Console.WriteLine("Card Collection Creator");
                Console.WriteLine("--------------------------------------------");
                Console.WriteLine("[1] Create a collection manager");
                Console.WriteLine("[2] Create a card");
                Console.WriteLine("[3] Add a card to a collection");
                Console.WriteLine("[4] Remove a card from a collection.");
                Console.WriteLine("[5] Display a collection.");
                Console.WriteLine("[6] Display all collections.");
                Console.WriteLine("[7] Exit");

                Console.Write("\nSelection --> ");
                string selection = Console.ReadLine();
                switch (selection.ToLower())
                {
                    case "1":
                    case "create collection manager":
                        {

                            currentManager = new CollectionManager();

                            Console.WriteLine("The collection manager has been created.");

                            Utility.PauseBeforeContinuing();
                        }
                        break;
                    case "2":
                    case "create a card":
                        {
                            string name;
                            string description;
                            int value;

                            Console.Clear();

                            if (currentManager != null)
                            {

                                Console.Write("Enter a name for the card --> ");
                                name = Console.ReadLine();

                                Console.Write("Enter a description for the card --> ");
                                description = Console.ReadLine();

                                value = Validation.GetInt("Enter a value for the card --> ");

                                Card card = new Card(name, description, value);

                                currentManager.Cards.Add(card);

                                card.Display();

                                Utility.PauseBeforeContinuing();
                            }
                            else
                            {
                                Console.WriteLine("Please create collection manager first.");
                                Utility.PauseBeforeContinuing();
                            }
                        }
                        break;
                    case "3":
                    case "add a card to a collection":
                        {
                            int cardNum = 0;
                            string[] keys;

                            Console.Clear();

                            if (currentManager != null && currentManager.Cards != null)
                            {
                                keys = currentManager.Collections.Keys.ToArray();

                                Console.WriteLine("Collections");
                                Console.WriteLine("------------------------------");

                                for (int j = 0; j < keys.Length; j++)
                                {
                                    Console.WriteLine($"[{keys[j]}] {currentManager.Collections[keys[j]]}");
                                }

                                Console.Write("\nEnter the name of the collection you want to add to --> ");
                                string collectionName = Console.ReadLine();

                                for (int j = 0; j < keys.Length; j++)
                                {
                                    if(collectionName.ToLower() != keys[j] || keys.Length == 0)
                                    {
                                        currentManager.Collections.Add(collectionName, new List<Card>());
                                        Console.WriteLine($"\nThere aren't any collections with the name \"{collectionName}\".");
                                        Console.WriteLine($"\nA new collection with the name {collectionName} has been created.");
                                    }
                                }
                                

                                Console.WriteLine("Cards");
                                Console.WriteLine("------------------------------");

                                for (int i = 0; i< currentManager.Cards.Count; i++)
                                {
                                    Console.WriteLine($"[{i+1}] {currentManager.Cards[i].Name}");
                                }

                                cardNum = Validation.GetInt(1, currentManager.Cards.Count, "\nEnter the number of the card you want to add --> ");

                                Console.WriteLine($"\nYou've selected card {cardNum}.");

                                currentManager.Collections[collectionName].Add(currentManager.Cards[cardNum]);
                                currentManager.Cards.RemoveAt(cardNum);

                                Console.WriteLine($"Card {cardNum} has been removed from the cards list and added to the {collectionName} collection.");

                                Utility.PauseBeforeContinuing();
                            }
                            else
                            {
                                Console.WriteLine("Please create collection and at least one card first.");
                                Utility.PauseBeforeContinuing();
                            }
                        }
                        break;
                    case "4":
                    case "remove a card from a collection":
                        {
                            int cardNum = 0;
                            string[] keys;

                            Console.Clear();

                            if (currentManager != null && currentManager.Cards != null)
                            {
                                keys = currentManager.Collections.Keys.ToArray();

                                Console.WriteLine("Collections");
                                Console.WriteLine("------------------------------");

                                for (int j = 0; j < keys.Length; j++)
                                {
                                    Console.WriteLine($"[{keys[j]}] {currentManager.Collections[keys[j]]}");
                                }

                                Console.Write("\nEnter the name of the collection you want to remove from --> ");
                                string collectionName = Console.ReadLine();

                                for (int j = 0; j < keys.Length; j++)
                                {
                                    if (collectionName.ToLower() != keys[j])
                                    {
                                        currentManager.Collections.Add(collectionName, new List<Card>());
                                        Console.WriteLine($"\nThere aren't any collections with the name \"{collectionName}\".");
                                        Console.WriteLine($"\nA new collection with the name {collectionName} has been created.");
                                    }
                                }

                                Console.WriteLine("Cards");
                                Console.WriteLine("------------------------------");

                                for (int i = 0; i < currentManager.Cards.Count; i++)
                                {
                                    Console.WriteLine($"[{i + 1}] {currentManager.Cards[i].Name}");
                                }

                                cardNum = Validation.GetInt(1, currentManager.Cards.Count, "\nEnter the number of the card you want to remove --> ");

                                Console.WriteLine($"\nYou've selected card {cardNum}.");

                                currentManager.Cards.Add(currentManager.Cards[cardNum]);
                                currentManager.Collections[collectionName].RemoveAt(cardNum);
                                

                                Console.WriteLine($"Card {cardNum} has been added to the cards list and removed to the {collectionName} collection.");

                                Utility.PauseBeforeContinuing();
                            }
                            else
                            {
                                Console.WriteLine("Please create collection and at least one card first.");
                                Utility.PauseBeforeContinuing();
                            }
                        }
                        break;
                    case "5":
                    case "Display a collection":
                        {

                            if (currentManager.Collections.Count != 0)
                            {
                                string[] keys;

                                Console.Clear();

                                keys = currentManager.Collections.Keys.ToArray();

                                Console.WriteLine("Display a Collection");
                                Console.WriteLine("------------------------------");

                                for (int i = 0; i < keys.Length; i++)
                                {
                                    Console.WriteLine($"[{keys[i]}] {currentManager.Collections[keys[i]]}");
                                }

                                Console.Write("\nEnter the name of the collection you want to display --> ");
                                string collectionName = Console.ReadLine();

                                while (currentManager.Collections[collectionName.ToLower()] == null)
                                {
                                    Console.WriteLine($"\nThere aren't any collections with the name \"{collectionName}\".");
                                    Console.Write($"\nEnter the name of the collection you want to add to --> ");
                                    collectionName = Console.ReadLine();
                                }

                                Console.WriteLine($"{collectionName} - Cards");
                                Console.WriteLine("------------------------------");

                                for (int k = 0; k < currentManager.Collections[collectionName].Count; k++)
                                {
                                    currentManager.Cards[k].Display();
                                }

                                Utility.PauseBeforeContinuing();
                            }
                            else
                            {
                                Console.WriteLine("Please create a collection first.");

                                Utility.PauseBeforeContinuing();
                            }
                        }
                        break;
                    case "6":
                    case "Display all collections":
                        {
                            Console.Clear();

                            string[] keys = currentManager.Collections.Keys.ToArray();

                            for (int i = 0; i < currentManager.Collections.Count; i++)
                            {
                                Console.WriteLine($"[{keys[i]}] {currentManager.Collections[keys[i]]}");
                                Console.WriteLine($"Name: {currentManager.Cards[i].Name}\nDescription: {currentManager.Cards[i].Description}\nValue {currentManager.Cards[i].Value}");
                                Console.WriteLine("---------------------------------------------------");
                            }

                            Utility.PauseBeforeContinuing();
                        }
                        break;
                    case "7":
                    case "exit":
                        {
                            Console.Clear();

                            Console.WriteLine("Thanks for using Card Collection Creator.");
                            Utility.PauseBeforeContinuing();
                            running = false;
                        }
                        break;
                }
            }
        }
    }
}

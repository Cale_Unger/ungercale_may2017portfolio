﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace DBS2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Clear();

            string cs = @"server=10.63.24.143;userid=finalUser;password=pwd123;database=dbs_final_1705; port=8889";

            MySqlConnection conn = null;

            try
            {
                conn = new MySqlConnection(cs);
                MySqlDataReader rdr = null;
                conn.Open();

                string stm = "select hit_date, hit_time, hit_ms, method, page_id, uri_query, client_id, http_version, user_agent, referer_id, bytes_sent, bytes_rcvd, time_ms, area_id from log_hits right join log_scripts on log_scripts.id = log_hits.page_id";
                MySqlCommand cmd = new MySqlCommand(stm, conn);

                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    Console.WriteLine(rdr.GetInt32(0) + ": " + rdr.GetInt32(1) + ": " + rdr.GetInt32(2) + ": " + rdr.GetInt32(3) + ": " + rdr.GetString(4));
                }

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {

                if (conn != null)
                {
                    conn.Close();
                }

            }

            Console.WriteLine("Done");
        }
    }
}

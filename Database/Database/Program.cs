﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MySql.Data.MySqlClient;
using System.IO;
using System.Data;

namespace Database
{
    class Program
    {
        //Connection for the database
        MySqlConnection _con = null;

        Program()
        {
            //Initialize the connection field with a MySqlConnection object
            _con = new MySqlConnection();
        }

        static void Main(string[] args)
        {
            //Create an instance of Program
            Program app = new Program();
            app.Connect();

            DataTable data = app.QueryDB("SELECT make, model, year FROM vehicle LIMIT 40");
            DataRowCollection rows = data.Rows;

            List<Vehicle> cars = new List<Vehicle>();

            foreach(DataRow row in rows)
            {
                /*
                Console.Write(row["make"].ToString() + " ");
                Console.WriteLine(row["model"].ToString() + " ");
                Console.WriteLine(row["year"].ToString());
                */

                cars.Add(new Vehicle(row["make"].ToString(), row["model"].ToString(), Convert.ToInt32(row["year"].ToString())));
            }

            foreach(Vehicle car in cars)
            {
                Console.WriteLine(car);
            }

            Console.WriteLine("Press any key to continue.");
            Console.ReadKey();
        }

        void Connect()
        {
            BuildConnectionString();

            try
            {
                _con.Open();

                Console.WriteLine("Connection Successful!");
            }
            catch (MySqlException e)
            {
                string msg = "";
                switch (e.Number)
                {
                    default:
                    case 0:
                        {
                            msg = e.ToString();

                            break;
                        }
                    case 1042:
                        {
                            msg = "Can't resolve host address.\n" + _con.ConnectionString;

                            break;
                        }
                    case 1045:
                        {
                            msg = "Invalid username / password";
                            break;

                        }
                }
                Console.WriteLine(msg);
            }
        }

        void BuildConnectionString()
        {
            StringBuilder conString = new StringBuilder();
            conString.Append("Server=");

            using(StreamReader sr = new StreamReader("c:/VFW/connect.txt"))
            {
                string ip = sr.ReadLine();
                conString.Append($"{ip};");
            }

            conString.Append("uid=dbsAdmin;");
            conString.Append("pwd=pass;");
            conString.Append("database=example_1705;");
            conString.Append("port=8889;");

            _con.ConnectionString = conString.ToString();
        }

        DataTable QueryDB(string query)
        {
            MySqlDataAdapter adapt = new MySqlDataAdapter(query, _con);
            DataTable data = new DataTable();

            adapt.SelectCommand.CommandType = CommandType.Text;
            adapt.Fill(data);

            return data;
        }
    }
}

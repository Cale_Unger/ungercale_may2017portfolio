﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database
{
    class Vehicle
    {

        string _make;
        string _model;
        int _year;

        public Vehicle(string make, string model, int year)
        {
            _make = make;
            _model = model;
            _year = year;
        }

        public override string ToString()
        {
            return $"Make: {_make} Model: {_model} Year: {_year}";
        }
    }
}

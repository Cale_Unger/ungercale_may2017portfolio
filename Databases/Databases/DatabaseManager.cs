﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.IO;
using System.Data;

namespace Databases
{
    class DatabaseManager
    {
        private MySqlConnection _connection;

        public DatabaseManager()
        {
            _connection = new MySqlConnection();
        }

        public bool Connect()
        {
            BuildConnectionString();
            bool success = false;

            try
            {
                _connection.Open();

                Console.WriteLine("----------------------------");
                Console.WriteLine("   Connection Successful!   ");
                Console.WriteLine("----------------------------");

                success = true;
            }
            catch(MySqlException e)
            {
                string msg = "";

                switch (e.Number)
                {
                    default:
                    case 0:
                        {
                            msg = e.ToString();
                        }
                        break;

                    case 1042:
                        {
                            msg = "Can't resolve host address.\n" + _connection.ConnectionString;
                        }
                        break;

                    case 1045:
                        {
                            msg = "Invalid username / password";
                        }
                        break;
                }
                Console.WriteLine(msg);
            }
            return success;
        }

        private void BuildConnectionString()
        {
            StringBuilder conString = new StringBuilder();

            conString.Append("server=");
            
            using (StreamReader sr = new StreamReader("C:/VFW/connect.txt"))
            {
                string ip = sr.ReadLine();
                conString.Append($"{ip};");
            }

            conString.Append("uid=dbsSuper1705;");
            conString.Append("pwd=password;");
            conString.Append("database=example_1705;");
            conString.Append("port=8889;");

            _connection.ConnectionString = conString.ToString();
        }

        public DataTable QueryDb(string query)
        {
            MySqlDataAdapter adapt = new MySqlDataAdapter(query, _connection);
            DataTable data = new DataTable();

            adapt.SelectCommand.CommandType = CommandType.Text;
            adapt.Fill(data);

            return data;
        }
    }
}

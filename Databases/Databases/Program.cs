﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;

namespace Databases
{
    class Program
    {
        static void Main(string[] args)
        {
            DatabaseManager db = new DatabaseManager();

            if (db.Connect())
            {
                DataTable data = db.QueryDb("select make, mode, year from vehicle limit 40");
                DataRowCollection rows = data.Rows;

                foreach(DataRow r in rows)
                {
                    string make = r["make"].ToString();
                    string model = r["model"].ToString();
                    int year = Convert.ToInt32(r["year"].ToString());
                    Console.WriteLine($"{make} - {model} ({year})");
                }
            }

            Console.WriteLine("Press any key to continue.");
            Console.ReadLine();
        }
    }
}

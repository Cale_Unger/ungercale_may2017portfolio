﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day2PropertiesAndEncapsulation
{
    class Account
    {

        //Access modifiers
        //private, public, protected
        private int _accountNum;
        private decimal _balance;

        public Account(int accountNum, decimal balance)
        {
            _accountNum = accountNum;
            _balance = balance;
        }

        public void Display()
        {
            Console.WriteLine($"Account: {_accountNum}\n" + $"Balance: {_balance}");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day2PropertiesAndEncapsulation
{
    class Customer
    {
        private string _firstName;
        private string _lastName;
        private Account _account;
        
        public Customer(string firstName, string lastName) : this(firstName, lastName, null)
        {

        }

        public Customer(string firstName, string lastName, Account account)
        {
            _firstName = firstName;
            _lastName = lastName;
            _account = account;
        }

        public string FirstName
        {
            get
            {
                return _firstName;
            }
            set
            {
                _firstName = value;
            }
        }

        public string LastName
        {
            get
            {
                return _lastName;
            }
            set
            {
                _lastName = value;
            }
        }

        public string Name
        {
            get
            {
                return $"{ _lastName}, { _firstName}";
            }
        }

        //Automatic properties
        public string Address { get; set; }

        public Account Acct
        {
            set
            {
                _account = value;
            }
        }

        public void Display()
        {
            // cw + tab(x2)
            Console.WriteLine($"Name: {Name}\n" + $"Address: {Address}\n" + $"Account Info:\n");

            if (_account != null)
            {
                _account.Display();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day2PropertiesAndEncapsulation
{
    class Program
    {
        static void Main(string[] args)
        {
            //ctrl + shift + a - gets new item (class, etc)

            Account act; // creates a variable (box) to hold an object of that type

            /*
            act = new Account(); // creates an object of that type and assigns it to the variable act

            Account act2 = new Account();

            bool lifeTheUniverseAndEverything = true;

            // Terinary Operator
            int value = lifeTheUniverseAndEverything ? 42 : 0;
            */

            Customer customer = new Customer("Bob", "the Minion");
            Console.WriteLine($"Customer Name: {customer.FirstName}");
            customer.FirstName = "Dave";
            customer.Address = "123 Full Sail Drive, Orlando, blah blah blah";
            Console.WriteLine($"Customer Name: {customer.FirstName}");
            customer.Display();

            Account acct = new Account(1, 50m);
            customer.Acct = acct;
            customer.Display();

            Console.WriteLine("Enter 0 or 1.");
            string selection = Console.ReadLine();
            switch (selection)
            {
                case "0":
                case "hello world":
                    {
                        Console.WriteLine("Hello");
                        Console.WriteLine("World");
                    }
                    break;
                case "1":
                case "hello class":
                    {
                        Console.WriteLine("Hello");
                        Console.WriteLine("Class");
                    }
                    break;
                default:
                    {
                        Console.WriteLine("You didn't enter 0 or 1");
                    }
                    break;
            }

            Console.WriteLine("Press a key to continue.");
            Console.ReadKey();
        }
    }
}
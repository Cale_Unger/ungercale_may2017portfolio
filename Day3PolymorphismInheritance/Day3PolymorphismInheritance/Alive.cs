﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day3PolymorphismInheritance
{
    abstract class Alive
    {
        public abstract void Breath();

        public void DoSomething()
        {

        }
    }
}

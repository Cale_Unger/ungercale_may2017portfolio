﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day3PolymorphismInheritance
{
    class Animal : Alive
    {
        protected string _name;
        protected string _color;

        public override void Breath()
        {

        }

        public Animal(string name, string color)
        {
            _name = name;
            _color = color;
        }
    }
}

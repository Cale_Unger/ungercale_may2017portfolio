﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day3PolymorphismInheritance
{
    class Car : IMove
    {
        string _make;
        string _model;

        public Car(string make, string model)
        {
            _make = make;
            _model = model;

        }

        public void Move()
        {
            Console.WriteLine("Vroom");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day3PolymorphismInheritance
{
    class Cat : Animal
    {
        int _angerLevel;

        public Cat(string name, string color, int angerLevel) : base(name, color)
        {
            _angerLevel = angerLevel;
        }

        public override string ToString()
        {
            return $"Name: {_name}\nColor: {_color}\nAnger: {_angerLevel}".ToString();
        }
    }
}

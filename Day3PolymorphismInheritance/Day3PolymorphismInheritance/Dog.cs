﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day3PolymorphismInheritance
{
    class Dog : Animal, IMove
    {
        string _breed;

        public Dog(string name, string color, string breed) : base(name, color)
        {
            _breed = breed;
        }

        public void Feed()
        {
            Console.WriteLine("Barks Happily!");
        }

        public override string ToString()
        {
            return $"Name: {_name}\nColor: {_color}\nBreed: {_breed}".ToString();
        }

        public void Move()
        {
            Console.WriteLine("Wags it's tail.");
        }

        public void Method1()
        {
            Console.WriteLine("This is dog method 1.");
        }

        public void Method2()
        {
            Console.WriteLine("This is dog method 2.");
        }
    }
}
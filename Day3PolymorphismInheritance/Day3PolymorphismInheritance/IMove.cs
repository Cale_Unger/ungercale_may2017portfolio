﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day3PolymorphismInheritance
{
    interface IMove
    {
        // Only contains method signatures
        // Method signatures consist of : (return type), (method name), (method parameters)
        void Move();
    }
}

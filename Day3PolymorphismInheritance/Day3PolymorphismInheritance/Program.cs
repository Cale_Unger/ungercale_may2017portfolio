﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day3PolymorphismInheritance
{
    class Program
    {

        static void Main(string[] args)
        {
            Animal[] animals = new Animal[2];

            animals[0] = new Dog("Barky", "gray", "mutt");
            animals[1] = new Cat("Fluffy", "white", 50);
            Car car = new Car("Honda", "Civic");

            if(animals[0] is Dog)
            {
                Dog temp = animals[0] as Dog;
                temp.Feed();
                temp.Move();
            }

            Console.WriteLine(animals[0]);
            Console.WriteLine("\n"+animals[1]);

            car.Move();

            IMove somethingThatMoves;
            somethingThatMoves = animals[0] as Dog;
            somethingThatMoves.Move();
            somethingThatMoves = car;

            Alive livingAnimal = animals[0];
            livingAnimal.DoSomething();

            Dog Fido = animals[0] as Dog;
            

            Console.WriteLine("\nPress a key to continue.");
            Console.ReadKey();

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day5Exceptions
{
    class Program
    {
        static void Main(string[] args)
        {
            FileStream file = null;
            FileInfo info = new FileInfo("C:/file.txt");
            Object obj = null;

                try
                {
                    file = info.OpenWrite();
                    file.WriteByte(0xf);

                    Display(obj);

                }
                catch (ArgumentNullException e)
                {
                    Console.WriteLine(e.Message);
                    obj = new object();
                }
                finally
                {
                    if (file != null)
                    {
                        file.Close();
                    }
                }
            }
            static void Display(object something)
            {
                if (something == null)
                {
                    throw new ArgumentNullException("something");
                }
                Console.WriteLine(something);
            }
        }
    }

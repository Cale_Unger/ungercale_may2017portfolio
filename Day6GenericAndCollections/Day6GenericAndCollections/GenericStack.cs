﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day6GenericAndCollections
{
    class GenericStack<T>
    {
        private T[] _values;
        private int _currentIndex;
        private int _capacity;
        private int _growBy;

        public GenericStack()
        {
            _capacity = 10;
            _values = new T[_capacity];
            _currentIndex = 0;
            _growBy = 5;
        }

        public void Push(T value)
        {
            _values[_currentIndex] = value;
            _currentIndex++;

            if (_currentIndex == _capacity)
            {
                _capacity += _growBy;
                T[] newArray = new T[_capacity];

                for (int i = 0; i < _currentIndex; i++)
                {
                    newArray[i] = _values[i];
                }

                _values = newArray;
            }

        }

        public T Pop()
        {
            T value = default(T);
            if (_currentIndex != 0)
            {
                value = _values[_currentIndex - 1];
                _currentIndex--;

            }
            return value;
        }

        public override string ToString()
        {
            string asString = "";

            for (int i = 0; i < _currentIndex; i++)
            {
                asString += $" {_values[i]}";
            }

            return asString;
        }
    }
}

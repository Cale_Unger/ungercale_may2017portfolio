﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day7FileIOAndJson
{
    class Dog
    {
        private string _breed;
        private string _color;
        private string _name;
        private int _age;
        private char _size;
        private char _gender;

        public string Breed
        {
            get
            {
                return _breed;
            }
            set
            {
                _breed = value;
            }
        }
        public string Color
        {
            get
            {
                return _color;
            }
            set
            {
                _color = value;
            }
        }
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }
        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                _age = value;
            }
        }
        public char Size
        {
            get
            {
                return _size;
            }
            set
            {
                _size = value;
            }
        }
        public char Gender
        {
            get
            {
                return _gender;
            }
            set
            {
                _gender = value;
            }
        }

        public override string ToString()
        {
            return $"Name: {_name}\nAge: {_age}\nColor: {_color}\nBreed: {_breed}\nSize: {_size}\nGender: {_gender}";
        }
    }
}

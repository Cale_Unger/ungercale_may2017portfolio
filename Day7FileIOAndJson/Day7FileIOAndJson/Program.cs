﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Day7FileIOAndJson
{
    class Program
    {
        static void Main(string[] args)
        {
            Dog fuzzy = new Dog();
            Dog mystery = new Dog();

            fuzzy.Age = 42;
            fuzzy.Breed = "Golden Retriever";
            fuzzy.Color = "Golden";
            fuzzy.Gender = 'M';
            fuzzy.Name = "Fuzzy";
            fuzzy.Size = 'L';

            WriteDogToFile(null);
            WriteDogToFile(fuzzy);

            Console.WriteLine(fuzzy);

            ReadDogFromFile(fuzzy);
            ReadDogFromFile(mystery);

            Console.WriteLine(mystery);
            Console.WriteLine("\nPress a key to continue.");
            Console.ReadKey();
        }
        static void ReadDogFromFile(Dog dog)
        {
            //dog = new Dog();

            using (StreamReader sr = new StreamReader("../../Output/dog.txt"))
            {
                sr.ReadLine();

                dog.Age = Convert.ToInt32(sr.ReadLine());
                dog.Breed = sr.ReadLine();
                dog.Color = sr.ReadLine();
                dog.Gender = Convert.ToChar(sr.ReadLine());
                dog.Name = sr.ReadLine();
                dog.Size = Convert.ToChar(sr.ReadLine());
            }
        }
        static void WriteDogToFile(Dog fido)
        {
            if (fido != null)
            {

                //Create dog.txt - 2 folders up from the exe location and into the output folder
                using (StreamWriter sw = new StreamWriter("../../Output/dog.txt"))
                {
                    sw.WriteLine($"Dog Count:");
                    sw.WriteLine(fido.Age);
                    sw.WriteLine(fido.Breed);
                    sw.WriteLine(fido.Color);
                    sw.WriteLine(fido.Gender);
                    sw.WriteLine(fido.Name);
                    sw.WriteLine(fido.Size);

                }
            }
        }
    }
}

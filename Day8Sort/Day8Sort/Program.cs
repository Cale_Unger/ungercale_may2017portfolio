﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day8Sort
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = { 50, 35, 22, 48, 44, 42, 24, 42, 20, 42, 42, 42 };

            DisplayArray(array);
            BubbleSort(array);
            DisplayArray(array);

            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();
        }
        
        static void DisplayArray(int[] array)
        {
            Console.Write("Array Values: ");
            foreach (int value in array)
            {
                Console.Write($"{value} ");
            }
            Console.WriteLine();
        }
        static void BubbleSort(int[] unsorted)
        {
            for (int outerIndex = 0; outerIndex< unsorted.Length; outerIndex++)
            {
                for(int innerIndex = 1; innerIndex<unsorted.Length; innerIndex++)
                {
                    if(unsorted[innerIndex] < unsorted[innerIndex - 1])
                    {
                        int temp = unsorted[innerIndex];
                        unsorted[innerIndex] = unsorted[innerIndex - 1];
                        unsorted[innerIndex - 1] = temp;
                    }
                }
            }
        }
    }
}

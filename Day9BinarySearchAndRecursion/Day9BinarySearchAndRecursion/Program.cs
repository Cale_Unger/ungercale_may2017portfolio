﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day9BinarySearchAndRecursion
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] values = { 1, 7, 3, 2, 8, 9, 20 };
            int value = 10;
            Console.WriteLine($"Factorial of {value} is {CalcFactorial(value)}.");

            string input = "";
            int searchValue = 0;
            do
            {
                Console.Write("Enter a value to search for: ");
                input = Console.ReadLine();
            }
            while (!int.TryParse(input, out searchValue));

            int result = BinarySearch(searchValue, values, 0, values.Length-1);

            if (result == -1)
            {
                Console.WriteLine("Value was not found in the array.");
            }
            else
            {
                Console.WriteLine($"Value was found at index: {result}.");
            }

            Console.WriteLine("\nPress any key to continue.");
            Console.ReadKey();
        }


        /*Recursion Example
         * 5!
         */
        static int CalcFactorial(int number)
        {
            if (number == 0)
            {
                return 1;
            }
            return number * CalcFactorial(number - 1);
        }

        static int CalcFactorialLoop(int number)
        {
            int total = 0;

            for (int i = number; i > 0; i--)
            {
                //total = total * i;
                total *= i;
            }
            return total;
        }

        static int BinarySearch(int searchValue, int[] sortedValues, int start, int end)
        {
            Console.WriteLine($"checking - start: {start} end: {end}");
            int result = -1;
            if (start <= end)
            {

                int mid = (start + end) / 2;

                if (sortedValues[mid] == searchValue)
                {
                    result = mid;
                }
                else if (sortedValues[mid] > searchValue)
                {
                    result = BinarySearch(searchValue, sortedValues, start, mid - 1);
                }
                else
                {
                    result = BinarySearch(searchValue, sortedValues, mid + 1, end);
                }
            }
            return result;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DebugginLecture
{
    class Book
    {
        public Page[] pages;

        Book()
        {
            pages = new Page[0];
        }

        public Book(int numPages, String text, String title)
        {
            //add an additional page for the title
            pages = new Page[numPages + 1];
            //make the first page the title
            pages[0] = new Page(title, 0);

            //divide the book text in to words
            String[] words = text.Split(' ');

            //each page will have the same number of words
            int wordsPerPage = words.Length / numPages;

            //create all of the text pages
            for (int i = 1; i < numPages; i++)
            {
                String pageText = "";
                //put all of the words for the page in to one string
                for (int j = 0; j < wordsPerPage; j++)
                {
                    //add the next word until there
                    pageText += words[j + (wordsPerPage * i - 1)] + " ";
                }

                //make the page with the concatenated text and page number
                pages[i] = new Page(pageText,i);
            }
        }

        public void Read()
        {
            int CurrentPage = 0;
            while (true)
            {
                Console.WriteLine(pages[CurrentPage].text);
                Console.WriteLine("Page# " + pages[CurrentPage].pageNumber);
                Console.WriteLine("Turn page by typing turn or specify a page number to turn to. Stop reading by typing stop.");

                String userChoice = Console.ReadLine();
                if (userChoice.ToLower().Equals("turn"))
                {
                    CurrentPage++;
                }
                else if (userChoice.ToLower().Equals("stop"))
                {
                    return;
                }
                else {
                    try
                    {
                        CurrentPage = Int32.Parse(userChoice);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.StackTrace);
                    }
                }
            }
        }
    }
}

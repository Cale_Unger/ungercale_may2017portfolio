﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DebugginLecture
{
    class Building
    {
        string location;

        protected Building()
        {
            location = "a";
        }

        public Building(string location)
        {
            this.location = location;
        }
        
        public override string ToString()
        {
            return location;
        }

    }
}

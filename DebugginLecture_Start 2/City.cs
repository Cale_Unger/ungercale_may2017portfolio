﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DebugginLecture
{
    class City
    {
        Building[] buildings;

        public City()
        {
            buildings = new Building[3];
            //build the Library located on 3rd Street
            buildings[0] = new Library("3rd Street");
            //build the FireStation located on Main Street
            buildings[1] = new FireStation(4, "Main Street");
            //build a generic Building located on South Street
            buildings[2] = new Building("South Street");
        }

        public void whatIsHere()
        {
            foreach (Building building in buildings)
            {
                Console.WriteLine(building);
            }
        }

        public void goToTheLibrary()
        {
            Console.WriteLine("The library has these books:");
            try
            {
                ((Library)buildings[0]).showBooks();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }
            Console.WriteLine("Let's grab a book. Which one do we want?");
            Book chosenBook = null;
            try
            {
                string selection = Console.ReadLine();
                chosenBook = ((Library)buildings[0]).selectABook(selection);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }

            //read the selected book
            chosenBook.Read();
        }
    }
}

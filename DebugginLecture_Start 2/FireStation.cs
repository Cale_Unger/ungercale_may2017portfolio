﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DebugginLecture
{
    class FireStation : Building
    {
        int numberOfTrucks;

        public FireStation(int numTrucks, string location) : base(location)
        {
            //set the station's truck count
            numberOfTrucks = numTrucks;
        }
    }
}

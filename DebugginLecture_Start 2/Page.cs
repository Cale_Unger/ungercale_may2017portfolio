﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DebugginLecture
{
    class Page
    {
        public string text;
        public int pageNumber;

        Page()
        {
            text = "";
            pageNumber = 0;
        }

        public Page(string text, int pageNumber)
        {
            //store this page's text and page number
            this.text = text;
            this.pageNumber = pageNumber;
        }

    }
}

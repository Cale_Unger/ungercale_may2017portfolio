﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DebugginLecture
{
    class Program
    {
        static void Main(string[] args)
        {
            //make the city
            Console.WriteLine("Constructing the city.");
    
            City theCity = new City();

            //what's in the city?
            Console.WriteLine("What's in the city?");
            theCity.whatIsHere();

            //let's go to the library
            Console.WriteLine("Time to go to the library on 3rd Street.");
            theCity.goToTheLibrary();

            //done with the city
        }
    }
}

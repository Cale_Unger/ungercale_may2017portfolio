﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DebugginLecture
{
    abstract class Building
    {
        string location;

        public Building()
        {
            location = "a";
        }

        protected Building(string location)
        {
            this.location = location;
        }
        
        public override string ToString()
        {
            return location;
        }

    }
}

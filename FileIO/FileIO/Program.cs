﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FileIO
{
    class Program
    {
        static void Main(string[] args)
        {
            Program app = new Program();
            app.CreateDirectory(@"Output\");
            app.CreateDirectory(@"Tmp\");

            List<Customer> customers = new List<Customer>();
            customers.Add(new Customer("Dave", "the minion", 130));
            customers.Add(new Customer("Lightning", "McQueen", 15));

            try
            {
                using (StreamWriter sw = new StreamWriter(@"Output\Customer.txt"))
                {
                    foreach (Customer customer in customers)
                    {
                        sw.WriteLine(customer.Name);
                        sw.WriteLine(customer.Desc);
                        sw.WriteLine(customer.Age);
                    }
                }
            }
            catch (IOException e)
            {
                Console.WriteLine(e);
            }

            //Writing customers to a .json file
            try
            {
                using (StreamWriter sw = new StreamWriter(@"Output\Customer.json"))
                {
                    sw.WriteLine("[");
                    for(int i=0; i< customers.Count; i++)
                    {
                        sw.WriteLine("{");
                        sw.WriteLine($"\"Name\" : \"{customers[i].Name}\" ,");
                        sw.WriteLine($"\"Descripton\" : \"{customers[i].Desc}\" ,");
                        sw.WriteLine($"\"Age\" : \"{customers[i].Age}\"");
                        if (i == customers.Count - 1)
                        {
                            sw.WriteLine("}");
                        }
                        else
                        {
                            sw.WriteLine("},");
                        }
                    }
                    sw.WriteLine("]");
                }
            }
            catch (IOException e)
            {
                Console.WriteLine(e);
            }

            List<Customer> customersFromFile = new List<Customer>();
            // Reading customers from a .txt file

            try
            {
                using (StreamReader sr = new StreamReader(@"Output\Customer.txt"))
                {

                    while (!sr.EndOfStream)
                    {
                        string name = sr.ReadLine();
                        string desc = sr.ReadLine();
                        int age = int.Parse(sr.ReadLine());

                        customersFromFile.Add(new Customer(name, desc, age));
                    }
                        /*
                        sw.WriteLine(customer.Name);
                        sw.WriteLine(customer.Desc);
                        sw.WriteLine(customer.Age);
                        */
                }
            }
            catch (IOException e)
            {
                Console.WriteLine(e);
            }

            Console.WriteLine("Press a key to continue.");
            Console.ReadKey();

            app.DeleteDirectory(@"Tmp\");

        }
        private void CreateDirectory(string dirName)
        {

            if (Directory.Exists(dirName))
            {
                Console.WriteLine($"dir \"{dirName}\" already exists.");
            }
            else
            {
                Directory.CreateDirectory(dirName);
                Console.WriteLine($"dir {dirName} created.");
            }
        }

        private void DeleteDirectory(string dirName)
        {
            Directory.Delete(dirName);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileIOAndJSONFormat
{
    class Minion
    {
        string _name;
        int _age;
        int _numEyes;

        public Minion(string name, int age, int numEyes)
        {
            _name = name;
            _age = age;
            _numEyes = numEyes;
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                _age = value;
            }
        }

        public int NumEyes
        {
            get
            {
                return _numEyes;
            }
            set
            {
                _numEyes = value;
            }
        }
    }
}

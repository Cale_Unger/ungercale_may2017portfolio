﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FileIOAndJSONFormat
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             * JSON - Java Script Object Notation
             * 
             * Good, Human Readable, format for representing objects in a platform independent way.
             * 
             * Files - for a file to be valid it must have the following items correct.
             * 
             * Extension - tells the os what to expect in the file
             * Format - the way the data is layed out
             * 
             * Using - calls IDisposable interface's dispose method at the end
             * used with file I/O to ensure the closing of the file(s) that were being used
             * 
             * Stream reader - read in data in a string format
             * 
             * Stream writer - write out data in a string format
             * 
             * File Paths
             * 
             * relative - just the file name or uses .. and folder names to navigate the different directories.
             * 
             * absolute - the exact path starting with the drive letter
             * 
             * Directory.Exists(directoryName)
             * returns true or false if a folder exits
             * 
             * Directory.Create(directoryname)
             * create a directory - check before you create
             * 
             * File.Exists(fileName)
             * returns true or false if a file exists
             */

            List<Minion> minions = new List<Minion>();

            minions.Add(new Minion("Bob", 101, 2));
            minions.Add(new Minion("Dave", 80, 1));
            minions.Add(new Minion("Jeff", 200, 6));

            string filePath = "../../Files/";

            //when setting up file I/O
            //get the read for write to work for a single item first then make it work for all of them
            try
            {
                // File I/O operations in here
                using (StreamWriter sw = new StreamWriter(filePath + "minionList.txt"))
                {
                    for(int i = 0; i < minions.Count; i++)
                    {
                        //Name, Age, Number of Eyes
                        sw.Write(minions[i].Name);
                        sw.Write($"|{minions[i].Age}");
                        sw.WriteLine($"|{minions[i].NumEyes}");
                    }
                }
            }
            catch(IOException)
            {
              
            }

            minions.Clear();

            try
            {

                using(StreamReader sr = new StreamReader(filePath + "minionList.txt"))
                {
                    while (sr.EndOfStream == false)
                    {
                        string minionTxt = sr.ReadLine();
                        string[] minionData = minionTxt.Split('|');
                        int age = Int32.Parse(minionData[1]);
                        int eyes = Int32.Parse(minionData[2]);
                        Minion temp = new Minion(minionData[0], age, eyes);
                        minions.Add(temp);
                    }
                }
            }
            catch (IOException)
            {

            }

            try
            {
                using (StreamWriter sw = new StreamWriter(filePath + "minionList.json"))
                {
                    sw.Write("[");

                    for (int i = 0; i < minions.Count; i++)
                    {
                        sw.Write("{");

                        sw.Write($"\"Name\" : \"{minions[i].Name}\",");
                        sw.Write($"\"Age\" : \"{minions[i].Age}\",");
                        sw.Write($"\"Number of Eyes\" : \"{minions[i].NumEyes}\"");

                        if(i != minions.Count - 1)
                        {
                            sw.Write("},");
                        }
                        else
                        {
                            sw.Write("}");
                        }
                    }
                    sw.Write("]");
                }
            }
            catch(IOException)
            {

            }

            foreach(Minion m in minions)
            {
                Console.WriteLine( $"{m.Name} is {m.Age} years old and has {m.NumEyes} eyes");
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FundamentalsReview
{
    class Program
    {
        static void Main(string[] args)
        {
            //Variables
            //dataType boxName(variableName);
            //declaration and assignment
            int intVariable = 42;

            //Primitive types
            // a built in type. all of a set size. passed by copy when used as a parameter.
            //Reference types
            // classes - passed by reference. Means that you are working with the original object and not a copy

            //Basic Operations
            //MD AS
            // left --> right
            //()
            //%
            // /

            int v1 = 2;
            int v2 = 4;
            int v3 = 10;

            int result = v1 + v2;
            Console.WriteLine($"result of {v1} + {v2} = {result}.");

            result = v1 - v2;
            Console.WriteLine($"result of {v1} - {v2} = {result}.");

            result = v1 * v2;
            Console.WriteLine($"result of {v1} * {v2} = {result}.");

            result = v3 / v2;
            Console.WriteLine($"result of {v1} / {v2} = {result}.");

            result = v1 + v2 * v3;
            Console.WriteLine($"result of {v1} + {v2} * {v3} = {result}.");

            result = (v1 + v2) * v3;
            Console.WriteLine($"result of {v1} + {v2} * {v3} = {result}.");

            Console.WriteLine("\nPress any key to continue.");
            Console.ReadKey();
            Console.Clear();

            float currentValue = 2.5f;
            Console.WriteLine($" pre increment ++{currentValue} is {++currentValue}.");
            Console.WriteLine($" post increment {currentValue}++ is {currentValue++}.");
            Console.WriteLine($" pre increment --{currentValue} is {--currentValue}.");
            Console.WriteLine($" post increment {currentValue}-- is {currentValue--}.");

            Console.WriteLine("\nPress any key to continue.");
            Console.ReadKey();
            Console.Clear();

            //Arrays
            int[] arrayOfInts = null;
            arrayOfInts = new int[5];
            arrayOfInts = new int[] { 1, 10, 23 };
            int[,] multiIntArray;
            //int[,,] threedeeArray;

            multiIntArray = new int[3, 3];

            multiIntArray[0, 0] = 1;
            multiIntArray[0, 1] = 2;
            multiIntArray[0, 2] = 1;

            multiIntArray[1, 0] = 0;
            multiIntArray[1, 1] = 2;
            multiIntArray[1, 2] = 1;

            multiIntArray[2, 0] = 2;
            multiIntArray[2, 1] = 0;
            multiIntArray[2, 2] = 1;

            for(int i = 0; i<3; i++)
            {
                for(int j = 0; j<3; j++)
                {
                    if (multiIntArray[i, j] == 0)
                    {
                        Console.Write(' ');
                    }
                    else if (multiIntArray[i,j] == 1)
                    {
                        Console.Write('X');
                    }
                    else
                    {
                        Console.Write('O');
                    }
                }
                Console.WriteLine();
                //Console.ReadKey();
            }
            Console.WriteLine("\nPress any key to continue");
            Console.ReadKey();
        }
    }
}

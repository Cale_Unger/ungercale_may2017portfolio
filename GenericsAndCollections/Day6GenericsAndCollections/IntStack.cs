﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day6GenericsAndCollections
{
    class IntStack
    {
        private int[] _values;
        private int _currentIndex;
        private int _capacity;
        private int _growBy;

        public IntStack()
        {
            _capacity = 10;
            _values = new int[_capacity];
            _currentIndex = 0;  
            _growBy = 5; 
        }

        public void Push(int value)
        {
            _values[_currentIndex] = value;
            ++_currentIndex;

            if(_currentIndex == _capacity)
            {
                _capacity += _growBy;
                int[] newArray = new int[_capacity];

                for(int i = 0; i < _currentIndex; ++i)
                {
                    newArray[i] = _values[i];
                }

                _values = newArray;
            }
        }

        public int Pop()
        {
            int value = -1;
            if (_currentIndex != 0)
            {
                value = _values[_currentIndex - 1];
                --_currentIndex;
            }
            return value;
        }

        public override string ToString()
        {
            string asString = "";
            
            for(int i = 0; i < _currentIndex; ++i)
            {
                asString += $"{_values[i]} ";
            }

            return asString;
        }
    }
}

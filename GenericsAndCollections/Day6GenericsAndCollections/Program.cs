﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day6GenericsAndCollections
{
    class Program
    {
        static void Main(string[] args)
        {
            //GenericStack<GenericStack<int>> ggStack;
            GenericStack<Object> gStack = new GenericStack<Object>();

            gStack.Push(5.5);
            gStack.Push("dog");

            List<string> myList = new List<string>();
            myList.Add("hello");
            myList.Add("world");
            myList.Add("bob");

            Dictionary<string, IntStack> myDictionary = new Dictionary<string, IntStack>();
            myDictionary.Add("firstStack", new IntStack());
            myDictionary["secondStack"] = new IntStack();

            if(myDictionary.ContainsKey("firstStack"))
            {
                Console.WriteLine(myDictionary["firstStack"].Pop());
            }

            myDictionary.Remove("firstStack");

            myList.Remove("world");

            Queue<string> myQueue = new Queue<string>();
            myQueue.Enqueue("Hello");
            myQueue.Enqueue("World");
            myQueue.Dequeue();
            // FIFO



            Console.WriteLine($"{myList[0]} {myList[1]}.");


            IntStack stack = new IntStack();
            bool running = true;
            
            while(running)
            {
                Console.Clear();
                Console.WriteLine($"Stack: {stack}\n");
                Console.WriteLine("1. Push a value");
                Console.WriteLine("2. Pop a value");
                Console.WriteLine("3. Exit");
                Console.Write("Choose an option: ");
                string choice = Console.ReadLine().ToLower();

                switch(choice)
                {
                    case "1":
                    case "push a value":
                        {
                            Console.Write("Please enter an integer value: ");
                            string sValue = Console.ReadLine();
                            int value;

                            while(int.TryParse(sValue, out value) == false)
                            {
                                Console.Write("Please enter a valid integer: ");
                                sValue = Console.ReadLine();
                            }

                            stack.Push(value);

                            break;
                        }
                    case "2":
                    case "pop a value":
                        {
                            int value = stack.Pop();
                            Console.WriteLine($"Value {value} was popped off the stack.");
                            break;
                        }
                    case "exit":
                    case "3":
                        {
                            running = false;
                            break;
                        }
                }

                Console.WriteLine("Press a key to continue.");
                Console.ReadKey();
            }
        }
    }
}

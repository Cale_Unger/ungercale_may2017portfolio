﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericsAndCollectionsLecture
{
    class GStack<Type>
    {
        private Type[] _stack;
        private int _top;

        public GStack(int size)
        {
            _stack = new Type[size];
            _top = 0;
        }

        public void Push(Type item)
        {
            if (_top == _stack.Length)
            {
                ResizeArray();
            }
            _stack[_top] = item;
            _top++;
        }

        private void ResizeArray()
        {
            Type[] temp = new Type[_stack.Length * 2];
            _stack.CopyTo(temp, 0);
            _stack = temp;
        }

        public Type Pop()
        {
            Type item = default(Type);

            if (_top != 0)
            {
                --_top;
                item = _stack[_top];
            }
            else
            {

            }


            return item;
        }
    }
}

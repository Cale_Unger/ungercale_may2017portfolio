﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericsAndCollectionsLecture
{
    class IntStack
    {
        private int[] _stack;
        private int _top;

        public IntStack(int size)
        {
            _stack = new int[size];
            _top = 0;
        }

        public void Push(int item)
        {
            if(_top == _stack.Length)
            {
                ResizeArray();
            }
            _stack[_top] = item;
            _top++;
        }

        private void ResizeArray()
        {
            int[] temp = new int[_stack.Length * 2];
            _stack.CopyTo(temp, 0);
            _stack = temp;
        }

        public int Pop()
        {
            int item = -1;

            if (_top != 0)
            {
                --_top;
                item = _stack[_top];
            }
            else
            {

            }
            

            return item;
        }
    }
}

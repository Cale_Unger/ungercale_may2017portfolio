﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericsAndCollectionsLecture
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Collections
             * Work with any class or data type
             * Can resize themselves so you don't have to know the number of items you'll need them to hold.
             * Stack
             *  Like stacking blocks on top of each other
             *  Remember the call stack
             *  Push (item) - put item on top of the stack
             *  item Pop() - take the top item off the stack
             *  Ordering is called: LIFO (Last In First Out)
             * Queue
             *  Models the behavior of a line
             *  Enqueue (item) - add an item to the end of the queue
             *  item Dequeue() - removes the first item from the queue
             *  Ordering is called: FIFO (First In First Out)
             * List
             *  Like an array
             *  Add(item)
             *  remove(item/index/...)
             * Dictionary
             *  Key, Value pairs stored
             *  
             *  Generics
             *      ClassName<T> 
             */

            IntStack stack = new IntStack(5);
            stack.Push(1);
            stack.Push(3);
            stack.Push(5);
            stack.Push(7);
            stack.Push(9);
            stack.Push(11);
            stack.Push(13);
            stack.Push(15);
            stack.Push(17);

            int value = 0;
            while((value = stack.Pop()) != -1)
            {
                Console.WriteLine($"Value popped off was {value}.");
            }

            GStack<double> doubleStack = new GStack<double>(3);
            doubleStack.Push(32456.14159);
            doubleStack.Push(1089.25332);
            doubleStack.Push(747.12190);
            doubleStack.Push(90.0769);
            doubleStack.Push(1.0978);

            for(int i=0; i<5; i++)
            {
                Console.WriteLine($"DoubleStacks value was: {doubleStack.Pop()}.");
                /*
                if (i == 3)
                {
                    doubleStack.Push(15.3);
                }
                */
            }

            Console.WriteLine("Press a key to continue:::::::::::::");
            Console.ReadKey();

            Console.Clear();

            List<string> listOfStr = new List<string>();
            listOfStr.Add("Hello");
            listOfStr.Add("World");

            Console.WriteLine($"{listOfStr[0]} {listOfStr[1]}");
            listOfStr[0] = "Goodbye";

            foreach(string s in listOfStr)
            {
                Console.Write($"{s} ");
            }

            /* Remove
             * Remove()
             * RemoveAt()
             * RemoveAll()
             * RemoveRange()
             */

            /*List<>
            *size of list
            *listOfStr.Count;
            *do not confuse with
            *listOfStr.Capacity;
            */

            /* Dictionary<Key , Value>
             * Key - How am I going to find my items?
             * Value - What type of item is being held?
             */

            Dictionary<string, Person> roster = new Dictionary<string, Person>();

            Person b = new Person();
            b.Name = "Bob";
            b.Age = 42;

            roster.Add("bob", b);

            Console.WriteLine($"Name: {roster["bob"].Name} and Age: {roster["bob"].Age}");

            Person temp = roster["bob"];
            temp.Age = 50;

            if (roster.ContainsKey("bob"))
            {
                roster.Remove("bob");
            }

            Console.WriteLine("\nPress a key to continue:::::::::::::");
            Console.ReadKey();
        }
    }
}

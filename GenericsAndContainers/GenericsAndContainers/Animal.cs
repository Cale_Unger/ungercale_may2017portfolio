﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericsAndContainers
{
    class Animal
    {
        string _name;
        string _type;
        int _age;

        public Animal(string name, string type, int age)
        {
            _name = name;
            _type = type;
            _age = age;
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public string Type
        {
            get
            {
                return _type;
            }
            set
            {
                _type = value;
            }
        }

        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                _age = value;
            }
        }

        public override string ToString()
        {
            return $"Name: {_name}\nType: {_type}\nAge: {_age}\n\n";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericsAndContainers
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             * Containers
             * 
             * Types:
             * List. Dictionary, Stack, Queue
             * 
             * List
             *  Most array like
             *  Methods
             *      Add
             *      Remove
             *      Clear
             *      Count
             *      Capacity
             *      
             */

            /*
             * Dictionary
             * 
             * Key, Value - Pair
             *      Key - how your item is located
             *      Value - the item that is stored in the dictionary
             *      
             */

            Dictionary<string, Animal> pound2 = new Dictionary<string, Animal>();
            List<Animal> pound = new List<Animal>();

            bool running = true;

            while (running)
            {
                Console.WriteLine("1. Add an animal to the pound");
                Console.WriteLine("2. View animals");
                Console.WriteLine("3. Adopt an animal.");
                Console.WriteLine("4. Exit");
                string input = Console.ReadLine();

                switch (input.ToLower())
                {
                    case "1":
                    case "add":
                        {
                            Console.Write("What is the animals name? ");
                            string name = Console.ReadLine();

                            Console.Write($"What type of animal is {name}? ");
                            string type = Console.ReadLine();

                            int age = Validation.GetInt($"How old is {name}? ");

                            Animal animal = new Animal(name, type, age);

                            pound.Add(animal);
                        }
                        break;
                    case "2":
                    case "view":
                    case "view animals":
                        {
                            for(int i = 0; i < pound.Count; i++)
                            {
                                Console.WriteLine($"Name: {pound[i].Name}\nType: {pound[i].Type}\nAge: {pound[i].Age}");
                            }
                        }
                        break;
                    case "3":
                    case "adopt":
                        {
                            for(int i = 0; i< pound.Count; i++)
                            {
                                Console.WriteLine($"{i+1}. {pound[i].Name} - {pound[i].Type} - {pound[i].Age}");
                            }
                            int selection = Validation.GetInt(1, pound.Count, "Which animal would you like to adopt? (select the number): ");

                            pound.RemoveAt(selection - 1);
                        }
                        break;
                    case "4":
                    case "exit":
                        {
                            running = false;
                        }
                        break;
                }
                Console.Clear();

                Animal ani = new Animal("Fido", "Dog", 5);

                pound2.Add(ani.Name, ani);

                Console.WriteLine($"In the pound2 we have {pound2["Fido"]}");

                Animal temp = pound2["Fido"];

                if(pound2.ContainsKey("Bob") == false)
                {
                    pound2["Bob"] = temp;
                }

                pound2.Add("Fido", null);

                string[] keys = pound2.Keys.ToArray();

                foreach(string k in keys)
                {
                    Console.WriteLine(pound2[k]);
                }

                for(int i = 0; i < keys.Length; i++)
                {
                    Console.WriteLine(pound2[keys[i]]);
                }

                Utility.PauseBeforeContinuing();
            }

            /*
             * Stack
             *  Like the call stack
             *  
             * Queue
             *  Enqueue
             *  Dequeue
             */

            //Utility.PauseBeforeContinuing();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ObjectOrientedBasics;

namespace ObjectOrientedBasics
{
    class Preferences
    {
        bool _shouldAutoPlay;
        int _volumeLevel;

        public bool GetShouldAutoPlay()
        {
            return _shouldAutoPlay;
        }

        public int GetVolumeLevel()
        {
            return _volumeLevel;
        }
        
        public void SetShouldAutoPlay(bool shouldAutoPlay)
        {
            _shouldAutoPlay = shouldAutoPlay;
        }

        public void SetVolumeLevel(int volumeLevel)
        {
            _volumeLevel = volumeLevel;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritenceAndEncapsulation
{
    class Program
    {
        static void Main(string[] args)
        {

            /*
             * Pillars of OOP
             * 1. Inheritence
             * 2. Polymorphism 
             *      - Method Overloading
             *      - Containment
             * 3. Encapsulation
             *    - Breaking programs into smaller functional pieces
             *    - Independent (Black Box)
             *    - Communicate by passing information
             *    
             * Encapsulation
             * 1. Blackbox
             * 2. Interface
             *      - What it does. Not how it does it.
             * 3. Access Modifiers
             *      - Public
             *      - Private
             *      - Protected
             *      - Used to prevent coupling with other sections of code
             *      - Used to control access of sections of code
             *      
             * What is effective Encapsulation
             * 1. Abstract
             *      - Solve a problem and reuse the solutions
             *      - Generic solution
             * 2. Implementation hidden
             *      - Protects your object
             *          - Example: Checking account balance
             *      - Protects user from implementation
             *          - How does your bank handle deposits / withdrawls from your account
             * 3. Division of responsibility
             *      - You had 1 job
             *          - Everything in a class should be related to that 1 job.
             *          - If it doesn't apply to that one responsibility it does not belon in that class.
             *          
             * C# Properties
             * 1. Simplification of creating getter / setter methods
             * 2. Declaring
             *      - shortcut: prop + tab x2
             * 3. Automatic Properties
             *      -get; set;
             *      - no backer field
             * 4. Access Modifiers
             *      - Read/Write only fields
             * 5. Virtual Fields
             * 6. What shoud be a property and what should be a method?
             *      - rule of thumb: Actions (methods) vs Data access (Properties
             *      
             * Inheritence
             * 1. In-a-relationship
            */

            User = currentUser;

            currentUser = new Super("Bob the minion", "123 Gru Lane", 110);
            Super sUser = new User("Dave the minion", "123 Gru Lane", 130);

            Console.WriteLine($"Name: {currentUser.Name} ");
            
            if(currentUser is Super)
            {
                Super temp = currentUser as Super;
                temp.DisplayStatus();
            }

            Console.WriteLine($"Security Level: {currentUser.SecurityLevle}");

            Console.WriteLine("\nPress any key ton continue.");
            Console.ReadKey();
        }
    }
}

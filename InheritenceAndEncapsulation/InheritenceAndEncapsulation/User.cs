﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ObjectOrientedBasics;

namespace ObjectOrientedBasics
{
    class User
    {
        private string _name;
        private string _address;
        private int _age;

        //Containment
        private Preferences _preferences;
        
        public User(string name, string address, int age)
        {
            _name = name;
            _address = address;
            _age = age;
        }

        public void SetPreferences(Preferences preferences)
        {
            _preferences = preferences;
        }

        public Preferences Pref
        {
            get
            {
                return _preferences;
            }
            set
            {
                _preferences = value;
            }
        }
        public string Address
        {
            get
            {
                return _address;
            }
            set
            {
                _address = value;
            }
        }
        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                _age = value;
            }
        }

        public virtual int SecurityLevel
        {
            get
            {
                return 1;
            }
        }

        string _first;
        string _last;

        public string FullName
        {
            get
            {
                return $"{_first} {_last}";
            }
        }
        public void SetName(string name)
        {
            _name = name;
        }

        public void SetAddress(string address)
        {
            _address = address;
        }

        public void SetAge(int age)
        {
            _age = age;
        }
    }
}

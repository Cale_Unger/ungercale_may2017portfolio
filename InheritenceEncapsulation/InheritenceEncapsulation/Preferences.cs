﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritenceEncapsulation
{
    class Preferences
    {
        bool _shouldVideosAutoPlay;
        double _volumeLevel;

        public bool ShouldVideosAutoPlay
        {
            get
            {
                return _shouldVideosAutoPlay;
            }
            set
            {
                _shouldVideosAutoPlay = value;
            }
        }

        public double VolumeLevel
        {
            get
            {
                return _volumeLevel;
            }
            set
            {
                _volumeLevel = value;
            }
        }
        
        public Preferences(bool shouldVideosAutoPlay, double volumeLevel)
        {
            _shouldVideosAutoPlay = shouldVideosAutoPlay;
            _volumeLevel = volumeLevel;
        }
    }
}

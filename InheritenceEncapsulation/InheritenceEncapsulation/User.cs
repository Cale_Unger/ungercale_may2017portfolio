﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritenceEncapsulation
{
    class User
    {
        /*
         * Classes
         * 
         * Creating your own custom type
         *   Groups related methods and variables
         *   Container for related data about a single item
         *   Blueprint
         *   
         * Represent
         *   Tangible items
         *       Car, House, Person
         *   Intangible
         *       Checking account, graphics renderer, internet connection
         *       
         * Defining
         *   Keyword: Class
         *   Syntax: class NameOfYOurClass { // body of the class }
         *   
         *   A CLASS SHOULD ALWAYS BE DEFINED IN ITS OWN FILE
         */

        //Fields
        private string _name;
        private string _address;
        private int _age;

        private Preferences _preferences;

        public virtual int SecurityLevel
        {
            get
            {
                return 1;
            }
        }

        //Properties
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public string Address
        {
            get
            {
                return _address;
            }
            set
            {
                _address = value;
            }
        }

        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                _age = value;
            }
        }

        public Preferences Preferences
        {
            get
            {
                return _preferences;
            }
            set
            {
                _preferences = value;
            }
        }

        //Constructors
        public User(string name, string address, int age)
        {
            _name = name;
            _address = address;
            _age = age;
        }

        //Default Constructor
        public User()
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritenceEncapsulation
{
    class Validation
    {
        static public int GetInt(string message = "Enter an integer: ")
        {
            int validatedInt;
            string input;

            do
            {
                Console.Write(message);
                input = Console.ReadLine();
            }
            while (Int32.TryParse(input, out validatedInt) == false);

            return validatedInt;
        }

        static public int GetInt(int min, int max)
        {
            int validatedInt;
            string input;

            do
            {
                Console.Write("Enter an integer: ");
                input = Console.ReadLine();
            }
            while (Int32.TryParse(input, out validatedInt) == false || (validatedInt < min || validatedInt > max));

            return validatedInt;
        }

        static public int GetInt(int min, int max, string message = "Enter and integer: ")
        {
            int validatedInt;
            string input;

            do
            {
                Console.Write(message);
                input = Console.ReadLine();
            }
            while (Int32.TryParse(input, out validatedInt) == false || (validatedInt < min || validatedInt > max));

            return validatedInt;
        }

        static public bool GetBool(string message = "Enter Y or N: ")
        {
            bool selectedOption = true;
            bool keepAsking = true;
            string input = null;

            do
            {
                Console.WriteLine(message);
                input = Console.ReadLine();

                switch (input.ToLower())
                {
                    case "y":
                    case "yes":
                        {
                            selectedOption = true;
                            keepAsking = false;
                        }
                        break;
                    case "n":
                    case "no":
                        {
                            selectedOption = false;
                            keepAsking = false;
                        }
                        break;
                }
            }
            while (keepAsking);

            return selectedOption;
        }
    }
}

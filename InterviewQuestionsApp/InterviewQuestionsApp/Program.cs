﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterviewQuestionsApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Clear();

            int[] someValues = new int[] { 1, 2, 3, 4, 5};
            string[] moreValues = new string[] {"as", "asd", "foo", "foog"};
            string[] retVals = GetStringPrefixes(moreValues);

            if(SumOfThreeNumbers(someValues, 10))
            {
                Console.WriteLine("Found a set of 3.");
                Console.ReadKey();
            }
            else
            {
                Console.WriteLine("No set found.");
                Console.ReadKey();
            }

            FindMajorityNumber(someValues);

            foreach(string s in retVals)
            {
                if (!s.Equals(null) && !s.Equals(""))
                {
                    Console.WriteLine(s);
                }
            }
        }

        static string[] GetStringPrefixes(string[] strings)
        {
            string[] retVal = new string[strings.Length];
            int currentRetIndex = 0;

            Array.Sort(strings);

            for(int i = 0; i < strings.Length; i++)
            {
                if (!strings[i].Equals(""))
                {
                    retVal[currentRetIndex] = strings[i];
                    currentRetIndex++;
                    for (int j = i + 1; i < strings.Length; j++)
                    {
                        if (strings[j].StartsWith(strings[i]))
                        {
                            strings[j] = "";
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }

            return new string[] { };
        }

        static int FindMajorityNumber(int[] values)
        {
            Dictionary<int, int> histogram = new Dictionary<int, int>();
            foreach(int i in values)
            {
                if (histogram.ContainsKey(i))
                {
                    histogram[i]++;
                    if(histogram[i] > values.Length / 2)
                    {
                        return i;
                    }
                }
                else
                {
                    histogram.Add(i, 1);
                }
            }
            throw new Exception("Didn't find a majority value.");
        }

        static bool SumOfThreeNumbers(int[] values, int valueToFind)
        {
            for (int i = 0; i < values.Length; i++)
            {
                for (int j = i + 1; j < values.Length; j++)
                {
                    for (int k = j + 1; k < values.Length; k++)
                    {
                        int temp = values[i] + values[j] + values[k];
                        if(temp == valueToFind)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
    }
}

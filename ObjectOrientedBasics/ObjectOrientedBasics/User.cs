﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectOrientedBasics
{
    class User
    {
        string _name;
        string _address;
        int _age;

        Preferences _preferences;
        
        public User(string name, string address, int age)
        {
            _name = name;
            _address = address;
            _age = age;
        }

        public void SetPreferences(Preferences preferences)
        {
            _preferences = preferences;
        }

        public string GetName()
        {
            return _name;
        }

        public string GetAddress()
        {
            return _address;
        }

        public int GetAge()
        {
            return _age;
        }

        public void SetName(string name)
        {
            _name = name;
        }

        public void SetAddress(string address)
        {
            _address = address;
        }

        public void SetAge(int age)
        {
            _age = age;
        }

    }
}

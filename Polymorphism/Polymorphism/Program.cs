﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphism
{
    class Program
    {
        static void Main(string[] args)
        {

            List<Person> people = new List<Person>();
            people.Add(new Person("Bob"));
            people.Add(new Person("Amie"));
            people.Add(new Person("Jamie"));

            int i = 0;
            for(; i< people.Count; i++)
            {
                Console.WriteLine($"{i+1}: {people[i].Name}");
            }

            Console.WriteLine("Press a key to continue");
            Console.ReadKey();

            /*Polymorphism
             *      Treat a child as if it were the parent object
             *      Treat a collection of objects in a uniform way
             *      Upcast / downcast
             *          Can only access the options available to the curent type
             *          Can convert if you the type
             *          Keywords: "is' and "as"
             *              as returns null on a failed conversion
             *              
             *      Different behaviors form a single method
             *          Treat one type like another
             *      Extensible
             *          Add children later and they will work with existing code
             *          Virtual / Override
             *          
             */

            List<Shape> myShapes = new List<Shape>();
            myShapes.Add(new Square());
            myShapes.Add(new Triangle());

            foreach(Shape s in myShapes)
            {
                s.Draw();
                Console.WriteLine();
            }

            Console.WriteLine("Press any key to continue");
            Console.ReadKey();
        }
    }
}

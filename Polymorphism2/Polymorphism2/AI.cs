﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphism2
{
    class AI : Player
    {
        Random _rand;

        public AI(char symbol) : base(symbol)
        {
            _rand = new Random();
        }

        public override void MakeMove(Board b)
        {
            b.CurrentPlayer = _symbol;

            bool moveIsValid = false;

            while(moveIsValid == false)
            {
                moveIsValid = b.SelectPositionByIndex(_rand.Next(0, 8));
            }  
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphism2
{
    class Board
    {
        char[] _boardState;

        char _currentPlayer;

        private bool _running;

        int _cursorPosX;
        int _cursorPosY;

        public char CurrentPlayer
        {
            get
            {
                return _currentPlayer;
            }
            set
            {
                _currentPlayer = value;
            }
        }

        public bool Running
        {
            get
            {
                return _running;
            }
        }

        public Board()
        {
            _running = true;
            _boardState = new char[9];

            _cursorPosX = 0;
            _cursorPosY = 0;

            Console.CursorVisible = false;
        }

        public void Draw()
        {
            DrawBoard();
            DrawCursor();
        }

        private void DrawBoard()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            for(int i = 0; i < _boardState.Length; i++)
            {
                Console.SetCursorPosition(i % 3, i / 3);
                Console.Write(_boardState[i]);
            }
            Console.ForegroundColor = ConsoleColor.Gray;
        }

        private void DrawCursor()
        {
            Console.SetCursorPosition(_cursorPosX,_cursorPosY);
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("_");
            Console.ForegroundColor = ConsoleColor.Gray;
        }

        public void MoveLeft()
        {
            if(_cursorPosX > 0)
            {
                _cursorPosX--;
            }

            Draw();
        }

        public void MoveRight()
        {
            if(_cursorPosX < 2)
            {
                _cursorPosX++;
            }

            Draw();
        }

        public void MoveUp()
        {
            if(_cursorPosY > 0)
            {
                _cursorPosY--;
            }

            Draw();
        }

        public void MoveDown()
        {
            if(_cursorPosY < 2)
            {
                _cursorPosY++;
            }

            Draw();
        }

        public bool SelectPosition()
        {
            bool moveIsValid = false;

            int selection = 3 * _cursorPosY + _cursorPosX;

            if(_boardState[selection] == '\0')
            {
                _boardState[selection] = _currentPlayer;
                moveIsValid = true;
            }

            return moveIsValid;
        }

        public bool SelectPositionByIndex(int selection)
        {
            bool moveIsValid = false;

            if(selection >= 0 && selection <= _boardState.Length && _boardState[selection] == '\0')
            {
                _boardState[selection] = _currentPlayer;
                moveIsValid = true;
            }
            Draw();

            return moveIsValid;
        }

        private void CheckForWinner()
        {
            bool winnerFound = false;
            char winningPlayer = '\0';
            int[] winningPositions = null;

            //Check the rows for winners
            for(int i = 0; winnerFound = false &&  i <= 2;)
            {
                int index = i * 3;
                if(_boardState[index] != '\0' && (_boardState[index] == _boardState[index+1] && _boardState[index] == _boardState[index + 2]))
                {
                    winnerFound = true;
                    winningPlayer = _boardState[index];
                    winningPositions = new int[] { index, index + 1, index + 2 };
                }
            }
        }
    }
}

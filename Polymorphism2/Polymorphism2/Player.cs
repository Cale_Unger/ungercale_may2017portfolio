﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphism2
{
    class Player
    {
        protected char _symbol;

        public Player(char symbol)
        {
            _symbol = symbol;
        }

        public virtual void MakeMove(Board b)
        {
            bool moveIsValid = false;

            b.CurrentPlayer = _symbol;

            while(moveIsValid == false)
            {
                ConsoleKey key = Console.ReadKey().Key;

                switch (key)
                {
                    case ConsoleKey.LeftArrow:
                        {
                            b.MoveLeft();
                        }
                        break;
                    case ConsoleKey.RightArrow:
                        {
                            b.MoveRight();
                        }
                        break;
                    case ConsoleKey.UpArrow:
                        {
                            b.MoveUp();
                        }
                        break;
                    case ConsoleKey.DownArrow:
                        {
                            b.MoveDown();
                        }
                        break;
                    case ConsoleKey.Enter:
                        {
                            moveIsValid = b.SelectPosition();
                        }
                        break;
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphism2
{
    class Program
    {
        static void Main(string[] args)
        {
            //Tic-Tac-Toe

            Board ticTacToe = new Board();
            Player p1 = new Player('X');
            Player p2 = new AI('O');

            ticTacToe.Draw();

            do
            {
                p1.MakeMove(ticTacToe);
                p2.MakeMove(ticTacToe);
            }
            while (ticTacToe.Running);

        }
    }
}

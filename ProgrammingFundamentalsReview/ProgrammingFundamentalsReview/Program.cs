﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgrammingFundamentalsReview
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             * Do not use
             * 
             * goto
             * 
             * var
             * 
             * foreach
             * 
             */

            /*
             * Variables
             * 
             * creating a box to hold a specific type of data and giving it a name to reference it by
             * 
             * consist of
             *  (data type) (name) - (value it contains)
             */

            int var1;
            var1 = 5;

            double doubleValue = 3.14;

            /*
             * Basic operations
             * 
             * PEMDAS - Please Excuse My Dear Aunt Sally - Parenthesis Exponents Multiplication Division Addition Subtraction
             * Integer division - result is truncated
             * Modulus - gives remainder - the number you mod by establishes the max range of the values return. % 3 gives values 0-2
             * 
             */

            /*
             * Compound operations
             * 
             * *=
             * /=
             * +=
             * -=
             * 
             * 
             * Increment and decrement
             * Pre
             * ++var or --var
             * Post
             * var++ or var--
             */

            int v1 = 4;
            int v2 = 10;
            int sum = v1;
            //sum = sum + v2;
            sum += v1;

            /*
             * Relational operators
             * 
             * greater than >
             * less than <
             * equal to ==
             * greater than or equal to >=
             * less than or equal to <=
             * not !
             */

            if(v1 <= v2)
            {
                Console.WriteLine("v1 is less than or equal to v2.");
            }
            else
            {
                Console.WriteLine("");
            }

            /*
             * Input
             *  Console.Readline();
             *      Return a string that you'll have ot assign to a variable to use.
             *      
             * Output
             *  Console.WriteLine("optional string");
             *      Displays the itmes passed in as strings to the console
             *  Console.Write();
             */

            Console.WriteLine("Sample line of output");
            Console.Write("Please enter your name: ");
            string name = Console.ReadLine();
            //string interpolation
            Console.WriteLine($"Hello {name}!");
            Console.WriteLine("How old are you? (in years): ");
            string ageString = Console.ReadLine();
            int age = GetInt();
            Console.WriteLine($"{name} is {age} years old. \n");

            /* 
             * Arrays
             * 
             * single dimension
             *  int[] arrayOfInts;
             *  arrayOfInts = new int[size];
             *  arrayOfInts = {0, 2, 4, 5};
             *  
             * multiple dimension
             *  int[,] multiArrayOfInts = new int[size, otherSize];
             *  
             * length
             *  .length
             *      last valid index is always .length - 1
             */

            /*
             * Conditionals
             * 
             * if/else/else if
             * 
             * switch 
             *  similar to using an if/else/else if chain
             *  more efficient when checkking 4 or more options
             *  
             */

            Console.Write("Enter a value: ");
            string stringValue = Console.ReadLine();

            switch (stringValue)
            {
                case "42":
                    {
                        Console.WriteLine("The answer to life the universe and everything.");
                    }
                    break;
                default:
                    {
                        Console.WriteLine("Press enter.");
                    }
                    break;
            }
            decimal ageDecimal;
            decimal.TryParse(ageString, out ageDecimal);

            /*
             * Logical operators
             * 
             * and &&
             * or ||
             * not !
             * grouping ()
             * 
             * Short circuiting
             *  and && will not evaluate the second half if the first half fails
             *  
             */

            /*
             * Loops
             * 
             * for loop
             *  for(int i = 0; i < maxValue; i++)
             *  {
             *      
             *  }
             *  init step (int i = 0;)
             *  condition (i < maxValue;)
             *      force the loop to exit at some point is valid here
             *  increment (i++)
             *  
             * while loop
             *  might never execute the body of the loop
             * 
             * do {} while loop
             *  will always execute the body of the loop at least once
             * 
             * foreach - crutch
             *  
             */

            /*
             * Methods and functions
             * 
             * Why use
             *  reuse code in a single line
             *  organize the code - helps readability
             *  actis like a black box focused on solving a specific task
             * 
             */

            Console.WriteLine("Press a key to continue.");
            Console.ReadKey();
        }

        //Validated int
        //Method signature
        static int GetInt()
        {
            int validatedInt;
            string input;

            do
            {
                Console.Write("Enter an integer: ");
                input = Console.ReadLine();
            }
            while (Int32.TryParse(input, out validatedInt) == false);

            return validatedInt;
        }
    }
}

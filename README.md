# README #

The applications in this repository are fairly simple to run.
Navigate to the folder of the application you would like to run and do as follows:
*Open folder > open folder > double-click program.cs

### What is this repository for? ###

* This repository is for a collection of c# work made in Visual Studio by Cale Unger.
* Version: 1.1

### How do I get set up? ###

* Setup: To run any of the programs in this repository you will software that can process c# programs. 
         Ex: Visual Studio 2015
* Configuration: A standard installation of your choice of c# software.
* How to run tests: To run tests use the option within your program of choice to execute/run a program.

### Contribution guidelines ###

* Writing tests
* Code review: Simple give well organized constructive feedback.

### Who do I talk to? ###

* If you have any questions contact Cale Unger (Repo Owner) @caleunger@gmail.com

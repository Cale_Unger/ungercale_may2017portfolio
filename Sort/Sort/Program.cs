﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sort
{
    class Program
    {
        static void Main(string[] args)
        {
            Program app = new Program();

            int[] array = { 2, 7, 8 };
        }
    }
    private void display(int[] array)
    {
        Console.WriteLine("----------");
        foreach(int i in array)
        {
            Console.Write($"{i} ");
        }
        Console.WriteLine("\n----------");
    }
    private void sort(int[] array)
    {
        for(int i = 0; i< array.Length; i++)
        {
            for(int j = 1; j<array.Length - 1; j++)
            {
                //should the items swap positions
                if(array[i] > array[j])
                {
                    int temp = array[i];
                    array[j] = array[j+1];
                    array[j+1] = temp;

                    display(array);
                }
            }
        }
    }
}

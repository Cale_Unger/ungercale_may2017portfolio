﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    class Board
    {
        private char[] _gameBoard;

        public Board()
        {
            _gameBoard = new char[9];
        }

        public void Draw()
        {
            Console.WriteLine("    |    |    ");
            Console.WriteLine("--------------");
            Console.WriteLine("    |    |    ");
            Console.WriteLine("--------------");
            Console.WriteLine("    |    |    ");
        }

        private void DrawMoves()
        {
            for(int i = 0; i < _gameBoard.Length; i++)
            {
                if(_gameBoard[i] == 'x' || _gameBoard[i] == 'o')
                {
                    Console.SetCursorPosition(2 + 5 * i % 3,2 * i / 3);
                    Console.WriteLine("x");
                }
                
            }
        }

        public bool MakeMove(int i)
        {
            bool validMove = true;

            if(_gameBoard[i] != 'x' && _gameBoard[i] != 'o')
            {
                _gameBoard[i] = 'x';
            }
            else
            {
                validMove = false;
            }

            return validMove;
        }
    }
}

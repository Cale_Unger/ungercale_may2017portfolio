﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    class Player
    {
        int _playerNumber;

        public Player(int pNum)
        {
            _playerNumber = pNum;
        }

        public int GetMove()
        {
            int move = 0;
            string input;

            do
            {
                Console.WriteLine("Select a square (1 - 9) --> ");
                input = Console.ReadLine();
            }
            while (int.TryParse(input, out move) && (move <= 0 && move >= 10));

            

            return 0;
        }
    }
}

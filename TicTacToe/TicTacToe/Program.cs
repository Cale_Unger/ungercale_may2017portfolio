﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Board
             *      Draw
             *      ValidateMove
             *      CheckForWinner
             * Player
             *      GetMove
             *      
             */

            bool running = true;

            Console.WriteLine("Would you like to play tic tac toe (y/n)?");
            string input = Console.ReadLine();

            if (input.Equals("n"))
            {
                running = false;
            }

            Board game = new Board();
            Player p1 = new Player(1);

            while (running)
            {
                Console.Clear();
                game.Draw();
                int move = p1.GetMove();
                game.MakeMove(move);

                Console.WriteLine("Press any key to continue.");
            }
        }
    }
}

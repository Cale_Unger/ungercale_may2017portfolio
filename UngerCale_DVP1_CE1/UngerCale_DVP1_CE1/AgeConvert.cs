﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UngerCale_DVP1_CE1
{
    class AgeConvert
    {
        public void AgeConvertChallenge()
        {
            double age;
            double ageInDays;
            double ageInHours;
            double ageInMinutes;
            double ageInSeconds;
            string name;
            string ageString;

            Console.WriteLine("Age Convert Challenge");
            Console.WriteLine("---------------------");

            Console.WriteLine("Please enter your name.");
            name = Console.ReadLine();

            Console.WriteLine("Please enter your age.");
            ageString = Console.ReadLine();

            while (!(double.TryParse(ageString, out age)))
            {
                Console.WriteLine("Please only enter numbers for your age.");
                ageString = Console.ReadLine();
            }
            Console.WriteLine("Your name is {0} and you are {1} years old.", name, age);

            ageInDays = ageToDays(age);
            ageInHours = ageToHours(age);
            ageInMinutes = ageToMinutes(age);
            ageInSeconds = ageToSeconds(age);

            Console.WriteLine("{0}, your age in days is {1}.",name,ageInDays);
            Console.WriteLine("{0}, your age in hours is {1}.", name, ageInHours);
            Console.WriteLine("{0}, your age in minutes is {1}.", name, ageInMinutes);
            Console.WriteLine("{0}, your age in days is {1}.", name, ageInSeconds);
            Console.ReadKey();
        }
        public static double ageToDays(double age)
        {
            double ageDays = age* 365;
            return ageDays;
        }
        public static double ageToHours(double age)
        {
            double ageHours = (age * 365) * 24;
            return ageHours;
        }
        public static double ageToMinutes(double age)
        {
            double ageMinutes = ((age * 365) * 24) * 60;
            return ageMinutes;
        }
        public static double ageToSeconds(double age)
        {
            double ageSeconds = (((age * 365) * 24) * 60) * 60;
            return ageSeconds;
        }
    }
}

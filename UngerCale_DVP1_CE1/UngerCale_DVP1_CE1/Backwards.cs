﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UngerCale_DVP1_CE1
{
    class Backwards
    {
        public void BackwardsChallenge()
        {
            string input;

            Console.WriteLine("Backwards Challenge");
            Console.WriteLine("-------------------");

            Console.WriteLine("Please enter a 6 word sentence.");
            input = Console.ReadLine();

            input = validation(input);

            Console.WriteLine("Your sentence backwards is ");

            int sentanceCount = input.Trim().Split(' ').Count();
            string[] sentence = input.Split(' ');

            for (int i = sentence.Length - 1; i >= 0; i--)
            {
                string back = sentence[i];
                Console.Write(back + " ");

            }
            Console.ReadKey();
        }
       
        public string validation(string check)
        {
            while (string.IsNullOrWhiteSpace(check))
            {
                Console.WriteLine("The field is blank, please make an input.");
                check = Console.ReadLine();
            }
            return check;
            
        }
        
    }
}

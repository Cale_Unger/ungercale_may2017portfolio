﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UngerCale_DVP1_CE1
{
    class Menu
    {

        private SwapInfo swapProgram = null;
        private Backwards backwardsProgram = null;
        private AgeConvert ageProgram = null;
        private TempConvert tempProgram = null;



        public void MenuStart()
        {
            string input;
            while (true)
            {
                Console.WriteLine("Code Challenges");
                Console.WriteLine("---------------");
                Console.WriteLine("Press [1] for swapInfo");
                Console.WriteLine("Press [2] for Backwards");
                Console.WriteLine("Press [3] for AgeConvert");
                Console.WriteLine("Press [4] for TempConvert");
                Console.WriteLine("Press [5] to Exit");
                input = Console.ReadLine().ToLower();

                switch (input)
                {
                    case "1":
                    case "swapinfo":
                        {
                            swapProgram = new SwapInfo();
                            swapProgram.SwapInfoChallenge();
                        }
                        break;
                    case "2":
                    case "backwards":
                        {
                            backwardsProgram = new Backwards();
                            backwardsProgram.BackwardsChallenge();
                        }
                        break;
                    case "3":
                    case "ageconvert":
                        {
                            ageProgram = new AgeConvert();
                            ageProgram.AgeConvertChallenge();
                        }
                        break;
                    case "4":
                    case "tempconvert":
                        {
                            tempProgram = new TempConvert();
                            tempProgram.TempConvertChallenge();
                        }
                        break;
                    case "5":
                    case "exit":
                        {
                            Environment.Exit(0);
                        }
                        break;
                    default:
                        Console.WriteLine("Invalid Selection");
                        Console.ReadKey();
                        break;
                }
               
                Console.Clear();
            }
        }
    }
}

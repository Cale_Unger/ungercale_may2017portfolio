﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UngerCale_DVP1_CE1
{
    class Program
    {
        private static Menu menuProgram = null;

        static void Main(string[] args)
        {
            menuProgram = new Menu();
            menuProgram.MenuStart();
        }
    }
}

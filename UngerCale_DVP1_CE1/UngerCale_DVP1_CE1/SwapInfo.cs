﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UngerCale_DVP1_CE1
{
    class SwapInfo
    {
        string firstName;
        string lastName;
       

        public void SwapInfoChallenge()
        {
            Console.WriteLine("SwapInfo Challenge");
            Console.WriteLine("------------------");

            Console.WriteLine("Please enter your first name.");
            firstName = Console.ReadLine();

            Console.WriteLine("Got it. Your first name is {0}.", firstName);

            Console.WriteLine("Please enter your last name");
            lastName = Console.ReadLine();

            Console.WriteLine("Got it. Your last name is {0}.", lastName);
            ArrayList swap = new ArrayList();

            swap.Add(lastName);
            swap.Add(firstName);

            Console.WriteLine("\r");
            Console.Write("Your swap info is: ");
            for (int i = 0; i < swap.Count; i++)
            {  
                Console.Write("{0} ", swap[i]);
            }
            Console.ReadKey();

        }
        public string validation(string fn)
        {
            while (string.IsNullOrWhiteSpace(fn))
            {
                Console.WriteLine("The field is blank, please make an input.");
                fn = Console.ReadLine();
            }
            return fn;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UngerCale_DVP1_CE1
{
    class TempConvert
    {
        public void TempConvertChallenge()
        {
            //Declare Variables
            string tempNumber;
            double tempAmount;
            string tempType;
            double celsius;
            double fahrenheit;

            Console.WriteLine("Temp Convert Challenge");
            Console.WriteLine("---------------------");

            //Ask for temp input
            Console.WriteLine("Enter a temperature that you would like to convert.");
            tempNumber = Console.ReadLine();

            Console.WriteLine("Type 'C' to convert to fahrenheit or 'F' to convert to celcius.");
            tempType = Console.ReadLine();

            //Conditional
            if (tempType.ToLower() == "c")
            {
                tempAmount = int.Parse(tempNumber);
                fahrenheit = (tempAmount * 9 / 5) + 32;
                Console.WriteLine("The temperature is " + fahrenheit + " degrees fahrenheit.");
            }
            else if (tempType.ToLower() == "f")
            {
                tempAmount = int.Parse(tempNumber);
                celsius = (tempAmount - 32) * 5 / 9;
                Console.WriteLine("The temperature is " + celsius + " degrees celsius.");
            }
            Console.ReadKey();
        }
    }
}

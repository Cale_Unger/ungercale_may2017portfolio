﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UngerCale_Lab1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Clear();

            //Intro
            Console.WriteLine("I need to tell my instructor some things about myself.");

            //Name
            Console.WriteLine("My name is Cale Unger.");

            //Degree Program
            Console.WriteLine("My degree program is Mobile Development.");

            //Hobbies / Interests
            Console.WriteLine("My hobbies / interests are:");
            Console.WriteLine("  -Playing video games\n  -Coding\n  -Playing violin & guitar");

            //Desert Island Scenario
            Console.WriteLine("If I were stranded on a desert island what drink, book, and music album would I bring?\nI would bring:");
            Console.WriteLine("  -Drink: Water\n  -Book: Jurassic Park\n  -Album:Master of Puppets");
        }
    }
}

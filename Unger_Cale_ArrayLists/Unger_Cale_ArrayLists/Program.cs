﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unger_Cale_ArrayLists
{
    class Program
    {
        static void Main(string[] args)
        {

            /*Cale Unger
             * Array Lists
             * 3-10-17
             */

            Console.Clear();

            ThisArray();
        }
        public static void ThisArray()
        {
            //Intro
            Console.WriteLine("This program will take two array lists and loops through them to create sentences.");

            //Before Changes
            Console.WriteLine("\nBefore Changes (Original Array Output)\n---------------------------------------");

            //Array Lists
            ArrayList part1 = new ArrayList{ "The Ravens are", "The Playstation is", "Nikola Tesla was" };
            ArrayList part2 = new ArrayList{ "football team", "game console", "genius" };

            //Loop
            for (int i = 0; i < part1.Count; i++)
            {
                Console.WriteLine("{0} a {1}.", part1[i], part2[i]);
            }

            //Subtraction
            part1.Remove("The Ravens are");
            part1.Remove("The Playstation is");

            part2.Remove("football team");
            part2.Remove("game console");

            //Addition
            part1.Insert(0, "Star Wars has");
            part2.Insert(0, "great story");

            //After Changes
            Console.WriteLine("\nAfter Changes (New Array Output)\n---------------------------------------");

            //Loop to check if subtraction and addition was successful
            for (int i = 0; i < part1.Count; i++)
            {
                Console.WriteLine("{0} a {1}.", part1[i], part2[i]);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unger_Cale_Conditionals
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Clear();

            /*Cale Unger
             * Conditionals
             * 3-3-17
             */

            //---------------------------------------------------------------------------------------------
            //Temperature Converter
            //---------------------

            //Declare Variables
            string tempNumber;
            double tempAmount;
            string tempType;
            double celsius;
            double fahrenheit;
            //string tempString;

            //Intro
            Console.WriteLine("Temperature Converter\n-------------------------");

            //Ask for temp input
            Console.WriteLine("Enter a temperature that you would like to convert.");
            tempNumber = Console.ReadLine();

            Console.WriteLine("Type 'C' to convert to fahrenheit or 'F' to convert to celcius.");
            tempType = Console.ReadLine();

            //Conditional
            if (tempType.ToLower() == "c")
            {
                tempAmount = int.Parse(tempNumber);
                fahrenheit = (tempAmount * 9/5) + 32;
                Console.WriteLine("The temperature is " + fahrenheit + " degrees fahrenheit.");
            }
            else if (tempType.ToLower() == "f")
            {
                tempAmount = int.Parse(tempNumber);
                celsius = (tempAmount - 32) * 5/9;
                Console.WriteLine("The temperature is " + celsius + " degrees celsius.");


                /*Test 1
                 * Input: 32f
                 * Output: 0c
                 */

                /*Test 2
                 * Input: 100c
                 * Output: 212f
                 */

                /*Test 3
                 * Input: 50c
                 * Output: 122f
                 * A lower case letter doesn't make a difference in the case of c or f.
                 */

                /*Test 4 - My Own
                 * Input: 20f
                 * Output: -6.66c
                 */
            }

            //---------------------------------------------------------------------------------------------
            //Last Chance for Gas
            //---------------------

            //Declare Variables
            string tankHold;
            double tankAmount;
            string howFull;
            double  howMuch;
            string mpg;
            double mPG;

            //Intro
            Console.WriteLine("\nLast Chance for Gas\n---------------------");
            Console.WriteLine("You're about to pass the last gas station for the next 200 miles.\nLet's calculate whether you need to stop for gas or not.");

            //User Input
            Console.WriteLine("\nHow many gallons does your car tank hold?");
            tankHold = Console.ReadLine();
            tankAmount = double.Parse(tankHold);

            Console.WriteLine("How full is your gas tank?(Percentage)");
            howFull = Console.ReadLine();
            howMuch = double.Parse(howFull);

            Console.WriteLine("How many miles per gallon does your car go?(mpg)");
            mpg = Console.ReadLine();
            mPG = double.Parse(mpg);

            //Calculations
            double tankPercent = howMuch / 100;
            double distance = (tankAmount * tankPercent) * mPG;

            /*Test
            Console.WriteLine("Distance = " + distance);
            */

            //Conditionals
            if (distance > 200)
            {
                Console.WriteLine("Yes, you can drive " + distance + " more miles and can make it without stopping for gas!");
            }
            else
            {
                Console.WriteLine("No, you only have " + distance + " gallons of gas in your tank, better get gas now while you can!");

                /*Test 1
                 * Gallons: 20
                 * Gas Tank: 50
                 * MPG: 25
                 * Result: Yes, you can drive 250 more miles and can make it without stopping for gas!
                 */

                /*Test 2
                 * Gallons: 12
                 * Gas Tank: 60
                 * MPG: 20
                 * Result: No, you only have 144 gallons of gas in your tank, better get gas now while you can!
                 */

                /*Test 3 - My Own
                 * Gallons: 10
                 * Gas Tank: 20
                 * MPG: 30
                 * Result: No, you only have 60 gallons of gas in your tank, better get gas now while you can!
                 */
            }

            //---------------------------------------------------------------------------------------------
            //Grade Letter Calculator
            //-------------------------

            //Declare variables here
            string grade;
            int gradeNum;

            //Intro
            Console.WriteLine("\nWelcome to the Grade Letter Calculator!\n----------------------------------");

            //User input
            Console.WriteLine("What is your grade percentage for SDI?");
            grade = Console.ReadLine();
            gradeNum = int.Parse(grade);

            //Checks
            if (gradeNum < 0 || gradeNum > 100) {
                Console.WriteLine("What is your grade percentage for SDI?");
                grade = Console.ReadLine();
                gradeNum = int.Parse(grade);
            }
            else if (gradeNum >= 90 && gradeNum <= 100) {
                Console.WriteLine("You have a " + grade + "% which means you have earned an A in the class!");
            }
            else if (gradeNum >= 80 && gradeNum <= 89) {
                Console.WriteLine("You have a " + grade + "% which means you have earned a B in the class!");
            }
            else if (gradeNum >= 73 && gradeNum <= 79) {
                Console.WriteLine("You have a " + grade + "% which means you have earned a C in the class!");
            }
            else if (gradeNum >= 70 && gradeNum <= 72) {
                Console.WriteLine("You have a " + grade + "% which means you have earned a D in the class!");
            }
            else {
                Console.WriteLine("You have a " + grade + "% which means you have earned an F in the class!");

                /*Test 1
                 * Grade: 92
                 * Result:You have a 92% which means you have earned an A in the class!
                 */

                /*Test 2
                 * Grade: 80
                 * Result:You have a 80% which means you have earned a B in the class!
                 */

                /*Test 3
                 * Grade: 67
                 * Result:You have a 67% which means you have earned an F in the class!
                 */

                /*Test 4
                 * Grade: 120
                 * Result:The user is reprompted when entering an invalid value.
                 */

                /*Test 5 - My Own
                 * Grade: 100
                 * Result:You have a 100% which means you have earned an A in the class!
                 */
            }



            //---------------------------------------------------------------------------------------------
            //Discount Double Check
            //-------------------------

            //Declare variables here
            string firstItem;
            decimal firstCost;
            string secondItem;
            decimal secondCost;
            decimal total;
            decimal discount1;
            decimal discount2;

            //Intro
            Console.WriteLine("\nWelcome to Discount Double Check!\n----------------------------------");

            //User input
            Console.WriteLine("Enter the cost of your first item.");
            firstItem = Console.ReadLine();
            firstCost = decimal.Parse(firstItem);

            Console.WriteLine("Enter the cost of your second item");
            secondItem = Console.ReadLine();
            secondCost = decimal.Parse(secondItem);

            total = firstCost + secondCost;
            discount1 = total * 0.90m;
            discount2 = total * 0.95m;

            if (total >= 100) {
                Console.WriteLine("Your total purchase is $" + discount1 + ", which includes your 10% discount.");
            }
            else if (total >= 50 && total < 100){
                Console.WriteLine("Your total purchase is $" + discount2 + ", which includes your 5% discount.");
            }
            else{
                Console.WriteLine("Your total purchase is $" + total + ".");

                /*Test 1
                 * Cost 1: 45.50
                 * Cost 2: 75
                 * Total: 108.45
                 * Result: Your total purchase is $108.45, which includes your 10% discount.
                 */

                /*Test 2
                 * Cost 1: 30
                 * Cost 2: 25
                 * Total: 52.25
                 * Result: Your total purchase is $52.25, which includes your 5% discount.
                 */

                /*Test 3
                 * Cost 1: 5.75
                 * Cost 2: 12.50
                 * Total: 18.25
                 * Result: Your total purchase is $18.25.
                 */

                /*Test 4 - My Own
                 * Cost 1: 5
                 * Cost 2: 10
                 * Total: 15
                 * Result:Your total purchase is $15.00.
                 */
            }
        }
    }
    }


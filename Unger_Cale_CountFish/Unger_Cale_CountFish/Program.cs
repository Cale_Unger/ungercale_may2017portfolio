﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Unger_Cale_CountFish
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Cale Unger
             * Count Fish
             * 3-8-17
             */

            Console.Clear();

            //Declare variable
            ArrayList colors = new ArrayList{ "red", "blue", "green", "yellow", "blue", "green", "blue", "blue", "red", "green" };
            string choiceString;

            //Intro
            Console.Write("\nFish\n-----------------------------\n[");

            //This will output the array above.
            //No matter how it changes.
            foreach (string color in colors)
            {
                Console.Write(color+" ");
            }
            
            //Tell the user what's happening
            Console.Write("]\n\nThis program will find the total amount of fish that are the color you select.");

            //User Input
            Console.WriteLine("\nChoose (1) for Red, (2) for Blue, (3) for Green, or (4) for yellow.");
            choiceString = Console.ReadLine();

            //Check for validation
            while (!(choiceString == "1" || choiceString == "2" || choiceString == "3" || choiceString == "4"))
            {
                //Alert the user that something is wrong
                Console.WriteLine("\nPlease type a number from 1-4.");

                //Re-ask the question
                Console.WriteLine("Choose (1) for Red, (2) for Blue, (3) for Green, or (4) for yellow.");

                //Re-catch the response using the same variable
                choiceString = Console.ReadLine();
            }
            //Confirm the user's response
            Console.WriteLine("\nGot it! You entered the number {0}.\n", choiceString);

            //Checks to make sure that the current amount of fish are output based on the color chosen by the user.
            int i = 0;
            string colorChoice = "";

            foreach (string color in colors)
                {
                if (choiceString == "1" && color == "red") {
                    i++;
                    colorChoice = "red";
                }
                else if (choiceString == "2" && color == "blue") {
                    i++;
                    colorChoice = "blue";
                   
                }
                else if (choiceString == "3" && color == "green")
                {
                    i++;
                    colorChoice = "green";
                }
                else if (choiceString == "4" && color == "yellow")
                {
                    i++;
                    colorChoice = "yellow";
                }
            }
            Console.WriteLine("In the fish tank there are {0} fish of the color {1}.", i, colorChoice);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unger_Cale_CustomClass
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Cale Unger
             * Custom Class - Animal Shelter
             * 3-22-17
             */

            //The prompt stated that the user must be able to enter atleast 5 times, so I created an infinite loop
            //where the user can exit whenever they want.
            Console.Clear();

            //Intro
            Console.WriteLine("\nWelcome to the \"Orlando Little Paws Puppy Shelter\" calculator.");
            Console.WriteLine("\nPlease answer all questions below and exit whenever.\n");
            
            //Class
            ShelterPop dogs = new ShelterPop("", 20, 0, 10, 0);

            //Variables
            int i = 0;
            int difference;
            int addOrSubAmount;
            int currentPop = dogs.GetCurrentPop();
            string addOrSubString = "";
            string addOrSub = "";


            //Call upon the getter for the title
            Console.WriteLine("The current population of the our shelter is {0} dogs.", dogs.GetCurrentPop());
            while (i <= 5) {
                
                Console.WriteLine("\nWould you like to adopt or donate today?");
                addOrSubString = Console.ReadLine();
                i++;

                while(addOrSubString.ToLower() != "adopt" && addOrSubString.ToLower() != "donate")
                {
                    //Tell the user there is an error and re-prompt the question
                    Console.WriteLine("\nPlease only type in \"adopt\" or \"donate\".\nWould you like to adopt or donate today?.", addOrSubString);
                    addOrSubString = Console.ReadLine();

                }
                while(currentPop == 20 && addOrSubString.ToLower() == "donate")
                {
                    Console.WriteLine("You can only adopt because the shelter is full.");

                    Console.WriteLine("\nWould you like adopt or donate?");
                    addOrSubString = Console.ReadLine();
                }
                while(currentPop == 0 && addOrSubString.ToLower() == "adopt")
                {
                    Console.WriteLine("You can only donate because the shelter is full.");

                    Console.WriteLine("\nWould you like to adopt or donate today?");
                    addOrSubString = Console.ReadLine();
                }
                //Confirm the user's response
                Console.WriteLine("\nGot it! You entered the choice of {0}.", addOrSubString);

                
                Console.WriteLine("How many dogs would you like to {0}?",addOrSubString);
                addOrSub = Console.ReadLine();
                
                
                while(!(int.TryParse(addOrSub, out addOrSubAmount)))
                {
                    //Tell the user there is an error and re-prompt the question
                    Console.WriteLine("\nPlease only type in whole numbers.\nPlease type how many dogs you would like to {0}.", addOrSubString);
                    addOrSub = Console.ReadLine();

                }
                
                string currentPopString = dogs.AdoptOrDonate(addOrSubString, addOrSubAmount, currentPop);
                currentPop = int.Parse(currentPopString);

                

                if (currentPop > 20)
                {
                    difference = (currentPop - addOrSubAmount);
                    Console.WriteLine("\nThe shelter can't accept that many dogs. \nYou can only donate up to {0} more dogs.", difference);
                    Console.WriteLine("\nThe current population of the shelter is {0}.", difference);
                }
                else if (currentPop < 0)
                {
                    difference = (currentPop + addOrSubAmount)  ;
                    Console.WriteLine("\nYou can't adopt that many dogs. \nThe shelter only has {0} dogs.", difference);
                    Console.WriteLine("\nThe current population of the shelter is {0}.", difference);
                }
                //Output
                Console.WriteLine("\nYou have chosen to {0} {1} dogs.", addOrSubString, addOrSubAmount);

                if (currentPop == 0)
                {
                    
                    Console.WriteLine("\nThe current population of the shelter is {0}.", currentPop);
                    Console.WriteLine("The shelter is now empty.");
                    Console.WriteLine("Now only donations will be accepted.");

                }
                else if (currentPop == 20)
                {
      
                    Console.WriteLine("\nThe current population of the shelter is {0}.", currentPop);
                    Console.WriteLine("The shelter is now full.");
                    Console.WriteLine("Now only adoptions will be accepted.");

                }
                
                }
            }
        }
    }


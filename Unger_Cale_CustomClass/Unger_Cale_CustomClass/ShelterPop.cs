﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unger_Cale_CustomClass
{
    class ShelterPop
    {
        //Create member variables
        //Property, traits - describe the comic book itself
        //Local "global" variable
        //By default starts with a lowercase m

        //By default these are all private
        //They are accessible by anything in the same class

        string mAddOrSub;
        int mAddOrSubAmount;
        int mMaxPop;
        int mMinPop;
        int mCurrentPop;

        //Create our constructor function
        //The name must be the same as our class name
        //Does not have a return type

        //Setup our member variables
        //These are made form teh arguments caught with parameters
        //We use an underscore _ to start off our parameters - "proffessional rule"

        public ShelterPop(string _addOrSub, int _maxPop, int _minPop, int _currentPop, int _addOrSubAmount)
        {
            //Set the initial values of the member variables, using the incoming data
            //Re-define the value of the member that already exists above
            mAddOrSub = _addOrSub;
            mMaxPop = _maxPop;
            mMinPop = _minPop;
            mCurrentPop = _currentPop;
            mAddOrSubAmount = _addOrSubAmount;
        }

        //Getter - returns information back to where the function was called
        //We create a getter if we want the user to be able to see it

        //Because we are practicing we will return every member variable
        public string GetAddOrSub()
        {
            //Return the matching number variable
            return mAddOrSub;
        }
        public int GetMaxPop()
        {
            //Return the matching number variable
            return mMaxPop;
        }
        public int GetMinPop()
        {
            //Return the matching number variable
            return mMinPop;
        }
        public int GetCurrentPop()
        {
            //Return the matching number variable
            return mCurrentPop;
        }
        public int GetAddOrSubAmount()
        {
            return mAddOrSubAmount;
        }
        //Create the setters that change the value of our member variables permanently after the setter
        //Setter is another custom function
        public void SetCurrentPop(int _currentPop)
        {
            //Add the keyword of "this" = specifically calls upon the object that was called
            //Add in some validation to stop the negative values
            //Change the member variable
            if (_currentPop < 0)
            {
                //This will run if the book has been permanently damaged and is worthless
                //Set the value to the lowest value possible that we will accept aka zero
                this.mCurrentPop = 0;
            }
            else
            {
                this.mCurrentPop = _currentPop;
            }
        }
        public void SetAddOrSub(string _addOrSub)
        {
            this.mAddOrSub = _addOrSub;
        }
        public void SetMaxPop(int _maxPop)
        {
            this.mMaxPop = _maxPop;
        }
        public void SetMinPop(int _minPop)
        {
            this.mMinPop = _minPop;
        }
        //Create a custom function that will use our comic book member variables
        //Based on user input of the current year, it will tell us how old the book is
        public string AdoptOrDonate(string _addOrSub, int _addOrSubAmount, int _currentPop)
        {
            if (_addOrSub == "donate")
            {
                //return a string of no profit
                int add = _currentPop + _addOrSubAmount;
                string addString = add.ToString();
                return addString;
            }
            else if(_addOrSub == "adopt")
            {
                //return
                int subtract = _currentPop - _addOrSubAmount;
                string subtractString = subtract.ToString();
                return subtractString;
            }
            else
            {
                _addOrSub = "\nWould you like to adopt or donate today?";
                string addOrSub = Console.ReadLine();
                return _addOrSub;
            }
        }
    }
}

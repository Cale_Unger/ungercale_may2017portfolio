﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unger_Cale_FinalProject
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Cale Unger
             * 5/7/17
             * SDI Final Project Edited for Portfolio 1
             * Portfolio 1
             */

            Console.Clear();

            //Variables
            string howManyMeals;
            int numOfMeals;
            string calories;
            string sumOfCaloriesString;
            string totalCaloriesWantedString;
            string totalCaloriesLeftString;

            //Welcome
            Console.WriteLine("This program will calculate the amount of calories you have eaten in a day.");

            //Ask for input
            Console.WriteLine("\nHow many meals have you eaten today?");
            howManyMeals = Console.ReadLine();

            while (!(int.TryParse(howManyMeals, out numOfMeals)))
            {
                //Tell the user there is an error and re-prompt the question
                Console.WriteLine("\nPlease only type in whole numbers.\nPlease enter how many meals you have eaten today.");
                howManyMeals = Console.ReadLine();

            }

            //Parse required to catch GetCalories function
            numOfMeals = int.Parse(howManyMeals);

            //Catch GetCalories function
            calories = GetCalories(numOfMeals);

            //Print Output from GetCalories function
            Console.WriteLine(calories);

            //Catch TotalCalories function
            sumOfCaloriesString = TotalCalories(calories);

            //Print output from TotalCalories function
            Console.WriteLine("\nThe total calories you have eaten today is {0}.", sumOfCaloriesString);

            //Ask for input
            Console.WriteLine("\nHow many calories did you want to consume today?");
            totalCaloriesWantedString = Console.ReadLine();

            //Parse required to catch CaloriesLeft
            int sumOfCalories = int.Parse(sumOfCaloriesString);
            int totalCaloriesWanted = int.Parse(totalCaloriesWantedString);
            
            //Catch CaloriesLeft function
            totalCaloriesLeftString = CaloriesLeft(sumOfCalories,totalCaloriesWanted);

            //Parse required for over eaten or not conditional
            int totalCaloriesLeft = int.Parse(totalCaloriesLeftString);

            //Over eaten or not conditiional
            //Over eaten
            if(totalCaloriesLeft > totalCaloriesWanted)
            {
                Console.WriteLine("\nYou have over eaten by {0} calories today.",Math.Abs(totalCaloriesLeft));
            }
            //Can still eat
            else if(totalCaloriesLeft < totalCaloriesWanted)
            {
                Console.WriteLine("\nYou have {0} calories left to eat today.",Math.Abs(totalCaloriesLeft));
            }

            /*Test Data 1
             * # of meals = 3
             * 1st meal calories = 800
             * 2nd meal calories = 1000
             * 3rd meal calories = 1200
             * Calories wanted = 2500
             * Final output = 3600,1100
             * 
             * Test Data 2
             * # of meals = 2
             * 1st meal calories = 800
             * 2nd meal calories = 1000
             * Calories wanted = 1500
             * Final output =
             * 
             * Test Data 3
             * # of meals = 1
             * 1st meal calories = 1000
             * Calories wanted = 500
             * Final output = 
             */

        }
        public static string GetCalories(int numMeals)
        {
            int amountOfCalories;
            int i = 0;

            string[] numCalories = new string[numMeals];

            for(i = 0; i < numMeals; i++)
            {

                Console.WriteLine("\nEnter the amount of calories for meal number {0}.", i+1);
                numCalories[i] = Console.ReadLine();
                
                while (!(int.TryParse(numCalories[i], out amountOfCalories)))
                {
                    //Tell the user there is an error and re-prompt the question
                    Console.WriteLine("\nPlease only type in whole numbers.\nPlease enter the amount of calories for meal number {0}.",i+1);
                    numCalories[i] = Console.ReadLine();

                }
                
            }

            return numCalories[i-1];

        }
        public static string TotalCalories(string calorieList)
        {
            int i = 0;
            int total = 0;
            int calorieNum;

            calorieNum = int.Parse(calorieList);

            for(i = 0; i < calorieList.Length; i++)
            {
                
                total += calorieNum;
                
            }

            
            return total.ToString();
        }
        public static string CaloriesLeft(int totalCalories, int totalCaloriesWanted)
        {
            int output = 0; ;

            //Over eaten
            if(totalCalories > totalCaloriesWanted)
            {
                
                output = totalCalories - totalCaloriesWanted;
            }
            //Can still eat
            else if (totalCalories < totalCaloriesWanted)
            {
                
                output = totalCaloriesWanted - totalCalories;
            }

            return output.ToString();
        }
    }
}


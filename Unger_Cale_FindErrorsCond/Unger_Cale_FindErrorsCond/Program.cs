﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SDI_Find_Errors_Conditionals
{
    class Program
    {
        static void Main(string[] args)
        {
            //  NAME: Cale Unger
            //  DATE: 3-8-17
            //  Scalable Data Infrastructures
            //  Day 4 Lab
            //  Find and fix the errors

            String myName = "John Doe";
            String myJob = "Cat Wrangler";

            decimal myRatePerCat = 7.50m;
            decimal totalPay = 0;

            int numberOfCats = 40;
            bool employed = true;

            Console.WriteLine("Hello!  My name is " + myName + ".");
            Console.WriteLine("I'm a " + myJob + ".");
            Console.WriteLine("My current assignment has me wrangling " + numberOfCats + " cats.");
            Console.WriteLine("So, let's get to work!");

            while (numberOfCats > 0)
            {

                if (employed == true)
                {
                    totalPay += myRatePerCat;
                    Console.WriteLine("I've wrangled another cat and I have made $" + totalPay + " so far.  \r\nOnly " + numberOfCats + " left!");

                }
                else
                {

                }

                numberOfCats--;

                if (numberOfCats == 5)
                {

                    employed = false;
                    Console.WriteLine("I've been fired! Someone else will have to wrangle the rest!");
                }

            }

        }
    }
}
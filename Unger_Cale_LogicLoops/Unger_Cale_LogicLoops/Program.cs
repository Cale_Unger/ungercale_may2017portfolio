﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unger_Cale_LogicLoops
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Clear();

            /*Cale Unger
             * Logic Loops
             * 3-6-17
             * SDI
             */

            //Declare variables
            int tire1 = 0;
            string tireP1 = "";
            int tire2 = 0;
            string tireP2 = "";
            int tire3 = 0;
            string tireP3 = "";
            int tire4 = 0;
            string tireP4 = "";

            //Array
            int[] tirePressure = new int[4] { tire1, tire2, tire3, tire4 };
            string[] tireString = new string[4] { tireP1, tireP2, tireP3, tireP4 };

            //Intro
            Console.WriteLine("Tire Pressure\n--------------------");
            Console.WriteLine("This program will check as to whether or not you need a tire checkup.\nPlease follow the instructions below.\n");

            //User inputs / Feedback
            Console.WriteLine("Enter the pressure of your front left tire. Then press enter / return");
            tireP1 = Console.ReadLine();
          
            //Checks - Validation
            while (!(int.TryParse(tireP1, out tire1)))
            {
                //Alert the user that something is wrong
                Console.WriteLine("Oops, please only type in numbers!");

                //Re-ask the question
                Console.WriteLine("Please type in a number for the tire pressure");

                //Re-catch the response using the same variable
                tireP1 = Console.ReadLine();
            }
            //Confirm the user's response and ask for tire pressure
            Console.WriteLine("\r\nGot it! You entered a pressure of {0}.\r\nWhat is the pressure of your front right tire?", tire1);
            tireP2 = Console.ReadLine();

            while (!(int.TryParse(tireP2, out tire2)))
            {
                //Alert the user that something is wrong
                Console.WriteLine("Oops, please only type in numbers!");

                //Re-ask the question
                Console.WriteLine("Please type in a number for the tire pressure");

                //Re-catch the response using the same variable
                tireP2 = Console.ReadLine();

            }
            //Confirm the user's response and ask for tire pressure
            Console.WriteLine("\r\nGot it! You entered a pressure of {0}.\r\nWhat is the pressure of your back left tire?", tire2);
            tireP3 = Console.ReadLine();

            while (!(int.TryParse(tireP3, out tire3)))
            {
                //Alert the user that something is wrong
                Console.WriteLine("Oops, please only type in numbers!");

                //Re-ask the question
                Console.WriteLine("Please type in a number for the pressure.");

                //Re-catch the response using the same variable
                tireP3 = Console.ReadLine();
            }
            //Confirm the user's response and ask for tire pressure
            Console.WriteLine("\r\nGot it! You entered a pressure of {0}.\r\nWhat is the pressure of your back left tire?", tire3);
            tireP4 = Console.ReadLine();

            while (!(int.TryParse(tireP4, out tire4)))
            {
                //Alert the user that something is wrong
                Console.WriteLine("Oops, please only type in numbers!");

                //Re-ask the question
                Console.WriteLine("Please type in a number for the pressure.");

                //Re-catch the response using the same variable
                tireP4 = Console.ReadLine();
            }
            //Confirm the user's response
            Console.WriteLine("\r\nGot it! You entered a pressure of {0}. Press enter / return.", tire4);
            tireP4 = Console.ReadLine();

            if(tire1 == tire2 && tire3 == tire4)
            {
                Console.WriteLine("Your tires pass spec!\n");
            }
            else
            {
                Console.WriteLine("Get your tires checked out!\n");

                /*Test Data 1
                 * front right:32
                 * front left:32
                 * back right:30
                 * back left:30
                 * Output:Your tires pass spec!
                 * 
                 * Test Data 2
                 * front right:36
                 * front left:32
                 * back right:25
                 * back left:25
                 * Output:Get your tires checked out!
                 * 
                 * Test Data 3
                 * front right:20
                 * front left:20
                 * back right:40
                 * back left:40
                 * Output:Your tires pass spec!
                 */
            }

            //Declare variables
            int age;
            string ageString;
            int time;
            string timeString;
            decimal discount = 7.00m;
            decimal normal = 12.00m;

            //Intro
            Console.WriteLine("Movie Ticket Price\n--------------------");
            Console.WriteLine("This program will calculate the price of a movie ticket.\nPlease answer all questions below.\n");

            Console.WriteLine("How old are you?");
            ageString = Console.ReadLine();

            while (!(int.TryParse(ageString, out age)))
            {
                //Alert the user that something is wrong
                Console.WriteLine("Oops, please only type in numbers!");

                //Re-ask the question
                Console.WriteLine("Please type in a number for your age.");

                //Re-catch the response using the same variable
                ageString = Console.ReadLine();
            }
            //Confirm the user's response
            Console.WriteLine("\r\nGot it! You entered the age of {0}.\r\nWhat time is your movie?(Use military time 1 - 24)", age);
            timeString= Console.ReadLine();

            while (!(int.TryParse(timeString, out time)))
            {
                //Alert the user that something is wrong
                Console.WriteLine("Oops, please only type in numbers!");

                //Re-ask the question
                Console.WriteLine("Please type in a number for the time.");

                //Re-catch the response using the same variable
                timeString = Console.ReadLine();
            }
            //Confirm the user's response
            Console.WriteLine("\r\nGot it! You entered a time of {0}. Press enter / return.", time);
            timeString = Console.ReadLine();

            if (age >= 55 || age < 10)
            {
                Console.WriteLine("Your ticket price is $" + discount + ".");
            }
            else if(time>14 && time < 17)
            {
                Console.WriteLine("Your ticket price is $" + discount + ".");
            }
            else
            {
                Console.WriteLine("Your ticket price is $"+normal+".");

                /*Test Data 1
                 * age:57
                 * time:20
                 * price:7.00
                 * 
                 * Test Data 2
                 * age:9
                 * time:20
                 * price:7.00
                 * 
                 * Test Data 3
                 * age:38
                 * time:20
                 * price:12.00
                 * 
                 * Test Data 4
                 * age:25
                 * time:16
                 * price:7.00
                 * 
                 * My Own - 5
                 * age:5
                 * time:12
                 * price:7.00
                 */
            }

            //Declare variables
            int[] numberArray = new int[] {5,10,15,20,25,30};
            string oddOrEven;
            int sum = 0;

            //Intro
            Console.WriteLine("\nAdd up the odds or evens\n--------------------");
            Console.WriteLine("This program will add up odds or evens depending on your choice.\nPlease answer all questions below.\n");

            //User Input
            Console.WriteLine("Do you want to see the sum of odd or even numbers?");
            oddOrEven = Console.ReadLine();

            for (int i = 0; i < numberArray.Length; i++)
            {
                
                if (oddOrEven.ToLower() == "even" && i % 2 != 0)
                {
                    sum += numberArray[i];
                }
                else if(oddOrEven.ToLower() == "odd" && i % 2 == 0)
                {
                    sum += numberArray[i];
            }
               
            }
            Console.WriteLine("\nThe odd numbers add up to " +sum+ ".");

            /*Data Test 1
                 * Array: {1,2,3,4,5,6,7}
                 * Output Even: The even numbers add up to 12.
                 * Output Odd: The odd numbers add up to 16.
                 * 
                 * Data Test 2
                 * Array: {12,13,14,15,16,17}
                 * Output Even: The even numbers add up to 42.
                 * Output Odd: The odd numbers add up to 45.
                 * 
                 * Data Test 3
                 * Array: {5,10,15,20,25,30}
                 * Output Even: The even numbers add up to 60.
                 * Output Odd: The odd numbers add up to 45.
                 */

            //Declare variables
            decimal limit = 0;
            string limitString;
            decimal purchase = 0;
            string purchaseString;

            //Intro
            Console.WriteLine("\nCharge It!\n--------------------");
            Console.WriteLine("This program will calculate purchases until you reach your chosen credit limit.\nPlease answer all questions below.\n");

            //User Input
            Console.WriteLine("What is your credit limit?");
            limitString = Console.ReadLine();

            while (!(decimal.TryParse(limitString, out limit)))
            {
                //Alert the user that something is wrong
                Console.WriteLine("Oops, please only type in numbers!");

                //Re-ask the question
                Console.WriteLine("Please type in a number for your limit.");

                //Re-catch the response using the same variable
                limitString = Console.ReadLine();
            }
            //Confirm the user's response
            Console.WriteLine("\r\nGot it! You entered a limit of {0}.", limit);
            
            while(limit > 0)
            {

                Console.WriteLine("\nEnter a purchase amount.");
                purchaseString = Console.ReadLine();
                purchase = decimal.Parse(purchaseString);
                limit = limit - purchase;
                Console.WriteLine("Got it! You spent ${0} and can still spend ${1}.", purchase, limit);
                }
            if(limit <= 0)
            {
                Console.WriteLine("\nWith your last purchase of ${0} you have reached your credit limit and exceeded it by ${1}.\n", purchase, Math.Abs(limit));
            }
                /*Data Test 1
                 * limit: 20
                 * purchase 1: 5
                 * output: With your current purchase of $5, you can still spend $15.
                 * purchase 2: 12
                 * output: With your current purchase of $12, you can still spend $3.
                 * purchase 3: 7
                 * output: With your last purchase of $7 you have reached your credit limit and exceeded it by $4.
                 * 
                 * Data Test 2 - My Own
                 * limit: 100
                 * purchase 1: 50
                 * output: With your current purchase of $50, you can still spend $50.
                 * purchase 2: 75
                 * output: With your last purchase of $75 you have reached your credit limit and exceeded it by $25.
                 */
            }
        }
        }
    

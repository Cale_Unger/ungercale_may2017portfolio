﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unger_Cale_MadLibs
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Clear();

            /*Cale Unger
             * Mad Libs
             *3-1-17
             */

            //Variables
            string string1;
            string string2;
            string string3;

            //Arrays
            string[] stringArray = new string[5];  
            int[] intArray = new int[5];

            //Welcome
            Console.WriteLine("Welcome to Mad Michael's Mad Libs.\n");

            //User Prompt - Name
            Console.WriteLine("Please type a boy's name. Then Press return/enter");
            stringArray[0] = Console.ReadLine();

            //User Prompt - Animal
            Console.WriteLine("\nName an animal.");
            stringArray[1] = Console.ReadLine();
            
            //User Prompt - Animal Name
            Console.WriteLine("\nType a name you would give a pet.");
            stringArray[2] = Console.ReadLine();

            //User Prompt - Food
            Console.WriteLine("\nName a food that animal would eat.");
            stringArray[3] = Console.ReadLine();

            //User Prompt - Number of Animals
            Console.WriteLine("\nType a number");
            string1 = Console.ReadLine();
            intArray[0] = int.Parse(string1);

            //User Prompt - Amount of Food
            Console.WriteLine("\nType another number.");
            string2 = Console.ReadLine();
            intArray[1] = int.Parse(string2);

            //User Prompt - Miles
            Console.WriteLine("\nType a third number.");
            string3 = Console.ReadLine();
            intArray[2] = int.Parse(string3);

            //User Prompt - Store
            Console.WriteLine("\nName a store.");
            stringArray[4] = Console.ReadLine();

            //Mad Lib
            Console.WriteLine("\nLike any other start of the week " + stringArray[0] + " was biking " + intArray[0] + " mile(s) to " + stringArray[4] + ".");
            Console.WriteLine("But this week seemed different. It was early Monday morning and " + stringArray[0] + " had not been greeted by his pet " + stringArray[1] + " " + stringArray[2] + ".");
            Console.WriteLine("Normally " + stringArray[2] + " would greet "+stringArray[0]+" on his way out the front door.");
            Console.WriteLine(stringArray[0] + " brushed away the thought and continued on his way to " + stringArray[4] + ".");
            Console.WriteLine("He purchased " + stringArray[3] + " which was " + stringArray[2] + "'s favorite food. Along with many other things.");
            Console.WriteLine(stringArray[0] + " began his ride home hoping that " + stringArray[2] + " would greet him at the door upon his return.");
            Console.WriteLine("Upon returning home " + stringArray[2] + " was not at the door awaiting " + stringArray[0] + "'s return.");
            Console.WriteLine(stringArray[0] + " began to shout for " + stringArray[2] + " worried that something bad may have happened.");
            Console.WriteLine("But all of sudden "+stringArray[0]+" heard a loud noise coming from behind him.");
            Console.WriteLine("After turning around " + stringArray[0] + " saw " + stringArray[2] + " along with what seemed like "+intArray[1]+" other neigborhood pet(s)");
            Console.WriteLine(stringArray[0] + " was relieved that he would not have to search for another "+intArray[2]+" hour(s) to find "+stringArray[2]+"\nTHE END.");
        }
    }
}

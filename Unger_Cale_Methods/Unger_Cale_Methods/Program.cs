﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unger_Cale_Methods
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Cale Unger
             * Methods
             * 3-8-17
             */

            Console.Clear();

            //Declare variables
            double width;
            string widthString;
            double height;
            string heightString;
            int numCoats;
            string coatsString;
            double surfaceArea;
            string surfaceString;
            double numGallons;

            //Intro
            Console.WriteLine("Painting A Wall\n--------------------");
            Console.WriteLine("This program will calculate how many gallons of paint are needed to paint a wall.\nPlease answer all questions below. Then press return/enter.\n");

            Console.WriteLine("How wide is your wall in feet?(width)");
            widthString = Console.ReadLine();

            while (!(double.TryParse(widthString, out width)))
            {
                //Alert the user that something is wrong
                Console.WriteLine("\nOops, please only type in numbers!");

                //Re-ask the question
                Console.WriteLine("\nPlease type in a number for the width.");

                //Re-catch the response using the same variable
                widthString = Console.ReadLine();
            }
            //Confirm the user's response
            Console.WriteLine("\nGot it! You entered a height of {0}.\r\nHow tall is your wall in feet?(height)", width);
            heightString = Console.ReadLine();

            while (!(double.TryParse(heightString, out height)))
            {
                //Alert the user that something is wrong
                Console.WriteLine("\nOops, please only type in numbers!");

                //Re-ask the question
                Console.WriteLine("\nPlease type in a number for the height.");

                //Re-catch the response using the same variable
                heightString = Console.ReadLine();
            }
            //Confirm the user's response
            Console.WriteLine("\nGot it! You entered a width of {0}.\r\nHow many coats of paint will be applied to the wall?(integer)", height);
            coatsString = Console.ReadLine();

            while (!(int.TryParse(coatsString, out numCoats)))
            {
                //Alert the user that something is wrong
                Console.WriteLine("\nOops, please only type in numbers!");

                //Re-ask the question
                Console.WriteLine("\nPlease type in a number for the coats.");

                //Re-catch the response using the same variable
                coatsString = Console.ReadLine();
            }
            //Confirm the user's response
            Console.WriteLine("\r\nGot it! You entered a height of {0}.\r\nHow much surface area will one gallon of paint cover?(feet^2)", numCoats);
            surfaceString = Console.ReadLine();

            while (!(double.TryParse(surfaceString, out surfaceArea)))
            {
                //Alert the user that something is wrong
                Console.WriteLine("\nOops, please only type in numbers!");

                //Re-ask the question
                Console.WriteLine("\nPlease type in a number for the surface area.");

                //Re-catch the response using the same variable
                surfaceString = Console.ReadLine();
            }
            //Confirm the user's response
            Console.WriteLine("\r\nGot it! You entered a surface area of {0}.\n", surfaceArea);

            numGallons = gallons(width, height, numCoats, surfaceArea);

            //Final Output
            Console.WriteLine("For {0} coats on the wall, you will need {1} gallons of paint.\n", numCoats, numGallons);

            /*Test Data
             * width:8
             * height:10
             * coats:2
             * surface area:300
             * result:For 2 coats on the wall, you will need 0.533333333333333 gallons of paint.
             * 
             * Test Data 2
             * width:30
             * height:12.5
             * coats:3
             * surface area:350
             * result:For 3 coats on the wall, you will need 3.21428571428571 gallons of paint.
             * 
             * Test Data
             * width:10
             * height:10
             * coats:2
             * surface area:400
             * result:For 2 coats on the wall, you will need 0.5 gallons of paint.
             */

            // ----------------------------------------------------------------------------------------------------------------
            //Declare variables
            double weight;
            string weightString;
            double stings;

            //Intro
            Console.WriteLine("Stung!\n--------------------");
            Console.WriteLine("This program will calculate how many bee stings are needed to kill an animal of a user input weight.\nPlease answer all questions below.\n");

            Console.WriteLine("What is the weight of your animal?");
            weightString = Console.ReadLine();

            while (!(double.TryParse(weightString, out weight)))
            {
                //Alert the user that something is wrong
                Console.WriteLine("\nOops, please only type in numbers!");

                //Re-ask the question
                Console.WriteLine("\nPlease type in a number for the weight.");

                //Re-catch the response using the same variable
                weightString = Console.ReadLine();
            }
            //Confirm the user's response
            Console.WriteLine("\r\nGot it! You entered a weight of {0}.\n", weight);

            stings = sting(weight);

            Console.WriteLine("It takes {0} bee stings to kill this animal.\n", stings);

            /*Test Data
             *weight:10
             *result:It takes 90 bee stings to kill this animal.
             * 
             * Test Data 2
             * weight:160
             * result:It takes 1440 bee stings to kill this animal.
             * 
             * Test Data 3
             * weight:twenty
             * result:(Re-prompt), User Enters: 20, Then: It takes 180 bee stings to kill this animal.
             * 
             * Test Data 4
             * weight:90
             * result:It takes 810 bee stings to kill this animal.
             */

            //-----------------------------------------------------------------------------------------------------------

            //Declare Variables
            string[] arrayList = new string[] { "1...", "2...", "3..." };

            //string reverse = reverse(arrayList);

            //Intro
            Console.WriteLine("Reverse It!\n--------------------");
            Console.WriteLine("This program will output a hardcoded array in reverse.\n");

            //Output Arrays
            Console.Write("Your original array was [");
            for (int i=0; i < arrayList.Length; i++)
            {
                Console.Write(arrayList[i]+" ");
            }

            string reverseList = Reverse(arrayList);

            Console.WriteLine("] and now it is reversed as [" + reverseList + "].");

            /*Data Test 1
             * Array: { "Apple", "Pear", "Peach", "Coconunt", "Kiwi" }
             * Output: Your original array was Apple Pear Peach Coconunt Kiwi and now it is reversed as Kiwi Coconunt Peach Pear Apple .
             * 
             * Data Test 2
             * Array: { "Red", "Yellow", "Orange", "Green", "Blue", "Indigo", "Violet" }
             * Output: Your original array was Red Yellow Orange Green Blue Indigo Violet and now it is reversed as Violet Indigo Blue Green Orange Yellow Red .
             * 
             * Data Test 3
             * Array:
             * Output:
             */

        }

        public static double gallons(double w, double h, int numCoats, double sa)
        {
            //The formula for the area of a rectangle is
            //width*height

            //Do the math stuff and store it in a variable
            double gallons = (w * h) * (numCoats) / (sa);

            //Return the area to our main code
            return gallons;
        }
        public static double sting(double w)
        {

            double sting = w * 9;
            return sting;
        }
        
        public static string Reverse(string[] a)
        {
            string reverse = "";

            for (int i = a.Length - 1; i >= 0; i--)
            {
                reverse += a[i]+" ";
            } 
            return reverse;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unger_Cale_RestaurantCalc
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Clear();

            /*Cale Unger
             * Restaurant Calculator
             * 3-1-17
             */

            //Declare variables here
            string check1;
            decimal checkAmount1;
            string check2;
            decimal checkAmount2;
            string check3;
            decimal checkAmount3;
            string tipPercent;
            decimal tipPercentAmount;
            decimal subWithoutTip;
            decimal totalTip;
            decimal grandTotalPlusTip;
            decimal cpp;
            
            //Intro
            Console.WriteLine("Welcome to the RestaurantCalc App. The app currently only accepts three meal amounts at a time. Please follow all instructions below carefully to get an accurate calculation of your bill.");

            
            //Meal 1
            Console.WriteLine("\nEnter the cost of the first meal. Then press return/enter.");
            check1 = Console.ReadLine();

            //Meal 2
            Console.WriteLine("Enter the cost of the second meal.");
            check2 = Console.ReadLine();

            //Meal 3
            Console.WriteLine("Enter the cost of the third meal.");
            check3 = Console.ReadLine();

            //Tip
            Console.WriteLine("Please enter a tip percentage based on the quality of your service.");
            tipPercent = Console.ReadLine();

            //Parsing
            checkAmount1 = decimal.Parse(check1);
            checkAmount2 = decimal.Parse(check2);
            checkAmount3 = decimal.Parse(check3);
            tipPercentAmount = decimal.Parse(tipPercent);

            //Calculations
            subWithoutTip = checkAmount1 + checkAmount2 + checkAmount3;
            totalTip = subWithoutTip * (tipPercentAmount/100);
            grandTotalPlusTip = subWithoutTip + totalTip;
            cpp = grandTotalPlusTip / 3;

            //Summary - Display all information given
            Console.WriteLine("\nHere is a summary of your entered information.\nMeal 1: $"+checkAmount1+"\nMeal 2: $"+checkAmount2+"\nMeal 3: $"+checkAmount3+"");
            Console.WriteLine("Tip Percentage: " + tipPercent+"%\n");

            //Totals
            Console.WriteLine("Here are all the totals for your bill.\nSub-total: $"+subWithoutTip+"\nTip Amount: $"+totalTip+"\nTotal: $"+grandTotalPlusTip+"");
            Console.WriteLine("\nIf you would like to split the bill evenly three ways everyone's individual total is $" + cpp+".");

            //End
            Console.WriteLine("\nThanks for using the RestaurantCalc App. Have a nice day. :)\n");

            /*Test Data #1
             * Check #1:$10.00
             * Check #2:$15.00
             * Check #3:$25.00
             * Tip: 20%
             * Sub-total without tip:$50.00
             * Total-tip:$10.00
             * Grand total with tip:$60.00
             * Cost per person:$20.00
             * 
             * Computer repeated all information as shown above.
             * Math is correct.
             */

            /*Test Data #2
             * Check #1:$20.25
             * Check #2:$17.75
             * Check #3:$23.90
             * Tip: 10%
             * Sub-total without tip:$61.90
             * Total-tip:$6.19
             * Grand total with tip:$68.09
             * Cost per person:$22.696666666666666666666666667
             * 
             * Computer repeated all information as shown above.
             * Math is correct.
             */

            /*Test Data #3 - My own data test
             * Check #1:$8
             * Check #2:$10
             * Check #3:$5.50
             * Tip: 5%
             * Sub-total without tip:$23.50
             * Total-tip:$1.17
             * Grand total with tip:$24.67
             * Cost per person:$8.2250
             * 
             * Computer repeated all information as shown above.
             * Math is correct.
             */
        }
    }
}

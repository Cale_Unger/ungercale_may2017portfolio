﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unger_Cale_StringObjects
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Cale Unger
             * String Objects
             * 3-15-17
             */

            Console.Clear();

            //Call
            email();
            separators();
        }
        public static void email()
        {
            //Email Address Checker
            //---------------------------

            //Intro
            Console.WriteLine("Email Address Checker\n---------------------------");

            //Declare Variables
            string email;
            int indexOfSymbol;
            string validEmail;
            string valid = "";
            string invalid = "";
            int lastIndex;

            //User input
            Console.WriteLine("Enter an email address. Then press return.");
            email = Console.ReadLine();
           
            //Get index of "@" and "."
            indexOfSymbol = email.IndexOf("@");
            lastIndex = email.LastIndexOf("@");

            //Validation
            validEmail = emailCheck(indexOfSymbol, lastIndex, valid, invalid, email);

            //Output
            Console.WriteLine(validEmail);

            /*Data Test 1
             * input: test@fullsail.com
             * output: The email address of test@fullsail.com is a valid email address.
             * 
             * Data Test 2
             * input: test@full@sail.com
             * output: The email address of test@full@sail.com is not a valid email address.
             * 
             * Data Test 3
             * input: test@full sail.com
             * output: The email address of test@full sail.com is not a valid email address.
             * 
             * Data Test 4
             * input: caleunger@gmail.com
             * output: The email address of caleunger@gmail.com is a valid email address.
             */
        }
        public static void separators()
        {
            //Separator Swap Out
            //---------------------------

            //Intro
            Console.WriteLine("Separator Swap Out\n---------------------------");

            //Declare variables
            string list;
            string separator;
            string newSeparator;

            //User input
            Console.WriteLine("\nMake a list with each item separated by a symbol. Then press return.");
            list = Console.ReadLine();

            Console.WriteLine("\nEnter the symbol you used to separate the listed items.");
            separator = Console.ReadLine();

            Console.WriteLine("\nEnter the new symbol that you would like to replace the old one with.");
            newSeparator = Console.ReadLine();

            string redoList = newList(newSeparator, list, separator);

            //Output
            Console.WriteLine("\nThe original string of {0} with the new separator is {1}.",list,redoList);

            /*Data Test 1
             * list: 1,2,3,4,5
             * separator: ,
             * new separator: -
             * output: The original string of 1,2,3,4,5 with the new separator is 1-2-3-4-5.
             * 
             * Data Test 2
             * list: red: blue: green: pink:
             * separator: :
             * new separator: -
             * output: The original string of red: blue: green: pink: with the new separator is red- blue- green- pink-.
             * 
             * Data Test 3
             * list: computer.phone.tablet.
             * separator: .
             * new separator: ,
             * output: The original string of computer.phone.tablet. with the new separator is computer,phone,tablet,.
             */

        }
        //Creates list with new separator
        public static string newList(string ns,string l,string s)
        {
                string replace = l.Replace(s, ns);
                string newList = replace;
                return newList;
        }
        //Checks if the user input fits the valid email criteria
        public static string emailCheck(int iOS, int lastI, string v, string inv, string email)
        {
        if(iOS == lastI && !email.Contains(" ")){
                v = "\nThe email address of " +email+ " is a valid email address.\n";
                string emailCheck = v;
                return emailCheck;
            }
        else{
                inv = "\nThe email address of "+email+" is not a valid email address.\n";
                string emailCheck = inv;
                return emailCheck;
        }
        
        }
    }
}

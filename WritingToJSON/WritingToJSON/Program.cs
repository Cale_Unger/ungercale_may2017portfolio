﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WritingToJSON
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] labels;
            string[] characters;

            using (StreamReader sr = new StreamReader("../../Data/labels.txt"))
            {
                //char[] splitItems = ['\r', '\n'};
                //labels
                labels = sr.ReadToEnd().Split('\n');
            
            }

            using (StreamReader sr = new StreamReader("../../Data/characters.txt"))
            {
                characters = sr.ReadToEnd().Split('\n');
            }

            string JSONtoOutput = "";

            JSONtoOutput += "[";
            for(int index = 0; index < characters.Length; ++index)
            {
                int modIndex = index % labels.Length;
                if(modIndex == 0)
                {
                    JSONtoOutput += "{";

                }
                else
                {
                    JSONtoOutput += ",";
                }

                JSONtoOutput += $"\"{labels[modIndex]}\" : \"{characters[modIndex]}\"";

                if (modIndex == labels.Length)
                {
                    JSONtoOutput += "}";
                }
            }
            JSONtoOutput += "]";

            using (StreamWriter sw = new StreamWriter("../../Data/save.json"))
            {
                sw.WriteLine(JSONtoOutput);
            }

            Console.WriteLine("Press a key to exit.");
            Console.ReadKey();
        }
    }
}

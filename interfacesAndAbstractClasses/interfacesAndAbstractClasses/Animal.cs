﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace interfacesAndAbstractClasses
{
    abstract class Animal
    {
        protected string _name;

        public string GetName()
        {
            return _name;
        }

        abstract public void SetName(string name);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace interfacesAndAbstractClasses
{
    class Dog : Animal, Greeting, IComparable
    {
        string _name;

        public Dog(string name)
        {
            _name = name;
        }

        public void DisplayGreeting()
        {
            Console.WriteLine($"{_name} - *Barks Happily*");
        }

        public int CompareTo(object o)
        {
            if(o is Dog)
            {
                Dog temp = o as Dog;
                return _name.CompareTo(temp._name);
            }
            return 0;
        }

        public override void SetName(string name)
        {
            _name = name;
        }
    }
}

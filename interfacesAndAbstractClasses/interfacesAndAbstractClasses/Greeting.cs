﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace interfacesAndAbstractClasses
{
    interface Greeting
    {
        //Interfaces only contain method signatures no data and no method bodies
        //Everything in an interface will be public so no access modifiers
        void DisplayGreeting();
       
        //Really good for unrelated items small bits of similar functionality.

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace interfacesAndAbstractClasses
{
    class Person : Greeting
    {
        private string _name;

        public Person(string name)
        {
            _name = name;
        }

        public void DisplayGreeting()
        {
            Console.WriteLine($"{_name} says \"Hello\".");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace interfacesAndAbstractClasses
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Person p1 = new Person("Bob");
            /*
            p1.DisplayGreeting();

            
            Greeting greeter = p1;
            greeter.DisplayGreeting();

            greeter = new Dog("Fido");
            greeter.DisplayGreeting();
            */

            Greeting[] greeters = new Greeting[2];
            greeters[0] = p1;
            greeters[1] = new Dog("Fido");

            foreach(Greeting g in greeters)
            {
                g.DisplayGreeting();
            }

            Dog d1 = new Dog("Barky");
            Dog d2 = new Dog("Jeff");

            Console.WriteLine(d2.CompareTo(d1));

            Animal animal = d1;

            Utility.PauseBeforeContinuing();

        }
    }
}

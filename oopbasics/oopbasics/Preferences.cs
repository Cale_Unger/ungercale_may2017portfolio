﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oopbasics
{
    class Preferences
    {
        bool _shouldVideosAutoPlay;
        double _volumeLevel;

        public bool GetShouldVideosAutoPlay()
        {
            return _shouldVideosAutoPlay;
        }

        public double GetVolumeLevel()
        {
            return _volumeLevel;
        }

        public void SetShouldVideosAutoPlay(bool shouldVideosAutoPlay)
        {
            _shouldVideosAutoPlay = shouldVideosAutoPlay;
        }

        public void SetVolumeLevel(double volumeLevel)
        {
            _volumeLevel = volumeLevel;
        }
        
        public Preferences(bool shouldVideosAutoPlay, double volumeLevel)
        {
            _shouldVideosAutoPlay = shouldVideosAutoPlay;
            _volumeLevel = volumeLevel;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oopbasics
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            Console.WriteLine("Lets get two values");
            int value1 = GetInt();
            int value2 = GetInt("Bob asks for any int: ");
            int value3 = GetInt(10, 99, "Enter a value between 10 and 99: ");
            */

            //User
            Console.WriteLine("Please enter a name: ");
            string name = Console.ReadLine();
            Console.WriteLine($"Please enter an address for {name}: ");
            string address = Console.ReadLine();
            int age = GetInt(0, 120, $"Please enter and age for {name} (0-120): ");

            User user = new User(name, address, age);

            /*
            user.SetName(name);
            user.setAddress(address);
            user.setAge(age);
            */

            bool shouldAutoPlay = GetBool("Should videos auto play? (yes / no): ");
            double volumeLevel = 10;

            Preferences prefs = new Preferences(shouldAutoPlay, volumeLevel);
            Preferences prefsForLater = null;

            if (user != null)
            {
                user.SetPreferences(prefs);
            }

            Console.WriteLine($"User: {user.GetName()} is Age: {user.GetAge()}.");

            Console.WriteLine("Press any key to continue.");
            Console.ReadKey();

        }
        static int GetInt(string message = "Enter an integer: ")
        {
            int validatedInt;
            string input;

            do
            {
                Console.Write(message);
                input = Console.ReadLine();
            }
            while (Int32.TryParse(input, out validatedInt) == false);

            return validatedInt;
        }

        static int GetInt(int min, int max)
        {
            int validatedInt;
            string input;

            do
            {
                Console.Write("Enter an integer: ");
                input = Console.ReadLine();
            }
            while (Int32.TryParse(input, out validatedInt) == false || (validatedInt < min || validatedInt > max));

            return validatedInt;
        }

        static int GetInt(int min, int max, string message = "Enter and integer: ")
        {
            int validatedInt;
            string input;

            do
            {
                Console.Write(message);
                input = Console.ReadLine();
            }
            while (Int32.TryParse(input, out validatedInt) == false || (validatedInt < min || validatedInt > max));

            return validatedInt;
        }

        static bool GetBool(string message = "Enter Y or N: ")
        {
            bool selectedOption = true;
            bool keepAsking = true;
            string input = null;

            do
            {
                Console.WriteLine(message);
                input = Console.ReadLine();

                switch (input.ToLower())
                {
                    case "y":
                    case "yes":
                        {
                            selectedOption = true;
                            keepAsking = false;
                        }
                        break;
                    case "n":
                    case "no":
                        {
                            selectedOption = false;
                            keepAsking = false;
                        }
                        break;
                }
            }
            while (keepAsking);

            return selectedOption;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oopbasics
{
    class User
    {
        /*
         * Classes
         * 
         * Creating your own custom type
         *   Groups related methods and variables
         *   Container for related data about a single item
         *   Blueprint
         *   
         * Represent
         *   Tangible items
         *       Car, House, Person
         *   Intangible
         *       Checking account, graphics renderer, internet connection
         *       
         * Defining
         *   Keyword: Class
         *   Syntax: class NameOfYOurClass { // body of the class }
         *   
         *   A CLASS SHOULD ALWAYS BE DEFINED IN ITS OWN FILE
         */

        //Fields
        string _name;
        string _address;
        int _age;

        Preferences _preferences;

        public string GetName()
        {
            return _name;
        }

        public string GetAddress()
        {
            return _address;
        }

        public int GetAge()
        {
            return _age;
        }

        public Preferences GetPreferences()
        {
            return _preferences;
        }

        //Setters
        public void SetName(string name)
        {
            _name = name;
        }

        public void setAddress(string address)
        {
            _address = address;
        }

        public void setAge(int age)
        {
            _age = age;
        }

        public void SetPreferences(Preferences preferences)
        {
            _preferences = preferences;
        }

        //Constructors
        public User(string name, string address, int age)
        {
            _name = name;
            _address = address;
            _age = age;
        }

        //Default Constructor
        public User()
        {

        }
    }
}
